import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
  Box,
  Grid,
  Button,
  List,
  ListItem,
  ListItemText,
  Typography,
  Divider,
  Chip,
  Rating,
} from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import AxiosPedido from "./../components/axios/AxiosPedido";
import AxiosRestaurante from "./../components/axios/AxiosRestaurante";
import AxiosCliente from "./../components/axios/AxiosCliente";
import ModalBasic from "./../components/basicos/ModalBasic";
import RealizarReclamo from "./../components/RealizarReclamo";

const DetallePedido = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const [nombre, setNombre] = useState(
    getSession() ? getSession().nombre : history.push(rutas.iniciar_sesion)
  );
  const [rol, setRol] = useState(
    getSession() ? getSession().rol : history.push(rutas.iniciar_sesion)
  );
  const [idPedido, setIdPedido] = useState(
    localStorage.getItem("__detalle_pedido")
      ? JSON.parse(localStorage.getItem("__detalle_pedido"))
      : history.push(rutas.lista_restaurantes)
  );
  var nuevoIdPed = localStorage.getItem("__detalle_pedido");

  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();
  const [loader, setLoader] = useState(false);
  const [data, setData] = useState([]);
  const [detalleMenus, setDetalleMenus] = useState([]);
  const [detallePrecios, setDetallePrecios] = useState([]);
  const [calificacionResto, setCalificacionResto] = useState(0);
  const [calificacionCliente, setCalificacionCliente] = useState(0);

  const [costoTotal, setcostoTotal] = useState();
  const [menus, setMenus] = useState([]);
  const [emailRestaurante, setEmailRestaurante] = useState();
  const [costoEnvio, setCostoEnvio] = useState();
  const [costoFinal, setCostoFinal] = useState();
  const [puedeRepetirPedido, setPuedeRepetirPedido] = useState(false);

  const handleSnackBar = () => {
    setSnackbar(!snackbar);
  };
  useEffect(() => {
    AxiosPedido.get(
      `/listar/${nuevoIdPed}`,
      {},
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setData(response.data);
          calcularCantidad(response.data.menus);
          setcostoTotal(response.data.monto);
          setMenus(response.data.menus);
          setEmailRestaurante(response.data.emailrestaurante);
          if (response.data.califClienteResto != null)
            setCalificacionCliente(
              response.data.califClienteResto.valorCalificacion
            );
          if (response.data.califRestoCliente != null)
            setCalificacionResto(
              response.data.califRestoCliente.valorCalificacion
            );
          setCostoEnvio(response.data.costoEnvio);
          setCostoFinal(response.data.monto);
          setLoader(false);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
    setLoader(false);
  }, [nuevoIdPed]);

  useEffect(() => {
    let subtotal = costoTotal - costoEnvio;
    let totalPrecios = 0;
    detallePrecios.map((m) => {
      totalPrecios = totalPrecios + m;
    });
    if (totalPrecios === subtotal) {
      setPuedeRepetirPedido(true);
    } else setPuedeRepetirPedido(false);
  }, [costoFinal]);

  const calcularCantidad = (menus) => {
    let array = [];
    let grouped = [];
    let groupedPrecio = [];
    let salida = "";
    let salidaPrecio = "";
    let precios = [];

    //separo los titulos y los precios
    menus.map((val, k) => {
      array.push(val.titulo);
      if (val.promocion === null) {
        precios.push(val.precio);
      } else {
        let p = val.precio - (val.precio * val.promocion.descuento) / 100;
        precios.push(p);
      }
    });
    // agrupo por titulo para no tener repetidos
    grouped = array.reduce((r, v, i, a) => {
      if (v === a[i - 1]) {
        r[r.length - 1].push(v);
      } else {
        r.push(v === a[i + 1] ? [v] : v);
      }
      return r;
    }, []);

    groupedPrecio = precios.reduce((r, v, i, a) => {
      if (v === a[i - 1]) {
        r[r.length - 1].push(v);
      } else {
        r.push(v === a[i + 1] ? [v] : v);
      }
      return r;
    }, []);

    let detalleMenu = [];
    let detallePrecio = [];

    // creo un string con la cantidad de un titulo y el titulo
    // ej 2 x Milanesa
    grouped.map((val, key) => {
      Array.isArray(val)
        ? (salida = val.length + " x " + val[0] + " ")
        : (salida = "1 x " + val + " ");

      detalleMenu.push(salida);
    });
    groupedPrecio.map((val, key) => {
      Array.isArray(val)
        ? (salidaPrecio = val.length * val[0])
        : (salidaPrecio = val);

      detallePrecio.push(salidaPrecio);
    });

    setDetalleMenus(detalleMenu);
    setDetallePrecios(detallePrecio);
    return salida;
  };

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  const [rating, setRating] = React.useState(0);

  const handleRatingCliente = (event, newValue) => {
    setRating(newValue);
    let datosEnviar = {
      idPedido: nuevoIdPed,
      valorCalificacion: newValue,
    };

    if (calificacionResto == 0) {
      sendRatingCliente(datosEnviar);
      setCalificacionResto(newValue);
    } else {
      let datosModificarCalificacion = {
        idPedido: nuevoIdPed,
        valorCalificacion: newValue,
        descripcion: "",
      };

      updateRatingCliente(datosModificarCalificacion);
      setCalificacionResto(datosModificarCalificacion.valorCalificacion);
    }

    setLoader(false);
  };

  const handleRatingResto = (event, newValue) => {
    setRating(newValue);
    let datosEnviar = {
      idPedido: nuevoIdPed,
      valorCalificacion: newValue,
    };

    if (calificacionCliente == 0) {
      sendRatingResto(datosEnviar);
      setCalificacionCliente(newValue);
    } else {
      let datosModificarCalificacion = {
        idPedido: nuevoIdPed,
        valorCalificacion: newValue,
        descripcion: "",
      };

      updateRatingResto(datosModificarCalificacion);
      setCalificacionCliente(datosModificarCalificacion.valorCalificacion);
    }

    setLoader(false);
  };

  const sendRatingResto = (body) => {
    AxiosCliente.post(`/calificar/restaurante`, body, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setLoader(false);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
  };

  const sendRatingCliente = (body) => {
    AxiosRestaurante.post(`/calificar/cliente`, body, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setLoader(false);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
  };

  const updateRatingResto = (body) => {
    AxiosCliente.put(`/calificar/restaurante/modificar`, body, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setLoader(false);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
  };

  const deleteRatingResto = () => {
    AxiosCliente.delete(`/calificar/pedido/${nuevoIdPed}/eliminar`, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setLoader(false);
          setCalificacionCliente(0);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
  };

  const deleteRatingCliente = () => {
    AxiosRestaurante.delete(`/calificar/pedido/${nuevoIdPed}/eliminar`, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setLoader(false);
          setCalificacionResto(0);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
  };

  const updateRatingCliente = (body) => {
    AxiosRestaurante.put(`/calificar/cliente/modificar`, body, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          setLoader(false);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });
  };

  const handleRepetir = () => {
    let datos = {
      costoTotal: costoTotal - costoEnvio,
      menus: menus,
      emailRestaurante: emailRestaurante,
      costoEnvio: costoEnvio,
    };
    localStorage.setItem("__carrito", JSON.stringify(datos));
    localStorage.setItem("__montoPaypal", costoFinal);

    AxiosRestaurante.get(
      `/buscar/full/${emailRestaurante}`,
      {},
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        setLoader(true);
        if (response.status == 200) {
          localStorage.setItem("__restaurante", JSON.stringify(response.data));
          setLoader(false);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setLoader(false);
      });

    //localStorage.setItem("__restaurante", JSON.stringify(dataRestaurante));
    history.push(rutas.crear_pedido);
  };

  const [openModalMenu, setOpenModalMenu] = useState(false);
  const handleCloseModalMenu = () => setOpenModalMenu(false);
  return (
    <>
      <NavBar />
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Pedido #{nuevoIdPed}</h1>
              {data.estado === "FINALIZADO" ? (
                <Chip label={data.estado} color="success" sx={{ ml: 2 }} />
              ) : (
                <Chip label={data.estado} color="error" sx={{ ml: 2 }} />
              )}
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <List
                sx={{
                  width: "100%",
                  maxWidth: 500,
                  bgcolor: "background.paper",
                  pl: 2,
                  pr: 2,
                }}
              >
                {detalleMenus.map((value, key) => (
                  <>
                    <ListItem
                      disablePadding
                      key={value}
                      disableGutters
                      secondaryAction={detallePrecios[key]}
                      sx={{ color: "black" }}
                    >
                      <ListItemText primary={value} />
                    </ListItem>
                  </>
                ))}
                <Divider sx={{ borderColor: colorCodes.red, borderWidth: 1 }} />
                <ListItem
                  disablePadding
                  disableGutters
                  secondaryAction={costoTotal - costoEnvio}
                  sx={{ color: "black" }}
                >
                  <ListItemText primary="Subtotal" />
                </ListItem>
                <ListItem
                  disablePadding
                  disableGutters
                  secondaryAction={data.costoEnvio}
                  sx={{ color: "black" }}
                >
                  <ListItemText primary="Envío" />
                </ListItem>
                <ListItem
                  disablePadding
                  disableGutters
                  secondaryAction={"$ " + costoTotal}
                  sx={{ color: "black", mt: 2, fontWeight: "bold" }}
                >
                  <ListItemText primary="Total" />
                </ListItem>
                <Divider sx={{ borderColor: colorCodes.red }} />
                <ListItem
                  disablePadding
                  disableGutters
                  secondaryAction={
                    data.metodoPago === "EFECTIVO" ? "Efectivo" : "PayPal"
                  }
                  sx={{ color: "black" }}
                >
                  <ListItemText primary="Método de pago" />
                </ListItem>
                <ListItem
                  disablePadding
                  disableGutters
                  secondaryAction={nombre}
                  sx={{ color: "black" }}
                >
                  <ListItemText primary="Datos de entrega" />
                </ListItem>
                <ListItem
                  disablePadding
                  disableGutters
                  secondaryAction={data.direccion}
                  sx={{ color: "black", mb: 2 }}
                ></ListItem>
              </List>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              {!puedeRepetirPedido ? (
                <Alert severity="warning">
                  Los items de este pedido han cambiado, por lo que no se puede
                  repetir
                </Alert>
              ) : null}
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ pb: 6 }}
            >
              {rol === "CLIENTE" ? (
                <>
                  {data.estado === "FINALIZADO" ||
                  data.estado === "RECHAZADO" ? (
                    puedeRepetirPedido ? (
                      <Button
                        variant="contained"
                        color="success"
                        sx={{ m: 2 }}
                        onClick={() => handleRepetir()}
                      >
                        Repetir pedido
                      </Button>
                    ) : null
                  ) : null}
                  <Button
                    variant="contained"
                    color="success"
                    sx={{ m: 2 }}
                    onClick={() => setOpenModalMenu(true)}
                  >
                    Hacer un reclamo
                  </Button>
                  <ModalBasic
                    open={openModalMenu}
                    onClose={handleCloseModalMenu}
                    content={
                      <RealizarReclamo
                        idPedido={nuevoIdPed}
                        setLoader={setLoader}
                        setMjeError={setMjeError}
                        handleSnackBar={handleSnackBar}
                        setSeverity={setSeverity}
                        setOpenModalMenu={setOpenModalMenu}
                      />
                    }
                  />
                </>
              ) : null}
            </Grid>
          </Grid>
          <Grid
            item
            xs={12}
            display="flex"
            justifyContent="center"
            alignItems="center"
            sx={{ pb: 6 }}
          >
            {rol === "CLIENTE" ? (
              <>
                {data.estado === "FINALIZADO" || data.estado === "RECHAZADO" ? (
                  calificacionCliente > 0 ? (
                    <>
                      <Typography component="legend">
                        Tu puntuaci&oacute;n del pedido es:
                      </Typography>
                      <Rating
                        name="simple-controlled"
                        value={calificacionCliente}
                        onChange={handleRatingResto}
                      />
                      <Button
                        variant="contained"
                        color="error"
                        sx={{ m: 2 }}
                        onClick={deleteRatingResto}
                      >
                        Eliminar calificación
                      </Button>
                    </>
                  ) : (
                    <>
                      <Typography component="legend">
                        Todavia no has ingresado una puntuaci&oacute;n:
                      </Typography>
                      <Rating
                        name="no-value"
                        value={calificacionCliente}
                        onChange={handleRatingResto}
                      />
                    </>
                  )
                ) : null}
              </>
            ) : (
              <>
                {data.estado === "FINALIZADO" || data.estado === "RECHAZADO" ? (
                  calificacionResto > 0 ? (
                    <>
                      <Typography component="legend">
                        Tu puntuaci&oacute;n al cliente es:
                      </Typography>
                      <Rating
                        name="simple-controlled"
                        value={calificacionResto}
                        onChange={handleRatingCliente}
                      />
                      <Button
                        variant="contained"
                        color="error"
                        sx={{ m: 2 }}
                        onClick={deleteRatingCliente}
                      >
                        Eliminar calificación
                      </Button>
                    </>
                  ) : (
                    <>
                      <Typography component="legend">
                        Todavia no has ingresado una puntuaci&oacute;n a este
                        cliente:
                      </Typography>
                      <Rating
                        name="no-value"
                        value={calificacionResto}
                        onChange={handleRatingCliente}
                      />
                    </>
                  )
                ) : null}
              </>
            )}
          </Grid>
        </Box>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default DetallePedido;
