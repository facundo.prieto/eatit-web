# Instalacion del proyecto

Primero que nada tener [git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git) y [node](https://nodejs.org/en/download/) instalado 

## Descargar proyecto

Abrir la consola e ir a la direccion donde queramos tener el proyecto y clonarnos el repositorio con el siguiente comando,

`git clone git@gitlab.com:proyecto-tecnologo/proyecto-web.git`

## Instalaciones

Ir dentro de la carpeta que descargamos

`cd proyecto-web`

Instalar dependencias

`npm install`

## Correr proyecto

Para correr el proyecto debemos correr el siguiente comando y espearar a que se nos abra en el navegador predeterminado la pagina web.

`npm run`
