const rutas = {
  aprobar_rechazar_restaurante: "/aprobar-rechazar-restaurante",
  balance_restaurante: "/balance-ventas-restaurante",
  alta_menu: "/alta-menu",
  direcciones_cliente: "/direcciones-cliente",
  crear_pedido: "/crear-pedido",
  perfil_cliente: "/perfil-cliente",
  restaurante: "/restaurante",
  visualizar_estadisticas: "/visualizar-estadisticas",
  detalle_pedido: "/detalle-pedido",
  recuperar_contrasena: "/recuperar-contrasena",
  crear_usuario_cliente: "/crear-usuario-cliente",
  crear_usuario_goolge: "/crear-usuario-goolge",
  crear_usuario_restaurante: "/crear-usuario-restaurante",
  crear_usuario_administrador: "/crear-usuario-admin",
  lista_usuarios: "/lista-usuarios",
  iniciar_sesion: "/iniciar-sesion",
  lista_restaurantes: "/lista-restaurantes",
  payment_ok: "/payment-ok",
  menu: "/menu",
  bandeja_entrada_pedidos: "/bandeja_entrada_pedidos",
  listar_pedidos_rc: "/listar_pedidos_rc",
  listar_historico_restaurante: "/listar_historico_restaurante",
  listar_historico_reclamos: "/listar_historico_reclamos",
  raiz: "/",
  admin_promociones: "/admin-promociones",
  listar_reclamos: "/listar_reclamos",
  cambiar_clave: "/cambiar_clave",
};

export default rutas;
