import React, { useEffect } from "react";
import "ol/ol.css";
import Feature from "ol/Feature";
import Map from "ol/Map";
import Point from "ol/geom/Point";
import VectorSource from "ol/source/Vector";
import View from "ol/View";
import { Icon, Style } from "ol/style";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { Circle } from "ol/geom";
import OSM from "ol/source/OSM";

const MapaRestaurante = (props) => {
  const { posicion = [], limite = 1000 } = props;
  console.log(posicion);
  useEffect(() => {
    const iconFeature = new Feature({
      geometry: new Point(posicion),
      name: "Null Island",
      population: 4000,
      rainfall: 500,
    });

    const iconStyle = new Style({
      image: new Icon({
        scale: 0.04,
        src: "https://www.agroavisos.net/wp-content/uploads/2017/04/map-marker-icon.png",
      }),
    });

    iconFeature.setStyle(iconStyle);

    const vectorSource = new VectorSource({
      features: [iconFeature],
    });

    //marker
    const marker = new VectorLayer({
      source: vectorSource,
    });

    //mapa
    const mapa = new TileLayer({
      source: new OSM(),
    });

    //radio
    const circleFeature = new Feature({
      geometry: new Circle(posicion, limite),
    });
    circleFeature.setStyle(
      new Style({
        renderer(coordinates, state) {
          const [[x, y], [x1, y1]] = coordinates;
          const ctx = state.context;
          const dx = x1 - x;
          const dy = y1 - y;
          const radius = Math.sqrt(dx * dx + dy * dy);

          const innerRadius = 0;
          const outerRadius = radius * 1.4;

          const gradient = ctx.createRadialGradient(
            x,
            y,
            innerRadius,
            x,
            y,
            outerRadius
          );
          gradient.addColorStop(0, "rgba(255,0,0,0)");
          gradient.addColorStop(0.6, "rgba(255,0,0,0.2)");
          gradient.addColorStop(1, "rgba(255,0,0,0.8)");
          ctx.beginPath();
          ctx.arc(x, y, radius, 0, 2 * Math.PI, true);
          ctx.fillStyle = gradient;
          ctx.fill();

          ctx.arc(x, y, radius, 0, 2 * Math.PI, true);
          ctx.strokeStyle = "rgba(255,0,0,1)";
          ctx.stroke();
        },
      })
    );

    const radio = new VectorLayer({
      source: new VectorSource({
        features: [circleFeature],
      }),
    });

    const map = new Map({
      layers: [mapa, marker, radio],
      target: "map",
      view: new View({
        center: posicion,
        zoom: 12,
      }),
    });
  }, []);

  return <div className="map" id="map"></div>;
};

export default MapaRestaurante;
