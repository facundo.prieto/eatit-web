import React, { useState } from "react";
import { Grid, Box, Button, Alert, Snackbar } from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import { Container } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ListPromociones from "../components/ListPromociones";

import Modal from "@mui/material/Modal";
import AltaPromocion from "../components/AltaPromocion";
import ListMenus from "./ListMenus";
import AltaMenu from "../screens/AltaMenu";
import ModalBasic from "./basicos/ModalBasic";
import { Link } from "react-router-dom";

const ListarMenuConPromociones = () => {
  const [openModal, setOpenModal] = useState(false);
  const [loader, setLoader] = React.useState(false);

  const handleLoader = () => {
    setLoader(!loader);
  };

  const [menu, setMenu] = useState();
  const [nuevosDatosPromocion, setNuevosDatosPromocion] = React.useState(true);
  const handleNuevosDatosPromocion = () => {
    setNuevosDatosPromocion(!nuevosDatosPromocion);
  };

  const [nuevosDatosMenu, setNuevosDatosMenu] = React.useState(true);
  const handleNuevosDatosMenu = () => {
    setNuevosDatosMenu(!nuevosDatosMenu);
  };

  const [openModalAltaMenu, setOpenModalAltaMenu] = useState(false);
  const handleOpenModalAltaMenu = () =>
    setOpenModalAltaMenu(!openModalAltaMenu);
  const handleCloseModalAltaMenu = () => setOpenModalAltaMenu(false);

  const handleModalAltaMenu = (value) => {
    setMenu(value);
    setOpenModalAltaMenu(true);
  };

  const handleOpenModal = () => setOpenModal(true);

  const handleCloseModal = () => {
    setOpenModalAltaMenu(false);
    setOpenModal(false);
  };

  const [snackbar, setSnackbar] = useState(false);
  const [severity, setSeverity] = useState("success");
  const [mje, setMje] = useState("");

  const handleSnackBar = () => setSnackbar(!snackbar);

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  const [menuNuevo, setMenuNuevo] = useState({
    titulo: "",
    id_resto: "",
    precio: "",
    descripcion: "",
    imagen: "",
    categorias: [],
  });
  const handleCrearMenu = (data) => setMenuNuevo(data);

  return (
    <>
      <Grid container spacing={2}>
        <Grid
          item
          xs={6}
          display="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6, pt: 2 }}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Grid
                item
                xs={6}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <h1 align="center">Menus </h1>
              </Grid>

              <Grid
                item
                xs={6}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Button
                  align="center"
                  variant="contained"
                  color="success"
                  sx={{ mb: 2, mt: 2 }}
                  onClick={handleOpenModalAltaMenu}
                >
                  <AddIcon />
                  Agregar Men&uacute;
                </Button>
              </Grid>
            </Grid>

            <ListMenus
              handleNuevosDatosPromocion={handleNuevosDatosPromocion}
              nuevosDatosPromocion={nuevosDatosPromocion}
              setNuevosDatosPromocion={setNuevosDatosPromocion}
              handleNuevosDatosMenu={handleNuevosDatosMenu}
              nuevosDatosMenu={nuevosDatosMenu}
              setNuevosDatosMenu={setNuevosDatosMenu}
              handleLoader={handleLoader}
              handleN
              loader={loader}
              setMje={setMje}
              handleSnackBar={handleSnackBar}
              setSeverity={setSeverity}
            />
          </Box>
        </Grid>
        <Grid
          item
          xs={6}
          display="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6, pt: 2 }}>
            <h1 align="center">Promociones</h1>
            <ListPromociones
              handleNuevosDatosPromocion={handleNuevosDatosPromocion}
              nuevosDatosPromocion={nuevosDatosPromocion}
              setNuevosDatosPromocion={setNuevosDatosPromocion}
              handleNuevosDatosMenu={handleNuevosDatosMenu}
              nuevosDatosMenu={nuevosDatosMenu}
              setNuevosDatosMenu={setNuevosDatosMenu}
              handleLoader={handleLoader}
              handleN
              loader={loader}
              setMje={setMje}
              handleSnackBar={handleSnackBar}
              setSeverity={setSeverity}
            />
          </Box>
        </Grid>
      </Grid>

      <Modal
        open={openModal}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <AltaPromocion />
      </Modal>

      <ModalBasic
        open={openModalAltaMenu}
        onClose={handleCloseModalAltaMenu}
        content={
          <AltaMenu
            handleCloseModalAltaMenu={handleCloseModalAltaMenu}
            isModal={false}
            showNavBar={false}
            handleCrearMenu={handleCrearMenu}
            handleNuevosDatosMenu={handleNuevosDatosMenu}
            handleNuevosDatosPromocion={handleNuevosDatosPromocion}
            setMje={setMje}
            setSnackbar={setSnackbar}
            setSeverity={setSeverity}
          />
        }
      />

      <Snackbar open={snackbar} autoHideDuration={2000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mje}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListarMenuConPromociones;
