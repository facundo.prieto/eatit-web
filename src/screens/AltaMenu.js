import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import rutas from "../assets/const/rutas";
import {
  Container,
  Grid,
  Box,
  Button,
  Autocomplete,
  TextField,
  Checkbox,
  Alert,
} from "@mui/material";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import { useDropzone } from "react-dropzone";
import Input from "../components/basicos/Input";
import NavBar from "../components/NavBar/NavBar";
import categoriasAlimentos from "../assets/data/CategoriasAlimentos.json";
import { getSession } from "./../components/auth/auth";
import AxiosImagen from "../components/axios/AxiosImagen";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import colorCodes from "../assets/colors/Colors";
import AddIcon from "@mui/icons-material/Add";

const AltaMenu = (props) => {
  const {
    handleCloseModalAltaMenu,
    isModal = false,
    showNavBar = true,
    handleCrearMenu,
    handleNuevosDatosMenu,
    setMje,
    setSnackbar,
    setSeverity,
  } = props;

  const [state, setState] = useState({
    titulo: "",
    precio: "",
    descripcion: "",
  });

  const handleChangeState = (event) => {
    setState((prevState) => ({
      ...prevState,
      [event.target.id]: event.target.value,
    }));
  };

  const [image, setImage] = useState("");

  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    onDrop: (file) => {
      setImage(
        Object.assign(file[0], {
          file: file[0],
          filename: file[0].name,
          preview: URL.createObjectURL(file[0]),
        })
      );
    },
  });

  const [categorias, setcategorias] = useState([]);

  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  const [loader, setLoader] = useState(false);

  let history = useHistory();

  const [emailRestaurante, setEmailRestaurante] = useState("");

  const [openModal, setOpenModal] = useState(false);

  const handleClose = () => {
    setOpenModal(false);
  };

  useEffect(() => {
    isModal
      ? setEmailRestaurante("")
      : setEmailRestaurante(
          getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
        );
  }, []);

  const handleClick = () => {
    setLoader(true);
    if (state.titulo && state.precio && image && categorias.length > 0) {
      var formData = new FormData();
      formData.append("filename", image.name);
      formData.append("file", image.file);

      AxiosImagen.post("/menu", formData, {
        headers: {
          Authorization: ``,
        },
      })
        .then(function (response) {
          if (response.status == 200) {
            let data = {
              titulo: state.titulo,
              emailRestaurante: emailRestaurante,
              precio: state.precio,
              descripcion: state.descripcion,
              image: response.data,
              categorias: categorias,
            };
            if (isModal) {
              handleCrearMenu(data);
              setMje(`El menú ${state.titulo} se cargo correctamente`);
              setSeverity("success");
            } else {
              console.log(data);
              AxiosRestaurante.post("/menu/crear", data, {
                headers: {
                  Authorization: ``,
                },
              })
                .then(function (response) {
                  console.log(response);
                  if (response.status == 200) {
                    setSeverity("success");
                    setMje(`El menú ${state.titulo} se cargo correctamente`);
                    handleNuevosDatosMenu();
                  } else {
                    setMje(response.data);
                    setSeverity("error");
                  }
                  setLoader(false);
                })
                .catch(function (error) {
                  console.log("catch /menu/crear");
                  setMje("Error en el servidor");
                  setSeverity("error");
                  setLoader(false);
                });
              handleCloseModalAltaMenu();
            }
          } else {
            setMje(response.data);
            setSeverity("error");
          }
          setLoader(false);
        })
        .catch(function (error) {
          console.log("catch /menu");
          setMje("Error en el servidor");
          setSeverity("error");
          setLoader(false);
        });
    } else {
      setMje("Los campos que tienen * son obligatorios");
      setSeverity("error");
      setLoader(false);
    }
    setSnackbar(true);
  };

  return (
    <>
      {showNavBar ? <NavBar /> : ""}
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Crear Men&uacute;</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="titulo"
                label="T&iacute;tulo *"
                value={state.titulo}
                onChange={handleChangeState}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="precio"
                label="Precio *"
                value={state.precio}
                onChange={handleChangeState}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="descripcion"
                label="Descripci&oacute;n"
                value={state.descripcion}
                onChange={handleChangeState}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="left"
              alignItems="center"
              sx={{ ml: 6, mt: 2 }}
            >
              <label>Subir imagen *</label>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 6, mr: 6, mb: 2 }}
            >
              {image ? (
                <Grid container>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ ml: 6, mr: 6, mb: 2 }}
                  >
                    <Alert severity="success" sx={{ width: "100%" }}>
                      Imagen cargada correctamente
                    </Alert>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    justifyContent="left"
                    alignItems="left"
                    sx={{ ml: 6, mr: 6, mb: 2 }}
                  >
                    <img
                      src={image.preview}
                      style={{
                        display: "block",
                        width: "auto",
                        height: "100px",
                        border: "2px solid white",
                      }}
                    />
                    <div
                      {...getRootProps()}
                      style={{
                        display: "flex",
                        width: "100%",
                        alignItems: "center",
                        justifyContent: "center",
                        padding: "0px 10px",
                        border: "1px dashed white",
                        borderRadius: "5px",
                        marginLeft: "2em",
                      }}
                    >
                      <input {...getInputProps()} />
                      <p>
                        {" "}
                        Haz click aqui o arrastra un archivo para cambiarlo{" "}
                      </p>
                      <div sx={{ m: "1em" }}>
                        <FileUploadIcon />
                      </div>
                    </div>
                  </Grid>
                </Grid>
              ) : (
                <div
                  {...getRootProps()}
                  style={{
                    display: "flex",
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "0px 10px",
                    border: "1px dashed white",
                    borderRadius: "5px",
                  }}
                >
                  <input {...getInputProps()} />
                  <p> Haz click aqui o arrastra un archivo para subirlo </p>
                  <div sx={{ m: "1em" }}>
                    <FileUploadIcon />
                  </div>
                </div>
              )}
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Autocomplete
                sx={{
                  mr: 6,
                  ml: 6,
                  mb: 2,
                  "& .MuiAutocomplete-tag": {
                    backgroundColor: "whitesmoke",
                  },
                }}
                fullWidth
                multiple
                id="categorias"
                options={categoriasAlimentos}
                disableCloseOnSelect
                getOptionLabel={(option) => option.descripcion}
                value={categorias}
                onChange={(event, newcategorias) => {
                  setcategorias(newcategorias);
                }}
                renderOption={(props, option, { selected }) => {
                  return (
                    <li {...props}>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                      />
                      {option.descripcion}
                    </li>
                  );
                }}
                renderInput={(params) => (
                  <TextField
                    sx={{
                      "& label.Mui-focused": {
                        color: "white",
                      },
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "white",
                      },
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          borderColor: "white",
                        },
                        "&:hover fieldset": {
                          borderColor: "white",
                        },
                        "&.Mui-focused fieldset": {
                          borderColor: "white",
                        },
                      },
                    }}
                    {...params}
                    label="Categor&iacute;as *"
                    placeholder="Categor&iacute;as *"
                  />
                )}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={handleClick}
              >
                <AddIcon />
                Agregar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default AltaMenu;
