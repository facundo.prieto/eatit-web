import React, { Component } from "react";
import {
  Grid,
  Box,
  Button,
  Alert,
  Snackbar,
  Container,
} from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import Input from "./../components/basicos/Input";
import NavBar from "./../components/NavBar/NavBar";

import axiosAdmin from '../components/axios/AxiosAdmin';
import { getSession } from "./../components/auth/auth";

class CrearUsuarioAdmin extends Component{
  constructor(){
    super();
    this.state = {
      email: "",
      contrasena: "",
      repcontrasena: "",
      severity: "",
      alertaError: false,
      msgErr:""
    }
  }

  handleEmail = (event) => {
    this.setState({ email: event.target.value });
  };

  handleContrasena = (event) => {
    this.setState({ contrasena: event.target.value });
  };

  handleRepContrasena = (event) => {
    this.setState({ repcontrasena: event.target.value });
  };

    handleComenzar = (event) => {
      //Controlo que se haya cargado la misma contraseña dos veces
      if(this.state.contrasena!==this.state.repcontrasena){
          this.setState({ alertaError: true, severity: "warning", msgErr:"Debe ingresar la misma contraseña dos veces" });
      }else{
          
        //Controlo que se hayan cargado todos los datos
        if(this.state.email==="" || (this.state.contrasena===""))
        {
            this.setState({ alertaError: true, severity: "error", msgErr:"Debe completar todos los datos solicitados" });
        }else{
            //Mando los datos del state al CrearUsuarioAdmin del ClienteController
            var t = this;
            let token = "";
            if(getSession() != null && getSession().token != null){
              token = getSession().token;
            }
            axiosAdmin.post('/crear', {
              email: this.state.email,
              password: this.state.contrasena,
            }, { headers: {
              'Authorization': `${token}` 
            },
            })
            .then(function (response) {
              console.log(response.data);
              if(response.status == 200){
                t.setState({ alertaError: true, severity: "success", msgErr:"Usuario administrador creado" });  
              }else{
                t.setState({ alertaError: true, severity: "error", msgErr:response.data});
              }
            })
            .catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    t.setState({ alertaError: true, severity: "error", msgErr:error.response.data });
                } else {
                    // Something happened in setting up the request that triggered an Error
                    t.setState({ alertaError: true, severity: "error", msgErr:error.message });
                }
            });
        }
      }
    }


  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alertaError: false, msgErr: "" });
  };

  render() {
    //console.log(this.state);
    return (
      <>
        <NavBar />
        <Container maxWidth="md">
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
            <Grid container spacing={2}>
              {/* Título */}
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <h1>Crear usuario administrador</h1>
              </Grid>

                  
                  {/* Campos */}
                  <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                      <Input id="email" label="Correo electrónico" onChange={this.handleEmail}/>
                  </Grid>
                  <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                      <Input id="contrasena" label="Contraseña" onChange={this.handleContrasena}/>
                  </Grid>
                  <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                      <Input id="repcontrasena" label="Repetir contraseña" onChange={this.handleRepContrasena}/>
                  </Grid>
                  
                  {/* Botón */}
                  <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                      <Button variant="contained" color="error" sx={{ mb: 2, my: 4 }} onClick={this.handleComenzar}>
                          COMENZAR
                      </Button>
                  </Grid>
              </Grid>
          </Box>

          <Snackbar
            open={this.state.alertaError}
            autoHideDuration={6000}
            onClose={this.handleClose}
          >
            <Alert
              onClose={this.handleClose}
              severity={this.state.severity}
              sx={{ width: "100%" }}
            >
              {this.state.msgErr}
            </Alert>
          </Snackbar>
        </Container>
      </>
    );
  }
}

export default CrearUsuarioAdmin;
