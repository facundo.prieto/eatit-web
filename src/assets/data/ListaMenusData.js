const ListaMenusData = [
    {
        id: 1,
        titulo: "Fainá",
        precio: 135,
        descripcion: "El mejor fainá de Montevideo.",
        image: "https://d29rjy3mdffawd.cloudfront.net/images/faina_de_queso_al_horno.jpg",
        categoria: "Pizzería"
        
    },
    {
        id: 2,
        titulo: "Pizza",
        precio: 135,
        descripcion: "La mejor pizza de Montevideo.",
        image: "https://www.pedidosya.com.uy/blog/wp-content/uploads/sites/4/2018/05/pizza-comun.jpg",
        categoria: "Pizzería"
    },
    {
        id: 3,
        titulo: "Muzzarella",
        precio: 200,
        descripcion: "La mejor muzza de Montevideo.",
        image: "https://www.pedidosya.com.uy/blog/wp-content/uploads/sites/4/2018/05/pizza-mozzarella.jpg",
        categoria: "Pizzería"
    },
    {
        id: 4,
        titulo: "Hamburguesa clásica con guarnición",
        precio: 550,
        descripcion: "300 g de carne premium, lechuga, tomate, mayonesa y huevo.",
        image: "https://easyrecetas.com/wp-content/uploads/2020/05/Receta-de-Hamburguesa-Cl%C3%A1sica.jpg",
        categoria: "Hamburguesas"
    },
    {
        id: 5,
        titulo: "Hamburguesa criolla con guarnición",
        precio: 560,
        descripcion: "300 g de carne premium, jamón, muzzarella, panceta, morrón, aceitunas, cebolla, lechuga, tomate, mayonesa y huevo.",
        image: "https://uruguayanmeats.uy/wp-content/uploads/2018/10/hamburguesa-1200-1200x480.jpg",
        categoria: "Hamburguesas"
    },
    {
        id: 6,
        titulo: "Hamburguesa vegetariana",
        precio: 550,
        descripcion: "Calabaza, zapallito, aceituna, morrón, lechuga, tomate, huevo duro, queso y mayonesa.",
        image: "https://i.blogs.es/01a71a/hamburguesa-vegetariana-boniato/650_1200.jpg",
        categoria: "Hamburguesas"
    },
];

export default ListaMenusData;