import React, { useState, useEffect } from "react";
import {
  List,
  Card,
  CardActions,
  CardContent,
  Button,
  Typography,
  ListItem,
  ListItemText,
  Divider,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { useHistory } from "react-router-dom";
import rutas from "./../../assets/const/rutas";
import AxiosRestaurante from "./../axios/AxiosRestaurante";
import { getSession } from "../auth/auth";

const Carrito = (props) => {
  let history = useHistory();
  const { menus = null, emailRestaurante = null } = props;

  const [costoTotal, setcostoTotal] = useState(0);
  const [costoEnvio, setCostoEnvio] = useState(0);
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const [dataRestaurante, setDataRestaurante] = useState([]);

  useEffect(() => {
    //console.log(menus);
    const sumaPrecios =
      menus !== null
        ? menus.reduce((prev, next) => prev + next.precio * next.cantidad, 0)
        : 0;
    setcostoTotal(sumaPrecios);
    //console.log(costoTotal);
  }, [menus]);

  useEffect(() => {
    AxiosRestaurante.get(
      `/buscar/full/${emailRestaurante}`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        //console.log(response.data);

        setDataRestaurante(response.data);
        setCostoEnvio(response.data.costoEnvio);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [menus]);

  const handleContinuar = () => {
    localStorage.setItem(
      "__carrito",
      JSON.stringify({
        costoTotal: costoTotal,
        menus: menus,
        emailRestaurante: emailRestaurante,
        costoEnvio: costoEnvio,
      })
    );
    //console.log(dataRestaurante);
    localStorage.setItem("__restaurante", JSON.stringify(dataRestaurante));
    history.push(rutas.crear_pedido);
  };
  return (
    <>
      {menus && menus.length === 0 ? (
        <Card
          sx={{ minWidth: 275, maxWidth: 300, position: "fixed", top: "100px" }}
        >
          <CardContent>
            <Typography variant="h6" component="div">
              Mi pedido
            </Typography>
            <Typography
              sx={{ mt: 1.5, textAlign: "center" }}
              color="text.secondary"
            >
              <SearchIcon sx={{ fontSize: 100 }} />
            </Typography>
            <Typography variant="body2" sx={{ textAlign: "center" }}>
              El carrito esta vacío
              <br />
            </Typography>
          </CardContent>
        </Card>
      ) : (
        <Card
          sx={{ minWidth: 275, maxWidth: 300, position: "fixed", top: "100px" }}
        >
          <CardContent>
            <Typography variant="h5" component="div">
              Mi pedido
            </Typography>
            <List dense={true}>
              {menus &&
                menus.map((value, key) => (
                  <>
                    <ListItem>
                      <ListItemText
                        primary={`${value.cantidad}x ${value.titulo}`}
                        secondary={value.descripcion}
                      />
                      <b>$ {value.precio * value.cantidad}</b>
                    </ListItem>
                    <Divider />
                  </>
                ))}
              <ListItem sx={{ border: "1px solid red", mb: 1 }}>
                <ListItemText primary="SubTotal" />
                <b>$ {costoTotal}</b>
              </ListItem>
            </List>
          </CardContent>
          <CardActions style={{ justifyContent: "center" }}>
            <Button
              color="error"
              variant="contained"
              size="small"
              onClick={() => handleContinuar()}
            >
              Continuar
            </Button>
          </CardActions>
        </Card>
      )}
    </>
  );
};
export default Carrito;
