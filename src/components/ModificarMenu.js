import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import rutas from "../assets/const/rutas";
import {
  Container,
  Grid,
  Box,
  Button,
  Autocomplete,
  TextField,
  Checkbox,
  CardMedia,
} from "@mui/material";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import Dropzone, { useDropzone } from "react-dropzone";
import Input from "../components/basicos/Input";
import categoriasAlimentos from "../assets/data/CategoriasAlimentos.json";
import { getSession } from "./../components/auth/auth";
import AxiosImagen from "../components/axios/AxiosImagen";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import colorCodes from "../assets/colors/Colors";

const ModificarMenu = (props) => {
  const {
    datos,
    handleOpenModalUpdateMenu,
    handleNuevosDatosMenu,
    setMje,
    handleSnackBar,
    setSeverity,
  } = props;

  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    onDrop: (file) => {
      setImage(
        Object.assign(file[0], {
          file: file[0],
          filename: file[0].name,
          preview: URL.createObjectURL(file[0]),
        })
      );
    },
  });
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  let history = useHistory();

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [titulo, setTitulo] = useState("");
  const [precio, setPrecio] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [image, setImage] = useState("");
  const [categorias, setcategorias] = useState([]);
  const handleChangeTitulo = (event) => setTitulo(event.target.value);
  const handleChangePrecio = (event) => setPrecio(event.target.value);
  const handleChangeDescripcion = (event) => setDescripcion(event.target.value);
  const handleChangeImage = (event) => setImage(event.target.value);
  useEffect(() => {
    setTitulo(datos.titulo);
    setPrecio(datos.precio);
    setDescripcion(datos.descripcion);
    setImage(datos.imagen);
    setcategorias(datos.categorias);
  }, [datos.imagen]);

  const handleClick = () => {
    if (datos.titulo && datos.precio && categorias.length > 0) {
      if (image.name) {
        var imgData = new FormData();
        imgData.append("filename", image.name);
        imgData.append("file", image.file);

        AxiosImagen.post("/menu", imgData, {
          headers: {
            Authorization: `token`,
          },
        })
          .then(function (response) {
            if (response.status == 200) {
              let data = {
                id: datos.id,
                titulo: titulo,
                emailRestaurante: getSession().email,
                precio: precio,
                descripcion: descripcion,
                image: response.data,
                categorias: categorias,
              };

              AxiosRestaurante.put("/menu/actualizar", data, {
                headers: {
                  Authorization: `${token}`,
                },
              })
                .then(function (response) {
                  if (response.status == 200) {
                    setMje(`El menú ${titulo} se modificó correctamente`);

                    setSeverity("success");
                  } else {
                    setSeverity("error");
                    setMje("Ocurrio un error en el sistema modificar");
                  }
                })
                .catch(function (error) {
                  setSeverity("error");
                  setMje("Ocurrio un error en el sistema imagen");
                });
            } else {
              setMje(response.data);
              setSeverity("error");
            }
            handleOpenModalUpdateMenu();
            handleNuevosDatosMenu();
          })
          .catch(function (error) {
            setMje("Problemas al cargar la imagen");
            setSeverity("error");
            handleOpenModalUpdateMenu();
          });
        handleSnackBar();
      } else {
        let data = {
          id: datos.id,
          titulo: titulo,
          emailRestaurante: getSession().email,
          precio: precio,
          descripcion: descripcion,
          image: image,
          categorias: categorias,
        };

        AxiosRestaurante.put("/menu/actualizar", data, {
          headers: {
            Authorization: `${token}`,
          },
        })
          .then(function (response) {
            if (response.status == 200) {
              setMje(`El menú ${titulo} se modificó correctamente`);

              setSeverity("success");
            } else {
              setSeverity("error");
              setMje("Ocurrio un error en el sistema modificar");
            }
          })
          .catch(function (error) {
            setSeverity("error");
            setMje("Ocurrio un error en el sistema imagen");
          });
        handleOpenModalUpdateMenu();
        handleNuevosDatosMenu();
      }
    }
  };

  return (
    <>
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Modificar Men&uacute;</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="titulo"
                label="T&iacute;tulo *"
                value={titulo}
                onChange={handleChangeTitulo}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="precio"
                label="Precio *"
                value={precio}
                onChange={handleChangePrecio}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="descripcion"
                label="Descripci&oacute;n"
                value={descripcion}
                onChange={handleChangeDescripcion}
                variant="standard"
              />
            </Grid>

            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="left"
              alignItems="center"
              sx={{ ml: 6, mt: 2 }}
            >
              <CardMedia
                component="img"
                sx={{ width: 120, p: 2, height: 120 }}
                image={datos.imagen}
                alt="Imagen no disponible"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="left"
              alignItems="center"
              sx={{ ml: 6, mt: 2 }}
            >
              <label>Subir imagen *</label>
            </Grid>

            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 6, mr: 6, mb: 2 }}
            >
              <div
                {...getRootProps()}
                style={{
                  display: "flex",
                  width: "100%",
                  alignItems: "center",
                  justifyContent: "center",
                  padding: "0px 10px",
                  border: "1px dashed white",
                  borderRadius: "5px",
                }}
              >
                <input {...getInputProps()} />
                <p> Haz click aqui o arrastra un archivo para subirlo </p>
                <div sx={{ m: "1em" }}>
                  <FileUploadIcon />
                </div>
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Autocomplete
                sx={{
                  mr: 6,
                  ml: 6,
                  mb: 2,
                  "& .MuiAutocomplete-tag": {
                    backgroundColor: "whitesmoke",
                  },
                }}
                fullWidth
                multiple
                id="categorias"
                options={categoriasAlimentos}
                disableCloseOnSelect
                getOptionLabel={(option) => option.descripcion}
                value={categorias}
                onChange={(event, newcategorias) => {
                  setcategorias(newcategorias);
                }}
                renderOption={(props, option, { selected }) => {
                  return (
                    <li {...props}>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                      />
                      {option.descripcion}
                    </li>
                  );
                }}
                renderInput={(params) => (
                  <TextField
                    sx={{
                      "& label.Mui-focused": {
                        color: "white",
                      },
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "white",
                      },
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          borderColor: "white",
                        },
                        "&:hover fieldset": {
                          borderColor: "white",
                        },
                        "&.Mui-focused fieldset": {
                          borderColor: "white",
                        },
                      },
                    }}
                    {...params}
                    label="Categor&iacute;as *"
                    placeholder="Categor&iacute;as *"
                  />
                )}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={handleClick}
              >
                Modificar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default ModificarMenu;
