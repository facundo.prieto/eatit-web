import React, { useState, useEffect } from "react";
import InfoIcon from "@mui/icons-material/Info";
import {
  Typography,
  Button,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Badge,
  Divider,
  Link,
  Grid,
} from "@mui/material";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { getSession } from "../components/auth/auth";
import AxiosCliente from "./axios/AxiosCliente";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import DeliveryDiningIcon from "@mui/icons-material/DeliveryDining";
import colorCodes from "./../assets/colors/Colors";

const ModalPedidosEnCurso = () => {
  let history = useHistory();
  const [datos, setDatos] = useState([]);
  useEffect(() => {
    let session = getSession();
    let email = "";
    if (session != null && session.email != null) {
      email = session.email;
    }
    let token = "";
    if (session != null && session.token != null) {
      token = session.token;
    }
    AxiosCliente.get(
      `/pedidos/listar/${email}`,
      {},
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        //console.log(response);
        let dat = response.data.filter((val) => {
          return val.estado === "CREADO" ||
            val.estado === "CONFIRMADO" ||
            val.estado === "EN_CAMINO"
            ? val
            : null;
        });

        setDatos(dat);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const handleIrAlPedido = (id) => {
    localStorage.setItem("__detalle_pedido", id);
    history.push(rutas.detalle_pedido);
  };

  return (
    <>
      {datos.length === 0 ? null : (
        <Accordion
          sx={{
            minWidth: 350,
            bottom: 15,
            left: 15,
            position: "fixed",
            zIndex: 5000,
          }}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            sx={{ mt: 1 }}
          >
            <Typography
              sx={{ fontSize: 14, fontWeight: "bold" }}
              color="text.secondary"
              gutterBottom
            >
              Pedidos en curso{" "}
              <Badge
                badgeContent={datos.length}
                color="error"
                max={5}
                sx={{ ml: 1 }}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
              >
                <DeliveryDiningIcon color="action" />
              </Badge>
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            {datos.map((value, key) => (
              <div
                style={{
                  border: "solid 1px #00000033",
                  borderRadius: "6px",
                  padding: "1em 1em 2px 1em",
                  marginBottom: "1em",
                }}
              >
                <Typography
                  sx={{ fontSize: 14, fontWeight: "bold" }}
                  color="text.secondary"
                  gutterBottom
                >
                  {value.estado === "CREADO" &&
                    "El local está procesando tu pedido"}
                  {value.estado === "CONFIRMADO" &&
                    "El local está preparando tu pedido"}
                  {value.estado === "EN_CAMINO" &&
                    "El repartidor salió con tu pedido del local"}
                </Typography>
                <Typography variant="caption" display="block" gutterBottom>
                  {value.restauranteNombre}
                  {" - "}
                  {value.metodoPago === "PAYPAL"
                    ? "Ya pago"
                    : "Paga en domicilio"}

                  {value.estado !== "CREADO" &&
                    " - " + value.tiempoEstimado + " minutos"}
                </Typography>
                <Divider sx={{ borderColor: "rgb(204 204 204)" }} />
                <Typography
                  variant="caption"
                  display="block"
                  gutterBottom
                  sx={{ pt: 1, cursor: "pointer" }}
                >
                  <Link
                    underline="none"
                    onClick={() => handleIrAlPedido(value.idPedido)}
                    color={colorCodes.red}
                  >
                    Ver m&aacute;s
                  </Link>
                </Typography>
              </div>
            ))}
          </AccordionDetails>
        </Accordion>
      )}
    </>
  );
};

export default ModalPedidosEnCurso;
