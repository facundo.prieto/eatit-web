const MenuData =
    {
        id: 1,
        titulo: "Fainá",
        precio: 135,
        descripcion: "El mejor fainá de Montevideo.",
        image: "https://d29rjy3mdffawd.cloudfront.net/images/faina_de_queso_al_horno.jpg",
        categoria: "Pizzería"
    }

export default MenuData;