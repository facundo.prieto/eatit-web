import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import Logout from "@mui/icons-material/Logout";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import HomeIcon from "@mui/icons-material/Home";
import VpnKeyIcon from "@mui/icons-material/VpnKey";
import { getSession, endSession } from "../auth/auth";
import rutas from "./../../assets/const/rutas";
import { useHistory } from "react-router-dom";
import {Rating} from "@mui/material";
import StarBorder from "@mui/icons-material/StarBorder";
import AxiosRestaurante from "../axios/AxiosRestaurante";

const UserMenu = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  let history = useHistory();
  const [rol, setRol] = useState(
    getSession() ? getSession().rol : history.push(rutas.iniciar_sesion)
  );
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const direcciones = () => {
    setAnchorEl(null);
    history.push(rutas.direcciones_cliente);
  };
  const finSesion = () => {
    endSession();
    history.push(rutas.iniciar_sesion);
  };
  const miPerfil = () => {
    setAnchorEl(null);
    history.push(rutas.perfil_cliente);
  };
  const inicioRestaurante = () => {
    setAnchorEl(null);
    history.push(rutas.raiz_restaurante);
  };
  const cambiarClave = () => {
    setAnchorEl(null);
    history.push(rutas.cambiar_clave);
  };

  const [cali, setCali] = useState(0);
  useEffect(() => {
    console.log(getSession())
    if (getSession() != null) {

      AxiosRestaurante.get(
        `/buscar/${getSession().email}`
      )
        .then(function (response) {
          //console.log(response.data);
          setCali(response.data.calificacion);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }, []);

  return (
    <React.Fragment>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Tooltip title="Account settings">
          <IconButton onClick={handleClick}>
            <AccountCircleIcon size="large" sx={{ color: "white" }} />
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem onClick={rol === "CLIENTE" ? miPerfil : inicioRestaurante}>
          <Avatar /> {getSession() ? getSession().nombre : null}
        </MenuItem>
        <Divider />
        {rol === "RESTAURANTE" ? (
        <MenuItem>
          Tu calificación 
          <Rating
            name="simple-controlled"
            value={cali}
            readOnly={true}
            emptyIcon={
              <StarBorder fontSize="inherit" style={{fill: "grey"}}/>
            }
          />
        </MenuItem>
        ) : null}
        {rol === "RESTAURANTE" ? (
          <Divider />
        ) : null}
        {rol === "CLIENTE" ? (
          <MenuItem onClick={direcciones}>
            <HomeIcon fontSize="small" sx={{ mr: 2 }} />
            Mis direcciones
          </MenuItem>
        ) : null}
        <MenuItem onClick={cambiarClave}>
          <VpnKeyIcon fontSize="small" sx={{ mr: 2 }} />
          Cambiar Clave
        </MenuItem>

        <MenuItem onClick={finSesion}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Salir
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
};
export default UserMenu;
