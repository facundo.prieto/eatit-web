import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
  Grid,
  Box,
  Typography,
  Rating,
  Button,
  Tooltip,
  IconButton,
} from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import DireccionesCliente from "./DireccionesCliente";
import AxiosCliente from "./../components/axios/AxiosCliente";
import ListaPedidos from "./../components/ListaPedidos";
import DeleteIcon from "@mui/icons-material/Delete";
import ModalBasic from "./../components/basicos/ModalBasic";
import Input from "./../components/basicos/Input";
import SearchIcon from "@mui/icons-material/Search";
import SyncIcon from "@mui/icons-material/Sync";

const PerfilCliente = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();
  const [abierto, setAbierto] = useState();
  const [loader, setLoader] = useState(false);
  const [data, setData] = useState([]);
  const [pedidos, setPedidos] = useState([]);
  const [openModalEliminar, setOpenModalEliminar] = React.useState(false);
  const [calificacion, setCalificacion] = useState(0);
  useEffect(() => {
    AxiosCliente.get(
      `/buscar/${getSession().email}`,
      {},
      {
        headers: { Authorization: `${getSession().token}` },
      }
    )
      .then(function (response) {
        console.log(response);
        setData(response.data);
        if (response.data.calificacion != null)
          setCalificacion(response.data.calificacion);
      })
      .catch(function (error) {
        console.log(error);
      });

    AxiosCliente.get(
      `/pedidos/listar/${getSession().email}`,
      {},
      {
        headers: { Authorization: `${getSession().token}` },
      }
    )
      .then(function (response) {
        console.log(response);
        setPedidos(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  const handleOpenModalEliminar = () => {
    setOpenModalEliminar(true);
  };

  const handleCloseModal = () => {
    setOpenModalEliminar(false);
  };

  const borrarCuenta = () => {
    AxiosCliente.delete(`${getSession().email}`, {
      headers: { Authorization: `${getSession().token}` },
    })
      .then(function (response) {
        console.log(response);
        setSeverity("success");
        setSnackbar(true);
        setMjeError("Su cuenta fue eliminada definitivamente");

        setTimeout(() => {
          history.push(rutas.iniciar_sesion);
        }, 3000);
      })
      .catch(function (error) {
        console.log(error);
        setSeverity("error");
        setSnackbar(true);
        setMjeError("Ocurrió un error en el sistema");
      });
  };

  //BARRA DE BUSQUEDA
  const [searchTerm, setSearchTerm] = useState("");
  console.log("el searchTerm es: " + searchTerm);
  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  let pedidosFiltrados = pedidos.filter(
    (item) =>
      item.idPedido.toString().toLowerCase().includes(searchTerm) ||
      item.idPedido.toString().includes(searchTerm) ||
      item.restauranteNombre.toLowerCase().includes(searchTerm) ||
      item.restauranteNombre.includes(searchTerm)
  );

  return (
    <>
      <NavBar />
      <Container maxWidth="lg">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={6}
              display="column"
              justifyContent="center"
              alignItems="center"
              sx={{ mb: 6 }}
            >
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
                sx={{ pl: 5, pr: 5 }}
              >
                <h1 align="center">Datos del cliente</h1>
                <h3 align="center">
                  {data.nombre} {data.apellido}
                </h3>
                <h5 align="center">{data.email}</h5>
              </Grid>
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
                align="center"
                sx={{ pl: 5, pr: 5 }}
              >
                <Typography component="legend" align="center">
                  Calificaci&oacute;n{" "}
                </Typography>
                <Rating
                  alignItems="center"
                  name="read-only"
                  value={calificacion}
                  readOnly
                />
              </Grid>

              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
                align="center"
                sx={{ pl: 5, pr: 5 }}
              >
                <Button
                  variant="contained"
                  color="error"
                  sx={{ mb: 2, mt: 2 }}
                  onClick={() => handleOpenModalEliminar()}
                >
                  <DeleteIcon />
                  Eliminar mi cuenta
                </Button>
              </Grid>
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
                sx={{ pl: 2, pr: 2 }}
              >
                <DireccionesCliente esPagina={false} />
              </Grid>
            </Grid>

            <Grid
              item
              xs={6}
              display="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ pl: 2, pr: 2 }}
              >
                <h1 align="center">Pedidos</h1>
              </Grid>

              <Grid item xs={12} display="flex">
                <Grid
                  item
                  xs={11}
                  display="flex"
                  justifyContent="left"
                  alignItems="left"
                >
                  <Input
                    fullWidth
                    id="buscar"
                    label="Buscar pedido"
                    value={searchTerm}
                    onChange={handleSearch}
                  />
                </Grid>
                <Grid
                  item
                  xs={1}
                  display="flex"
                  justifyContent="left"
                  alignItems="center"
                  sx={{ ml: -5 }}
                >
                  <Tooltip title="Busqueda por #Pedido o Restaurante">
                    <SearchIcon />
                  </Tooltip>
                </Grid>
              </Grid>
              <p></p>
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
              >
                <ListaPedidos data={pedidosFiltrados} />
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <ModalBasic
        open={openModalEliminar}
        onClose={handleCloseModal}
        content={
          <>
            {" "}
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              align="center"
            >
              Eliminar mi cuenta
            </Typography>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <p>Est&aacute; seguro que quiere eliminar su cuenta?</p>
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Button
                  variant="contained"
                  color="success"
                  sx={{ mb: 2, mr: 2 }}
                  onClick={handleCloseModal}
                >
                  Cancelar
                </Button>
                <Button
                  variant="contained"
                  color="error"
                  sx={{ mb: 2 }}
                  onClick={() => borrarCuenta()}
                >
                  Si, eliminar
                </Button>
              </Grid>
            </Grid>
          </>
        }
      />
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default PerfilCliente;
