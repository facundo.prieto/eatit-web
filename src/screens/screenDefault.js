import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
} from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";

const Default = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();
  const [abierto, setAbierto] = useState();
  const [loader, setLoader] = useState(false);
  const [data, setData] = useState([]);

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };
  return (
    <>
      <NavBar />
      <Container maxWidth="md"></Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default Default;
