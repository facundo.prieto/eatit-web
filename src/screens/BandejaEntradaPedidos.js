import React, { useState, useEffect } from "react";
import {
  Container,
  Grid,
  Box,
  IconButton,
  Button,
  Backdrop,
  Snackbar,
  Alert,
} from "@mui/material";
import NavBar from "../components/NavBar/NavBar";
import { getSession } from "./../components/auth/auth";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import colorCodes from "../assets/colors/Colors";
import Paper from "@mui/material/Paper";
import { styled, CircularProgress } from "@mui/material";
import { Link } from "react-router-dom";
import rutas from "./../assets/const/rutas";
import TablePedidos from "./../components/basicos/TablePedidos";
import SyncIcon from "@mui/icons-material/Sync";
import InicioRestaurante from "../components/InicioRestaurante";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const BandejaEntradaPedidos = () => {
  const [snackbar, setSnackbar] = useState(false);
  const [loader, setLoader] = useState(true);
  const [severity, setSeverity] = useState();
  const [pedidos, setPedidos] = useState([]);
  const [pedidosNuevos, setPedidosNuevos] = useState([]);
  const [pedidosConfirmados, setPedidosConfirmados] = useState([]);
  const [pedidosEnviados, setPedidosEnviados] = useState([]);
  const [mjeError, setMje] = useState("");
  const [state, setState] = useState({
    idRestaurante: "",
    nombreRestaurante: "",
    // precio: "",
    // descripcion: "",
  });

  const handleClose = () => {
    setSnackbar(false);
  };

  const [nuevosDatos, setNuevosDatos] = useState(true);
  const handleNuevosDatos = () => {
    setNuevosDatos(!nuevosDatos);
  };

  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }
  let email = "";
  if (getSession() != null && getSession().email != null) {
    // setState({email: getSession().email});
    email = getSession().email;
  }

  useEffect(() => {
    AxiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      let id = response.data.idRestaurante;
      setState({
        idRestaurante: response.data.idRestaurante,
        nombreRestaurante: response.data.nombre,
      });

      AxiosRestaurante.get(`/pedidos/listar/${id}`)
        .then(function (response) {
          console.log(response.data);
          console.log(response.status);
          if (response.status == 200) {
            console.log("status");
            setSeverity("success");
            let datos = response.data;
            let pedidosNuevosAux = [];
            let pedidosConfirmadosAux = [];
            let pedidosEnviadosAux = [];
            datos.map((m) => {
              if (m.estado === "CREADO") {
                pedidosNuevosAux.push(m);
              } else {
                if (m.estado === "CONFIRMADO") {
                  pedidosConfirmadosAux.push(m);
                } else {
                  if (m.estado === "EN_CAMINO") {
                    pedidosEnviadosAux.push(m);
                  }
                }
              }
              console.log("map");
            });
            setPedidosNuevos(pedidosNuevosAux);
            setPedidosConfirmados(pedidosConfirmadosAux);
            setPedidosEnviados(pedidosEnviadosAux);
            setPedidos(response.data);
            setLoader(false);
            console.log("Pedidos cargados!");
          } else {
            setSeverity("error");
            setPedidos(null);
            setMje(`No hay pedidos`);
          }
        })
        .catch(function (error) {
          console.log(error);
          setSeverity("error");
          setSnackbar(true);
          setMje(`Ocurrió un error en el sistema`);
          setLoader(false);
        });
    });
  }, [nuevosDatos]);

  const handleRefrescar = () => {
    handleNuevosDatos();
    AxiosRestaurante.get(`/pedidos/listar/${state.idRestaurante}`)
      .then(function (response) {
        console.log("AxiosRestaurante response: ");
        console.log(response.data);
        if (response.status == 200) {
          setSeverity("success");
          setPedidos(response.data);
          setLoader(false);
          console.log("Pedidos cargados!");
          console.log(pedidos);
        } else {
          setSeverity("error");
          setPedidos(null);
          setMje(`No hay pedidos`);
        }
      })
      .catch(function (error) {
        console.log(error);
        setSeverity("error");
        setSnackbar(true);
        setMje(`Ocurrió un error en el sistema`);
        setLoader(false);
      });

    if (pedidos.length !== 0) {
      let pedidosNuevosAux = [];
      let pedidosConfirmadosAux = [];
      let pedidosEnviadosAux = [];

      setPedidosNuevos([]);
      setPedidosConfirmados([]);
      setPedidosEnviados([]);
      pedidos
        .filter((data) => {
          if (data.estado === "FINALIZADO" || data.estado === "RECHAZADO") {
            return null;
          } else return data;
        })
        .map((m) => {
          if (m.estado === "CREADO") {
            pedidosNuevosAux.push(m);
          } else {
            if (m.estado === "CONFIRMADO") {
              pedidosConfirmadosAux.push(m);
            } else {
              if (m.estado === "EN_CAMINO") {
                pedidosEnviadosAux.push(m);
              }
            }
          }
        });
      setPedidosNuevos(pedidosNuevosAux);
      setPedidosConfirmados(pedidosConfirmadosAux);
      setPedidosEnviados(pedidosEnviadosAux);
    }
  };
  return (
    <>
      <NavBar />
      <Container maxWidth="xl">
        <Box bgcolor={colorCodes.backBox} sx={{ marginY: 6 }}>
          <Grid
            item
            xs={12}
            display="flex"
            justifyContent="center"
            alignItems="center"
            sx={{ pl: 3, pr: 3 }}
          >
            <Grid container justifyContent="left" sx={{ mt: 2 }}>
              <InicioRestaurante
                setSnackbar={setSnackbar}
                setSeverity={setSeverity}
                setLoader={setLoader}
                setMjeError={setMje}
              />
            </Grid>
            <Grid container justifyContent="right">
              <IconButton onClick={handleRefrescar} sx={{ color: "white" }}>
                <SyncIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            padding={3}
          >
            <Grid item xs={4}>
              <Item>
                <Paper
                  sx={{
                    width: "100%",
                    overflow: "hidden",
                    backgroundColor: "#c1c1c1",
                  }}
                >
                  <TablePedidos
                    data={pedidosNuevos}
                    estado={"NUEVOS"}
                    token={token}
                    handleRefrescar={handleRefrescar}
                    setMje={setMje}
                    setSeverity={setSeverity}
                    setSnackbar={setSnackbar}
                    handleNuevosDatos={handleNuevosDatos}
                  />
                </Paper>
              </Item>
            </Grid>
            <Grid item xs={4}>
              <Item>
                <Paper
                  sx={{
                    width: "100%",
                    overflow: "hidden",
                    backgroundColor: "#c1c1c1",
                  }}
                >
                  <TablePedidos
                    data={pedidosConfirmados}
                    estado={"CONFIRMADOS"}
                    token={token}
                    handleRefrescar={handleRefrescar}
                    setMje={setMje}
                    setSeverity={setSeverity}
                    setSnackbar={setSnackbar}
                    handleNuevosDatos={handleNuevosDatos}
                  />
                </Paper>
              </Item>
            </Grid>
            <Grid item xs={4}>
              <Item>
                <Paper
                  sx={{
                    width: "100%",
                    overflow: "hidden",
                    backgroundColor: "#c1c1c1",
                  }}
                >
                  <TablePedidos
                    data={pedidosEnviados}
                    estado={"ENVIADOS"}
                    token={token}
                    handleRefrescar={handleRefrescar}
                    setMje={setMje}
                    setSeverity={setSeverity}
                    setSnackbar={setSnackbar}
                    handleNuevosDatos={handleNuevosDatos}
                  />
                </Paper>
              </Item>
            </Grid>
          </Grid>
        </Box>
        <Grid container>
          <Grid item xs={12} justifyContent="right" display="flex">
            <Link to={rutas.listar_pedidos_rc}>
              <Item sx={{ backgroundColor: colorCodes.green, mr: 2 }}>
                <Button size="small" sx={{ color: "white" }}>
                  Ver Rechazados y entregados
                </Button>
              </Item>
            </Link>
          </Grid>
        </Grid>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default BandejaEntradaPedidos;
