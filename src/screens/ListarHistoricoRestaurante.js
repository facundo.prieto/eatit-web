import React, { useState, useEffect } from "react";
import NavBar from "../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
  Grid,
  Tooltip,
  IconButton,
  Box,
} from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "../assets/const/rutas";
import { useHistory } from "react-router-dom";
import ListaPedidosRestaurante from "../components/ListaPedidosRestaurante";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import colorCodes from "./../assets/colors/Colors";
import Input from "./../components/basicos/Input";
import SearchIcon from "@mui/icons-material/Search";
import SyncIcon from "@mui/icons-material/Sync";

const ListarHistoricoRestaurante = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMje] = useState("");
  const [severity, setSeverity] = useState();
  const [loader, setLoader] = useState(false);
  const [pedidos, setPedidos] = useState([]);
  const [state, setState] = useState({
    idRestaurante: "",
    nombreRestaurante: "",
  });

  const handleClose = () => {
    setSnackbar(false);
  };
  const [pedidosRechazados, setPedidosRechazados] = useState([]);
  const [pedidosFinalizados, setPedidosFinalizados] = useState([]);
  useEffect(() => {
    setLoader(true);
    AxiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      let id = response.data.idRestaurante;
      setState({
        idRestaurante: response.data.idRestaurante,
        nombreRestaurante: response.data.nombre,
      });

      AxiosRestaurante.get(`/pedidos/listar/${id}`)
        .then(function (response) {
          console.log(response.data);
          if (response.status == 200) {
            let pedidosRechazadosAux = pedidos.slice();
            let pedidosFinalizadosAux = pedidos.slice();

            response.data
              .filter((f) => {
                if (f.estado === "RECHAZADO") {
                  return f;
                } else if (f.estado === "FINALIZADO") {
                  return f;
                } else return null;
              })
              .map((m) => {
                if (m.estado === "RECHAZADO") {
                  pedidosRechazadosAux.push(m);
                  setPedidosRechazados(pedidosRechazadosAux);
                } else if (m.estado === "FINALIZADO") {
                  pedidosFinalizadosAux.push(m);
                  setPedidosFinalizados(pedidosFinalizadosAux);
                }
              });
            setSeverity("success");
            setPedidos(response.data);
            setLoader(false);
            console.log("Pedidos cargados!");
          } else {
            setSeverity("error");
            setPedidos(null);
            setMje(`No hay pedidos`);
            setLoader(false);
          }
        })
        .catch(function (error) {
          console.log(error);
          setSeverity("error");
          setSnackbar(true);
          setMje(`Ocurrió un error en el sistema`);
          setLoader(false);
        });
      setLoader(false);
    });
  }, [email]);

  const handleRefrescar = () => {
    setLoader(true);
    AxiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      let id = response.data.idRestaurante;
      setState({
        idRestaurante: response.data.idRestaurante,
        nombreRestaurante: response.data.nombre,
      });

      AxiosRestaurante.get(`/pedidos/listar/${id}`)
        .then(function (response) {
          console.log(response.data);
          if (response.status == 200) {
            let pedidosRechazadosAux = pedidos.slice();
            let pedidosFinalizadosAux = pedidos.slice();

            response.data
              .filter((f) => {
                if (f.estado === "RECHAZADO") {
                  return f;
                } else if (f.estado === "FINALIZADO") {
                  return f;
                } else return null;
              })
              .map((m) => {
                if (m.estado === "RECHAZADO") {
                  pedidosRechazadosAux.push(m);
                  setPedidosRechazados(pedidosRechazadosAux);
                } else if (m.estado === "FINALIZADO") {
                  pedidosFinalizadosAux.push(m);
                  setPedidosFinalizados(pedidosFinalizadosAux);
                }
              });
            setSeverity("success");
            setPedidos(response.data);
            setLoader(false);
            console.log("Pedidos cargados!");
          } else {
            setSeverity("error");
            setPedidos(null);
            setMje(`No hay pedidos`);
            setLoader(false);
          }
        })
        .catch(function (error) {
          console.log(error);
          setSeverity("error");
          setSnackbar(true);
          setMje(`Ocurrió un error en el sistema`);
          setLoader(false);
        });
      setLoader(false);
    });
  };

  //BARRA DE BUSQUEDA
  const [searchTerm, setSearchTerm] = useState("");
  console.log("el searchTerm es: " + searchTerm);
  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  let pedidosFiltrados = pedidos.filter(
    (item) =>
      item.idPedido.toString().toLowerCase().includes(searchTerm) ||
      item.idPedido.toString().includes(searchTerm) ||
      item.nombreCliente.toLowerCase().includes(searchTerm) ||
      item.nombreCliente.includes(searchTerm)
  );

  return (
    <>
      <NavBar />
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ pl: 2, pr: 2 }}
              >
                <h1 align="center">Historico de pedidos</h1>
                <Grid
                  item
                  xs={1}
                  display="flex"
                  justifyContent="right"
                  alignItems="center"
                >
                  <IconButton onClick={handleRefrescar} sx={{ color: "white" }}>
                    <SyncIcon />
                  </IconButton>
                </Grid>
              </Grid>
              <Grid item xs={12} display="flex">
                <Grid
                  item
                  xs={11}
                  display="flex"
                  justifyContent="left"
                  alignItems="left"
                >
                  <Input
                    fullWidth
                    id="buscar"
                    label="Buscar pedido"
                    value={searchTerm}
                    onChange={handleSearch}
                    setSearchTerm={setSearchTerm}
                  />
                </Grid>
                <Grid
                  item
                  xs={1}
                  display="flex"
                  justifyContent="left"
                  alignItems="center"
                >
                  <Tooltip title="Busqueda por #Pedido o Cliente">
                    <SearchIcon />
                  </Tooltip>
                </Grid>
                <Grid
                  item
                  xs={1}
                  display="flex"
                  justifyContent="left"
                  alignItems="center"
                ></Grid>
              </Grid>
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
              >
                <ListaPedidosRestaurante data={pedidosFiltrados} />
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListarHistoricoRestaurante;
