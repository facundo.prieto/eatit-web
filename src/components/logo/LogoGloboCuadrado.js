import * as React from "react";
import Grid from "@mui/material/Grid";
import logo from "../../assets/images/PennywiseLogoCuadrado.png";
import { textAlign } from "@mui/system";

const LogoGloboCuadrado = (props) => {
  return (
      <img src={logo} width="20" justify="left" alt="EatIT" />
  );
};

export default LogoGloboCuadrado;
