import React, { useState, useEffect } from "react";
import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  Box,
  Card,
  CardContent,
  CardMedia,
  CardActions,
  Typography,
  MenuItem,
  Button,
  Chip,
  Link,
} from "@mui/material";

import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import BackspaceIcon from "@mui/icons-material/Backspace";
import moment from "moment";
import { useHistory } from "react-router-dom";
import rutas from "../assets/const/rutas";

const ListaPedidosRestaurante = ({ data }) => {
  let history = useHistory();
  const [desde, setDesde] = useState(null);
  const [hasta, setHasta] = useState(null);
  const [selectedCliente, setSelectedRestaurante] = useState("");
  const [selectedTipoPago, setSelectedTipoPago] = useState("TODOS");
  const [clientesUsados, setClientesUsados] = useState([]);

  useEffect(() => {
    // uso esto para quedarme con los restaurantes que uso el cliente sin repetidos
    console.log(data);
    let clientes = [];
    data.map((value, key) => {
      clientes.push(value.nombreCliente);
    });

    let uniqueCliente = [...new Set(clientes)];

    setClientesUsados(uniqueCliente);
    //////////////////////////////////////////////////////////////////////////////
  }, [data]);

  const calcularCantidad = (menus) => {
    let array = [];
    let grouped = [];
    let salida = "";
    menus.map((val, k) => {
      array.push(val.titulo);
    });

    grouped = array.reduce((r, v, i, a) => {
      if (v === a[i - 1]) {
        r[r.length - 1].push(v);
      } else {
        r.push(v === a[i + 1] ? [v] : v);
      }
      return r;
    }, []);

    grouped.map((val, key) => {
      Array.isArray(val)
        ? (salida += val.length + " x " + val[0] + " ")
        : (salida += "1 x " + val + " ");
    });

    //console.log(salida);
    return salida;
  };

  const handleChangeSelectCliente = (event) => {
    console.log(event.target.value);
    setSelectedRestaurante(event.target.value);
  };

  const handleChangeSelectTipoPago = (event) => {
    setSelectedTipoPago(event.target.value);
  };

  const borrarFiltros = () => {
    setDesde(null);
    setHasta(null);
    setSelectedRestaurante("");
    setSelectedTipoPago("TODOS");
  };

  function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : "0" + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : "0" + day;

    return day + "/" + month + "/" + year;
  }

  const handleIrAlPedido = (id) => {
    localStorage.setItem("__detalle_pedido", id);
    history.push(rutas.detalle_pedido);
  };
  return (
    <>
      <Grid
        item
        xs={12}
        display="flex"
        justifyContent="center"
        alignItems="center"
        sx={{ ml: 14, mr: 1 }}
      >
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Grid
            item
            xs={6}
            display="column"
            justifyContent="center"
            alignItems="center"
          >
            <h4>Desde</h4>
            <DatePicker
              inputFormat="dd/MM/yyyy"
              value={desde}
              onChange={(newValue) => {
                setDesde(newValue);
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
        </LocalizationProvider>

        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Grid
            item
            xs={6}
            display="column"
            justifyContent="center"
            alignItems="center"
          >
            <h4>Hasta</h4>
            <DatePicker
              inputFormat="dd/MM/yyyy"
              value={hasta}
              onChange={(newValue) => {
                setHasta(newValue);
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
        </LocalizationProvider>
      </Grid>
      <Grid
        item
        xs={12}
        display="flex"
        justifyContent="center"
        alignItems="center"
        sx={{ mt: 2 }}
      >
        <Grid
          item
          xs={5}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ ml: 3, mr: 1 }}
        >
          <FormControl fullWidth error>
            <InputLabel id="demo-simple-select-label">Cliente</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectedCliente}
              label="Cliente"
              onChange={handleChangeSelectCliente}
              color="error"
            >
              {clientesUsados.map((option, key) => {
                return (
                  <MenuItem key={key} value={option}>
                    {option}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid
          item
          xs={5}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ ml: 3, mr: 1 }}
        >
          <FormControl fullWidth error>
            <InputLabel id="demo-simple-select-label">Tipo de Pago</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectedTipoPago}
              label="Tipo de Pago"
              onChange={handleChangeSelectTipoPago}
              color="error"
            >
              <MenuItem key={1} value="PAYPAL">
                PayPal
              </MenuItem>
              <MenuItem key={2} value="EFECTIVO">
                Efectivo
              </MenuItem>
              <MenuItem key={3} value="TODOS">
                Todos
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid
          item
          xs={1}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ ml: 3, mr: 2 }}
        >
          <Button variant="contained" color="error" onClick={borrarFiltros}>
            <BackspaceIcon sx={{ ml: 1 }} />
          </Button>
        </Grid>
      </Grid>
      {data
        .filter((val) => {
          let check = "";
          let from = "";
          let to = "";

          if (val.estado === "FINALIZADO" || val.estado === "RECHAZADO") {
            if (
              selectedCliente === "" &&
              selectedTipoPago === "TODOS" &&
              desde === null &&
              hasta === null
            ) {
              console.log(val.fecha);
              console.log(val);
              return val;
            } else if (
              selectedCliente === "" &&
              selectedTipoPago === "TODOS" &&
              desde !== null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              console.log(val.fecha);
              console.log(
                moment(val.fecha, "DD/MM/YYYY").isBetween(
                  moment(getFormattedDate(desde), "DD/MM/YYYY"),
                  moment(getFormattedDate(hasta), "DD/MM/YYYY"),
                  undefined,
                  []
                )
              );

              return val ? check.isBetween(from, to, undefined, []) : "";
            } else if (
              selectedCliente === "" &&
              selectedTipoPago === "TODOS" &&
              desde !== null &&
              hasta === null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");

              console.log(val ? check.isSameOrAfter(from, undefined) : "");

              return val ? check.isSameOrAfter(from, undefined) : "";
            } else if (
              selectedCliente === "" &&
              selectedTipoPago === "TODOS" &&
              desde === null &&
              hasta !== null
            ) {
              console.log("filtro porfecha");

              check = moment(val.fecha, "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              console.log(val ? check.isSameOrBefore(to, undefined) : "");

              return val ? check.isSameOrBefore(to, undefined) : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago === "TODOS" &&
              desde === null &&
              hasta === null
            ) {
              return val ? val.nombreCliente === selectedCliente : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago === "TODOS" &&
              desde !== null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.nombreCliente === selectedCliente &&
                    check.isBetween(from, to, undefined, [])
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago === "TODOS" &&
              desde !== null &&
              hasta === null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");

              return val
                ? val.nombreCliente === selectedCliente &&
                    check.isSameOrAfter(from, undefined)
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago === "TODOS" &&
              desde === null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.nombreCliente === selectedCliente &&
                    check.isSameOrBefore(to, undefined)
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago !== "TODOS" &&
              desde === null &&
              hasta === null
            ) {
              return val
                ? val.nombreCliente === selectedCliente &&
                    val.metodoPago === selectedTipoPago
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago !== "TODOS" &&
              desde !== null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.nombreCliente === selectedCliente &&
                    val.metodoPago === selectedTipoPago &&
                    check.isBetween(from, to, undefined, [])
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago !== "TODOS" &&
              desde !== null &&
              hasta === null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");

              return val
                ? val.nombreCliente === selectedCliente &&
                    val.metodoPago === selectedTipoPago &&
                    check.isSameOrAfter(from, undefined)
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedTipoPago !== "TODOS" &&
              desde === null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.nombreCliente === selectedCliente &&
                    val.metodoPago === selectedTipoPago &&
                    check.isSameOrBefore(to, undefined)
                : "";
            } else if (selectedCliente === "" && selectedTipoPago !== "TODOS") {
              return val ? val.metodoPago === selectedTipoPago : "";
            }
          } else return null;
        })
        .map((value, key) => (
          <Card sx={{ display: "flex", mt: 2, mr: 3, ml: 3 }}>
            <CardContent sx={{ flex: "1 0 auto" }}>
              <Typography component="div" variant="h5">
                {value.nombreCliente}{" "}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <Link
                  sx={{ cursor: "pointer" }}
                  underline="none"
                  onClick={() => handleIrAlPedido(value.idPedido)}
                >
                  Ver detalle #{value.idPedido}
                </Link>
              </Typography>
              <Typography variant="body2" color="text.secondary">
                {value.fecha} -{" "}
                {value.metodoPago === "EFECTIVO" ? "Efectivo" : "PayPal"} - ${" "}
                {value.monto}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                {calcularCantidad(value.menus)}
              </Typography>
            </CardContent>

            <CardActions disableSpacing align="center">
              <Typography component="div" variant="h5" align="center">
                {value.estado === "FINALIZADO" ? (
                  <Chip label="Entregado" color="success" />
                ) : (
                  <Chip label="Rechazado" color="error" />
                )}
              </Typography>
            </CardActions>
          </Card>
        ))}
    </>
  );
};

export default ListaPedidosRestaurante;
