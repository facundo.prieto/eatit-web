import React, { useState, useEffect } from "react";
import { List, CircularProgress } from "@mui/material";
import axiosRestaurante from "./axios/AxiosRestaurante";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";
import CardPromo2 from "../components/basicos/CardPromo2";
import { getSession } from "../components/auth/auth";
import CardMenu2 from "./basicos/CardMenu2";

const ListPromociones = (props) => {
  const {
    nuevosDatosMenu,
    handleNuevosDatosMenu,
    handleNuevosDatosPromocion,
    nuevosDatosPromocion,
    loader,
    setMje,
    handleSnackBar,
    setSeverity,
  } = props;

  let history = useHistory();
  const [state, setState] = useState({
    idRestaurante: "",
    imagen: "",
    nombre: "",
    calificacion: "",
    costoEnvio: "",
    direccion: {},
    horarios: [],
    ubicacion: {},
    cobertura: "",
    menu: [],
    categorias: [],
  });

  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );

  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }

  useEffect(() => {
    axiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      setState({
        idRestaurante: response.data.idRestaurante,
        imagen: response.data.imagen,
        nombre: response.data.nombre,
        calificacion: response.data.calificacion,
        costoEnvio: response.data.costoEnvio,
        direccion: response.data.direccion,
        horarios: response.data.horarios,
        ubicacion: response.data.ubicacion,
        cobertura: response.data.cobertura,
        menu: response.data.menu,
        email: response.data.email,
        categorias: response.data.categorias,
      });
    });
  }, [nuevosDatosMenu, nuevosDatosPromocion]);

  const [searchTerm, setSearchTerm] = useState("");
  const [menus, setPromociones] = useState([]);

  useEffect(() => {
    let promos = [];

    state.menu.forEach((m) => {
      if (!m.promocion) {
        promos.push(m);
      }
    });
    setPromociones(promos);
    handleNuevosDatosMenu();
  }, [state.menu]);

  let filteredListMenus = menus.filter(
    (item) =>
      item.titulo.toLowerCase().includes(searchTerm) ||
      item.titulo.includes(searchTerm) ||
      item.descripcion.toLowerCase().includes(searchTerm) ||
      item.descripcion.includes(searchTerm)
  );

  return (
    <>
      <List
        sx={{
          width: "100%",
          color: "black",
        }}
      >
        {!loader ? (
          !state.menu ? (
            "No tiene promociones cargadas en el sistema"
          ) : (
            <CardMenu2
              data={filteredListMenus}
              handleNuevosDatosMenu={handleNuevosDatosMenu}
              handleNuevosDatosPromocion={handleNuevosDatosPromocion}
              setMje={setMje}
              handleSnackBar={handleSnackBar}
              setSeverity={setSeverity}
            />
          )
        ) : (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <CircularProgress style={{ color: "red" }} />
          </div>
        )}
      </List>
    </>
  );
};

export default ListPromociones;
