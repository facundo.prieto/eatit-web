import React from "react";
import {
  List,
  ListItem,
  ListItemText,
  IconButton,
  Box,
  Typography,
  Modal,
  Grid,
  Alert,
  Snackbar,
  Button,
  ListItemSecondaryAction,
  CircularProgress,
} from "@mui/material";

import DeleteIcon from "@mui/icons-material/Delete";
import CreateIcon from "@mui/icons-material/Create";
import colorCodes from "./../assets/colors/Colors";
import Input from "./basicos/Input";
import { useForm } from "react-hook-form";
import axiosCliente from "./axios/AxiosCliente";
import { getSession } from "./auth/auth";

const ListDirecciones = (props) => {
  const { handleLoader, loader, data = [], handleNuevosDatos } = props;

  const { handleSubmit } = useForm();
  const [openModal, setOpenModal] = React.useState(false);
  const [snackbar, setSnackbar] = React.useState(false);
  const [openModalModificar, setOpenModalModificar] = React.useState(false);
  const [mjeError, setMjeError] = React.useState("");
  const [calle, setCalle] = React.useState();
  const [numero, setNumero] = React.useState();
  const [esquina, setEsquina] = React.useState();
  const [barrio, setBarrio] = React.useState();
  const [idDireccion, setIdDireccion] = React.useState();
  const [severity, setSeverity] = React.useState();

  const modificarDireccion = () => {
    handleLoader();
    axiosCliente
      .put(
        `/direccion/modificar`,
        {
          id: idDireccion,
          calle: calle,
          numero: numero,
          esquina: esquina,
          barrio: barrio,
        },
        {
          headers: { Authorization: `test` },
        }
      )
      .then(function (response) {
        console.log(response);

        handleNuevosDatos();
        setOpenModalModificar(false);
        setSeverity("success");
        setSnackbar(true);
        setMjeError(`La dirección se modificó correctamente`);
      })
      .catch(function (error) {
        console.log(error);
        setOpenModalModificar(false);
        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
      });
    handleLoader();
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: colorCodes.black,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const handleCalle = (event) => {
    setCalle(event.target.value);
  };
  const handleNumero = (event) => {
    setNumero(event.target.value);
  };
  const handleEsquina = (event) => {
    setEsquina(event.target.value);
  };
  const handleBarrio = (event) => {
    setBarrio(event.target.value);
  };

  const handleOpenBorrar = (id) => {
    handleLoader();
    let email = getSession().email;
    axiosCliente
      .delete(`/direccion/eliminar/${email}/${id}`, {
        headers: { Authorization: `test` },
      })
      .then(function (response) {
        console.log(response);

        handleNuevosDatos();
        setOpenModal(false);
        setSeverity("success");
        setSnackbar(true);
        setMjeError(`La dirección se eliminó correctamente`);
      })
      .catch(function (error) {
        console.log(error);
        setOpenModal(false);
        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
      });
    handleLoader();
  };
  const handleOpenModificar = (value) => {
    setIdDireccion(value.id);
    setCalle(value.calle);
    setNumero(value.numero);
    setEsquina(value.esquina);
    setBarrio(value.barrio);

    setOpenModalModificar(true);
  };
  const handleCloseModal = () => setOpenModal(false);
  const handleCloseModalModificar = () => setOpenModalModificar(false);
  const handleCloseSnackbar = () => setSnackbar(false);
  const handleBorrarDireccion = () => {
    setOpenModal(false);
    setSnackbar(true);
    setMjeError("La dirección se borro correctamente");
  };
  const handleModificarDireccion = () => {
    setOpenModalModificar(false);
    setSnackbar(true);
    setMjeError("La dirección se modificó correctamente");
  };

  return (
    <>
      <List
        sx={{
          width: "100%",
          ml: 6,
          mr: 6,
          pl: 4,
          pr: 4,
          bgcolor: "background.paper",
          color: "black",
        }}
      >
        {!loader ? (
          !data.length ? (
            "No tiene direcciones cargadas en el sistema"
          ) : (
            data.map((value) => (
              <ListItem key={value} disableGutters>
                <ListItemSecondaryAction>
                  <IconButton onClick={() => handleOpenModificar(value)}>
                    <CreateIcon />
                  </IconButton>
                  <IconButton onClick={() => handleOpenBorrar(value.id)}>
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
                <ListItemText
                  primary={`${value.calle} ${value.numero}`}
                  secondary={`Esquina: ${value.esquina} (${value.barrio})`}
                />
              </ListItem>
            ))
          )
        ) : (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <CircularProgress style={{ color: "red" }} />
          </div>
        )}
      </List>
      {/* Modal de eliminar */}
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Borrar direcci&oacute;n
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2 }}
            align="center"
          >
            &iquest;Esta seguro que desea borrar esta direccion?
          </Typography>
          <Grid
            item
            xs={12}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <Button
              size="small"
              variant="contained"
              color="error"
              sx={{ mb: 2, mt: 2, mr: 2 }}
              onClick={handleBorrarDireccion}
            >
              Si, continuar
            </Button>
            <Button
              size="small"
              variant="contained"
              color="inherit"
              sx={{ mb: 2, mt: 2, color: "black" }}
              onClick={handleCloseModal}
            >
              Cancelar
            </Button>
          </Grid>
        </Box>
      </Modal>
      {/* Modal de modificar */}
      <Modal
        open={openModalModificar}
        onClose={handleCloseModalModificar}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Modificar Direcci&oacute;n
          </Typography>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="calle"
                label="Calle"
                value={calle}
                onChange={handleCalle}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="numero"
                label="N&uacute;mero"
                value={numero}
                onChange={handleNumero}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="esquina"
                label="Esquina"
                value={esquina}
                onChange={handleEsquina}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="barrio"
                label="Barrio"
                value={barrio}
                name="barrio"
                onChange={handleBarrio}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={handleSubmit(() => modificarDireccion())}
              >
                <CreateIcon />
                Modificar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListDirecciones;
