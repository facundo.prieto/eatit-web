const RestauranteData =
    {
        abierto : true,
        aceptaPedidos : true,
        cobertura : 1200,
        costoEnvio : "35",
        direccion : {
            barrio : "Pocitos",
            calle : "Claudio Williman",
            esquina : "Dr. Jose Maria Montero",
            id : "1",
            numero : "532",
            
        },
        email : "mc@macdonalls.com",
        estado : "",
        calificacion : "4.2",
        horarios : [
            {
                dia : "Lunes",
                horaApertura : "08:00",
                horaCierre : "23:00"
            },
            {
                dia : "Martes",
                horaApertura : "08:00",
                horaCierre : "23:00"
            },
            {
                dia : "Miércoles",
                horaApertura : "08:00",
                horaCierre : "23:00"
            },
            {
                dia : "Jueves",
                horaApertura : "08:00",
                horaCierre : "23:00"
            },
            {
                dia : "Viernes",
                horaApertura : "08:00",
                horaCierre : "23:00"
            },
            {
                dia : "Sábado",
                horaApertura : "08:00",
                horaCierre : "23:00"
            },
            {
                dia : "Domingo",
                horaApertura : "",
                horaCierre : ""
            }
        ],
        imagen : "https://images.rappi.com.uy/restaurants_logo/willimanempanadas-1602271814708.png",
        nombre : "Williman Empanadas",
        solicitudRegistro: {
            comentarios : "",
            emailRestaurante : "",
            estadoRegistro : ""
        },
        ubicacion : {
            latitud : -6251776.963003827,
            longitud : -4152754.2864470812
        },
    }

export default RestauranteData;