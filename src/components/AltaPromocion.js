import React, { useState, useEffect } from "react";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import { Container, Grid, Box, Button } from "@mui/material";
import Input from "../components/basicos/Input";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";

const AltaPromocion = (props) => {
  const {
    datos,
    handleOpenModal,
    handleCloseModal,
    handleNuevosDatosMenu,
    setMje,
    handleSnackBar,
    setSeverity,
  } = props;

  const [state, setState] = useState({
    descripcion: "",
    descuento: "",
  });

  const handleChangeState = (event) => {
    setState((prevState) => ({
      ...prevState,
      [event.target.id]: event.target.value,
    }));
  };

  let history = useHistory();

  //LO NECESITO?
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const handleClick = () => {
    handleNuevosDatosMenu();
    console.log("datos menu: " + datos);
    if (state.descripcion && state.descuento) {
      console.log("descuento: " + state.descuento);
      console.log("descripcion: " + state.descripcion);
      if (state.descuento > 0 && state.descuento <= 100) {
        let dataPromocion = {
          descripcion: state.descripcion,
          descuento: state.descuento,
        };

        AxiosRestaurante.post(`/promocion/crear/${datos.id}`, dataPromocion, {
          headers: {
            Authorization: `${token}`,
          },
        })

          .then(function (response) {
            console.log(response);
            console.log(response.data);
            if (response.status == 200) {
              setMje(`La promoción se creó correctamente`);
              handleNuevosDatosMenu();
              handleOpenModal();
              setSeverity("success");
            } else {
              setMje(response.data);
              setSeverity("error");
              setMje("Ocurrio un error en el sistema");
            }
            handleNuevosDatosMenu();
          })
          .catch(function (error) {
            console.log("catch /promocion");
            setSeverity("error");
            setMje("Error en el servidor");
          });
      } else {
        setSeverity("error");
        setMje("El valor del descuento debe ser entre 1 y 100");
      }
      handleSnackBar();
      handleNuevosDatosMenu();
    } else {
      setSeverity("error");
      setMje("Los campos que tienen * son obligatorios");
      handleSnackBar();
    }
    handleCloseModal();
  };

  return (
    <>
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 4 }}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Agregar Promoci&oacute;n
          </Typography>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            ></Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="descripcion"
                label="Descripci&oacute;n *"
                value={state.descripcion}
                onChange={handleChangeState}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="descuento"
                label="Descuento (%) *"
                value={state.descuento}
                onChange={handleChangeState}
                variant="standard"
                type="number"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            ></Grid>

            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 6, mr: 6, mb: 2 }}
            ></Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            ></Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={handleClick}
              >
                <AddIcon />
                Agregar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default AltaPromocion;
