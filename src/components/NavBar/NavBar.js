import React, { useState, useEffect } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Menu from "@mui/material/Menu";
import { MenuItem } from "@mui/material";
import colorCodes from "../../assets/colors/Colors";
import LogoLetras from "./../logo/LogoLetras";
import { Link } from "react-router-dom";
import { getSession } from "../auth/auth";
import UserMenu from "./UserMenu";
import { useHistory } from "react-router-dom";
import rutas from "./../../assets/const/rutas";
import ModalPedidosEnCurso from "./../ModalPedidosEnCurso";

const NavBar = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [userRol, setUserRol] = useState(null);
  const open = Boolean(anchorEl);
  let history = useHistory();

  useEffect(() => {
    if (getSession() === null) {
      history.push(rutas.iniciar_sesion);
    } else {
      setUserRol(getSession().rol);
    }
  });
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        position="static"
        style={{ background: colorCodes.black, primary1Color: "#FFFFFF" }}
      >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            aria-controls="basic-menu"
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            {getSession() ? (
              getSession().rol === "ADMINISTRADOR" ? (
                <>
                  <Link to={rutas.crear_usuario_administrador}>
                    <MenuItem>Crear usuario administrador</MenuItem>
                  </Link>
                  <Link to={rutas.lista_usuarios}>
                    <MenuItem>Listar usuarios registrados</MenuItem>
                  </Link>
                  <Link to={rutas.aprobar_rechazar_restaurante}>
                    <MenuItem>Aprobar o rechazar restaurante</MenuItem>
                  </Link>
                  <Link to={rutas.visualizar_estadisticas}>
                    <MenuItem>Visualizar estadisticas</MenuItem>
                  </Link>
                </>
              ) : (
                ""
              )
            ) : (
              ""
            )}
            {getSession() ? (
              getSession().rol === "CLIENTE" ? (
                <>
                  <Link to={rutas.lista_restaurantes}>
                    <MenuItem>Restaurantes</MenuItem>
                  </Link>
                </>
              ) : (
                ""
              )
            ) : (
              ""
            )}
            {getSession() ? (
              getSession().rol === "RESTAURANTE" ? (
                <>
                  <Link to={rutas.bandeja_entrada_pedidos}>
                    <MenuItem>Bandeja de pedidos</MenuItem>
                  </Link>
                  <Link to={rutas.admin_promociones}>
                    <MenuItem>Menus y Promociones</MenuItem>
                  </Link>
                  <Link to={rutas.listar_historico_restaurante}>
                    <MenuItem>Histórico de pedidos</MenuItem>
                  </Link>
                  <Link to={rutas.listar_historico_reclamos}>
                    <MenuItem>Histórico de reclamos</MenuItem>
                  </Link>
                  <Link to={rutas.balance_restaurante}>
                    <MenuItem>Balance de ventas</MenuItem>
                  </Link>
                </>
              ) : (
                ""
              )
            ) : (
              ""
            )}
          </Menu>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <LogoLetras width="80" margin="0" />
          </Typography>
          <UserMenu />
        </Toolbar>
      </AppBar>
      {userRol === "CLIENTE" ? <ModalPedidosEnCurso /> : null}
    </Box>
  );
};

export default NavBar;
