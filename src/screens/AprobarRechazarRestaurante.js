import React, { useState, useEffect } from "react";
import colorCodes from "../assets/colors/Colors";
import Input from "../components/basicos/Input";
import {
  Container,
  CircularProgress,
  Grid,
  Box,
  Button,
  Alert,
  Snackbar,
  List,
  Backdrop,
  ListItem,
  ListItemText,
  Tabs,
  Tab,
  TabPanel,
} from "@mui/material";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import CardBasic from "../components/basicos/CardBasic.js";
import NavBar from "./../components/NavBar/NavBar";
import { getSession } from "../components/auth/auth";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";
import MapaRestaurante from "./../components/Maps/MapaRestaurante";

const AprobarRechazarRestaurante = () => {
  const [nuevosDatos, setNuevosDatos] = useState(true);
  const handleNuevosDatos = () => setNuevosDatos(!nuevosDatos);

  const [dataRestaurantes, setDataRestaurantes] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    setLoader(true);
    AxiosRestaurante.get(
      `/listar/full`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        console.log(response);
        if (response.data !== undefined) {
          let data = response.data.filter((val) => {
            return val.estado === "INACTIVO" ? val : "";
          });
          console.log(data);
          setDataRestaurantes(data);
        }
        setLoader(false);
      })
      .catch(function (error) {
        //console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema 1`);
        setLoader(false);
      });
    setLoader(false);
  }, [nuevosDatos]);

  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();

  let history = useHistory();
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  const aprobar = (email) => {
    console.log("el email: " + email);
    //setLoader(true);
    AxiosRestaurante.post(
      `/solicitud/aprobar/${email}`,
      {},

      {
        headers: { Authorization: `${token}` },
      }
    )

      .then(function (response) {
        handleNuevosDatos();
        console.log("que responde: " + response);
        setSeverity("success");
        setSnackbar(true);
        setMjeError(`Se aprobó el restaurante correctamente`);

        setLoader(false);
      })
      .catch(function (error) {
        //console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema 2`);
        setLoader(false);
      });
    setLoader(false);
  };

  const rechazar = (email) => {
    setLoader(true);
    AxiosRestaurante.post(
      `/solicitud/rechazar/${email}`,
      {},
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        handleNuevosDatos();
        console.log(response);
        setSeverity("success");
        setSnackbar(true);
        setMjeError(`Se rechazó el restaurante correctamente`);

        setLoader(false);
      })
      .catch(function (error) {
        //console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
    setLoader(false);
  };

  const [valueTab, setValueTab] = React.useState(0);

  const handeChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };
  const TabPanel = (props) => {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
      </div>
    );
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  return (
    <>
      <NavBar />
      <Container maxWidth="md">
        {!loader ? (
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <Tabs
                value={valueTab}
                onChange={handeChangeTab}
                aria-label="basic tabs example"
                textColor="error"
                indicatorColor="error"
              >
                {dataRestaurantes.length !== 0
                  ? dataRestaurantes.map((value, key) => (
                      <Tab label={value.nombre} {...a11yProps(key)} />
                    ))
                  : null}
              </Tabs>
            </Box>
            {dataRestaurantes.length !== 0 ? (
              dataRestaurantes.map((value, key) => (
                <TabPanel value={valueTab} index={key}>
                  <Grid container spacing={2}>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <h1>
                        <img
                          src={value.imagen}
                          style={{
                            display: "block",
                            width: "auto",
                            height: "100px",
                            border: "2px solid white",
                          }}
                        />
                      </h1>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="nombre-local"
                        label="Nombre del local"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.nombre}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="email"
                        label="Correo electronico"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.email}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="calle"
                        label="Calle"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.direccion.calle}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="numero"
                        label="Número"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.direccion.numero}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="esquina"
                        label="Esquina"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.direccion.esquina}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="barrio"
                        label="Barrio"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.direccion.barrio}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                      sx={{ mr: 6, ml: 6 }}
                    >
                      <h2>Ubicación del local</h2>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                      sx={{ mr: 6, ml: 6 }}
                    >
                      <MapaRestaurante
                        limite={value.cobertura}
                        posicion={[
                          parseInt(value.ubicacion.latitud),
                          value.ubicacion.longitud,
                        ]}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="cobertura"
                        type="number"
                        label="Distancia m&aacute;xima en metros"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.cobertura}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Input
                        id="costo_envio"
                        label="Costo de envio"
                        InputProps={{
                          readOnly: true,
                        }}
                        value={value.costoEnvio}
                        type="number"
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <h2>Horarios de atenci&oacute;n</h2>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                      sx={{ mr: 6, ml: 6 }}
                    >
                      <Grid item xs={12}>
                        {value.horarios.map((val) => (
                          <>
                            <List dense={true}>
                              <ListItem>
                                <ListItemText
                                  primary={
                                    val.horaCierre !== null &&
                                    val.horaCierre !== ""
                                      ? `${val.dia}, de ${val.horaApertura} a ${val.horaCierre}`
                                      : `${val.dia}, CERRADO`
                                  }
                                />
                              </ListItem>
                            </List>
                          </>
                        ))}
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                      sx={{ mr: 6, ml: 6 }}
                    >
                      <h2>Menús de ejemplo</h2>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Grid container spacing={2}>
                        {value.menu.map((val) => (
                          <Grid item xs={12}>
                            <CardBasic data={val} soloVista={true} />
                          </Grid>
                        ))}
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Button
                        variant="contained"
                        color="error"
                        sx={{ mb: 2 }}
                        onClick={() => rechazar(value.email)}
                      >
                        Rechazar
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <Button
                        variant="contained"
                        color="success"
                        sx={{ mb: 2 }}
                        onClick={() => aprobar(value.email)}
                      >
                        Aprobar
                      </Button>
                    </Grid>
                  </Grid>
                </TabPanel>
              ))
            ) : (
              <Grid container spacing={2}>
                <Grid
                  item
                  xs={12}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <h1>No hay restaurantes para revisar</h1>
                </Grid>
              </Grid>
            )}
          </Box>
        ) : (
          ""
        )}
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default AprobarRechazarRestaurante;
