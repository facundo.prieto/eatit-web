import React, { useState } from "react";
import {
  Button,
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Card,
  CardContent,
  Typography,
  Grid,
} from "@mui/material";
import AxiosPedido from "./../axios/AxiosPedido";
import AxiosRestaurante from "./../axios/AxiosRestaurante";
import rutas from "../../assets/const/rutas";
import { useHistory } from "react-router-dom";
import ModalBasic from "./ModalBasic";
import Input from "./../basicos/Input";
const TablePedidos = (props) => {
  let history = useHistory();

  const {
    data,
    estado,
    token,
    handleRefrescar,
    setMje,
    setSeverity,
    setSnackbar,
    handleNuevosDatos,
  } = props;

  const calcularCantidad = (menus) => {
    let array = [];
    let grouped = [];
    let salida = "";
    menus.map((val, k) => {
      array.push(val.titulo);
    });

    grouped = array.reduce((r, v, i, a) => {
      if (v === a[i - 1]) {
        r[r.length - 1].push(v);
      } else {
        r.push(v === a[i + 1] ? [v] : v);
      }
      return r;
    }, []);

    grouped.map((val, key) => {
      Array.isArray(val)
        ? (salida += val.length + " x " + val[0] + " ")
        : (salida += "1 x " + val + " ");
    });

    //console.log(salida);
    return salida;
  };
  const handleIrAlPedido = (id) => {
    localStorage.setItem("__detalle_pedido", id);
    history.push(rutas.detalle_pedido);
  };

  const registrarPago = (id) => {
    AxiosRestaurante.post(
      `/pedido/${id}/registrar-pago`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        //console.log(response);
        if (response.status == 200) {
          handleNuevosDatos();
          setMje(`Se ha registrado el pago para del pedido ${id}`);
          setSeverity("success");
          setSnackbar(true);
        } else {
          setMje(
            `Ocurrio un error al querer registrar un pago para el pedido ${id}`
          );
          setSeverity("error");
          setSnackbar(true);
        }
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          setMje(error);
          setSeverity("error");
          setSnackbar(true);
        } else {
          // Something happened in setting up the request that triggered an Error
          setMje(`Ocurrió un error en el sistema, intente nuevamente...`);
          setSeverity("error");
          setSnackbar(true);
        }
      });
  };

  const cambiarEstado = (id, estado) => {
    AxiosPedido.post(
      `${id}/estado/actualizar/${estado}?estado=${estado}&idPedido=${id}`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        //console.log(response);
        if (response.status == 200) {
          handleNuevosDatos();
          setMje(`El pedido ${id} ha pasado a estado ${estado}`);
          setSeverity("success");
          setSnackbar(true);
        } else {
          setMje(
            `Ocurrio un error al querer cambiar el estado a ${estado} para el pedido ${id}`
          );
          setSeverity("error");
          setSnackbar(true);
        }
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          setMje(error);
          setSeverity("error");
          setSnackbar(true);
        } else {
          // Something happened in setting up the request that triggered an Error
          setMje(`Ocurrió un error en el sistema, intente nuevamente...`);
          setSeverity("error");
          setSnackbar(true);
        }
      });
  };
  const [openModalConfirmar, setOpenModalConfirmar] = useState(false);
  const [idConfirmar, setIdConfirmar] = useState();
  const [tiempo, setTiempo] = useState(0);

  const handleCloseModal = () => {
    setOpenModalConfirmar(false);
  };

  const handleTiempo = (event) => {
    setTiempo(event.target.value);
  };

  const handleConfirmar = (id) => {
    setIdConfirmar(id);
    setOpenModalConfirmar(true);
  };
  const cambiarEstadoConfirmado = (id) => {
    AxiosPedido.post(
      `confirmar`,
      { idPedido: idConfirmar, tiempoEstimado: tiempo },
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        //console.log(response);
        if (response.status == 200) {
          handleNuevosDatos();
          setMje(`El pedido ${idConfirmar} ha pasado a estado CONFIRMADO`);
          setSeverity("success");
          setSnackbar(true);
          handleRefrescar();
        } else {
          setMje(
            `Ocurrio un error al querer cambiar el estado a CONFIRMADO para el pedido ${idConfirmar}`
          );
          setSeverity("error");
          setSnackbar(true);
        }
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          setMje("Ocurrio un error en el servidor");
          setSeverity("error");
          setSnackbar(true);
        } else {
          // Something happened in setting up the request that triggered an Error
          setMje(`Ocurrió un error en el sistema, intente nuevamente...`);
          setSeverity("error");
          setSnackbar(true);
        }
      });
    setOpenModalConfirmar(false);
  };

  return (
    <>
      <TableContainer sx={{ maxHeight: 440, minHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center">
                {estado} ( {data.length} )
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.length === 0 && <p>No hay pedidos para procesar</p>}
            {data.map((dataItem, key) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={key}>
                  <TableCell>
                    <Card sx={{ display: "flex" }}>
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          width: "100%",
                        }}
                      >
                        <CardContent>
                          <Typography
                            component="div"
                            variant="subtitle2"
                            color="error"
                          >
                            Pedido #{dataItem.idPedido}
                          </Typography>
                          <Typography component="div" variant="subtitle1">
                            {dataItem.nombreCliente}
                          </Typography>
                          <Typography
                            variant="subtitle2"
                            color="text.secondary"
                            component="div"
                          >
                            {dataItem.fecha}
                          </Typography>
                          <Typography
                            variant="subtitle2"
                            color="text.secondary"
                            component="div"
                          >
                            Direcci&oacute;n entrega: {dataItem.direccion}
                          </Typography>
                          <Typography
                            variant="caption"
                            color="text.secondary"
                            component="div"
                            sx={{ mt: 2 }}
                          >
                            {calcularCantidad(dataItem.menus)}
                          </Typography>
                          <Typography
                            variant="caption"
                            color="text.secondary"
                            component="div"
                            sx={{ mb: 2 }}
                          >
                            Comentarios:
                            {dataItem.comentarios}
                          </Typography>
                          {dataItem.estado === "CREADO" && (
                            <>
                              <Button
                                color="success"
                                size="small"
                                variant="contained"
                                fullWidth
                                onClick={() =>
                                  handleConfirmar(dataItem.idPedido)
                                }
                              >
                                Confirmar
                              </Button>

                              <Button
                                color="error"
                                size="small"
                                variant="contained"
                                fullWidth
                                onClick={() =>
                                  cambiarEstado(dataItem.idPedido, "RECHAZADO")
                                }
                              >
                                RECHAZAR
                              </Button>
                            </>
                          )}
                          {dataItem.estado === "CONFIRMADO" && (
                            <>
                              <Button
                                color="success"
                                size="small"
                                variant="contained"
                                fullWidth
                                onClick={() =>
                                  cambiarEstado(dataItem.idPedido, "EN_CAMINO")
                                }
                              >
                                EN CAMINO
                              </Button>
                              <Button
                                color="error"
                                size="small"
                                variant="contained"
                                fullWidth
                                onClick={
                                  () =>
                                    cambiarEstado(
                                      dataItem.idPedido,
                                      "RECHAZADO"
                                    ) // ESTADO CANCELADO NO EXISTE
                                }
                              >
                                CANCELAR
                              </Button>
                            </>
                          )}
                          {dataItem.estado === "EN_CAMINO" && (
                            <Button
                              color="success"
                              size="small"
                              variant="contained"
                              fullWidth
                              onClick={() =>
                                cambiarEstado(dataItem.idPedido, "FINALIZADO")
                              }
                            >
                              FINALIZADO
                            </Button>
                          )}

                          {dataItem.estado === "EN_CAMINO" &&
                            dataItem.metodoPago == "EFECTIVO" &&
                            !dataItem.estaPago && (
                              <Button
                                color="info"
                                size="small"
                                variant="contained"
                                fullWidth
                                onClick={() => registrarPago(dataItem.idPedido)}
                              >
                                REGISTRAR PAGO
                              </Button>
                            )}
                          {dataItem.estado === "RECHAZADO" && (
                            <Button
                              variant="contained"
                              color="success"
                              sx={{ mt: 2 }}
                              size="small"
                              onClick={() =>
                                handleIrAlPedido(dataItem.idPedido)
                              }
                            >
                              Ver detalle
                            </Button>
                          )}
                          {dataItem.estado === "FINALIZADO" && (
                            <Button
                              variant="contained"
                              color="success"
                              sx={{ mt: 2 }}
                              size="small"
                              onClick={() =>
                                handleIrAlPedido(dataItem.idPedido)
                              }
                            >
                              Ver detalle
                            </Button>
                          )}
                        </CardContent>
                      </Box>
                    </Card>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <ModalBasic
        open={openModalConfirmar}
        onClose={handleCloseModal}
        content={
          <>
            <p align="center">Indique el tiempo estimado de entrega</p>
            <Grid container>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ mb: 2 }}
              >
                <Input
                  id="tiempo"
                  label="Tiempo estimado"
                  value={tiempo}
                  name="tiempo"
                  onChange={handleTiempo}
                />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ ml: 4, mr: 4, mb: 4 }}
              >
                <Button
                  variant="contained"
                  color="error"
                  fullWidth
                  onClick={() => cambiarEstadoConfirmado()}
                >
                  Continuar
                </Button>
              </Grid>
            </Grid>
          </>
        }
      />
    </>
  );
};

export default TablePedidos;
