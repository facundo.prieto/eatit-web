import React, { useState, useEffect } from "react";
import {
  Box,
  Grid,
  IconButton,
  Card,
  Paper,
  styled,
  Snackbar,
  CardMedia,
  Alert,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import defaultImage from "../../assets/images/defaultImage.png";
import StarIcon from "@mui/icons-material/Star";
import { Link } from "react-router-dom";
import rutas from "./../../assets/const/rutas";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

const TablePedidosFinalizados = (props) => {
  const { data, estado } = props;
  const [snackbar, setSnackbar] = useState(false);
  const [severity, setSeverity] = useState();
  const [mje, setMje] = useState("");

  useEffect(() => {
    console.log(
      `Cantidad de Pedidos cargados en TablePedidosFinalizados: ` + data.length
    );
    console.log(data);
    data.forEach((m) => {
      console.log(`Pedido cargado en data: `);
      console.log(m.restauranteNombre);
    });
  });

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  return (
    <>
      <TableContainer sx={{ maxHeight: 440, minHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center">
                {estado === "RECHAZADOS" ? "RECHAZADOS" : ""}
                {estado === "CANCELADOS" ? "CANCELADOS" : ""}
                {estado === "ENTREGADOS" ? "ENTREGADOS" : ""}
                {estado === "FINALIZADOS" ? "FINALIZADOS" : ""}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((dataItem, key) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={key}>
                  <TableCell>
                    <Grid
                      container
                      rowSpacing={1}
                      columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                      direction="row"
                    >
                      <Grid item xs={10}>
                        <Link to={rutas.alta_menu}>
                          <Grid container>
                            <Grid item xs={3}>
                              <CardMedia
                                component="img"
                                sx={{
                                  width: 70,
                                  m: 0,
                                  height: 70,
                                  border: "1px solid black",
                                  borderRadius: 2,
                                }}
                                // image={value.imagen === "" ? defaultImage : value.imagen}
                                image={defaultImage}
                                alt="Imagen no disponible"
                              />
                            </Grid>
                            <Grid item xs={9}>
                              {dataItem.fecha}
                              <br />
                              {dataItem.metodoPago}
                            </Grid>
                          </Grid>
                        </Link>
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mje}
        </Alert>
      </Snackbar>
    </>
  );
};

export default TablePedidosFinalizados;
