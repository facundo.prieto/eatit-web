import * as React from "react";
import Grid from "@mui/material/Grid";
import logo from "../../assets/images/PennywiseLogoHilo.png";

const LogoGlobo = (props) => {
  const {width = "auto", height = "auto", margin = 6} = props;
  return (
    <Grid
      display="flex"
      justifyContent="center"
      alignItems="center"
      sx={{ mt: margin }}
    >
      <img src={logo} width={width} height={height} justify="center" alt="EatIT" />
    </Grid>
  );
};

export default LogoGlobo;
