import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import RecuperarContrasena from "./screens/RecuperarContrasena";
import CrearUsuarioCliente from "./screens/CrearUsuarioCliente";
import CrearUsuarioGoogle from "./screens/CrearUsuarioGoogle";
import CrearUsuarioRestaurante from "./screens/CrearUsuarioRestaurante";
import CrearUsuarioAdmin from "./screens/CrearUsuarioAdmin";
import Inicio from "./screens/Inicio";
import ListarUsuariosRegistrados from "./screens/ListarUsuariosRegistrados";
import IniciarSesion from "./screens/IniciarSesion";
import AltaMenu from "./screens/AltaMenu";
import Menu from "./screens/Menu";
import Restaurante from "./screens/Restaurante";
import DireccionesCliente from "./screens/DireccionesCliente";
import ListarRestaurantes from "./screens/ListarRestaurantes";
import BandejaEntradaPedidos from "./screens/BandejaEntradaPedidos";
import ListarPedidosRC from "./screens/ListarPedidosRC";
import Balance from "./screens/Balance";
import rutas from "./assets/const/rutas";
import AprobarRechazarRestaurante from "./screens/AprobarRechazarRestaurante";
import CrearPedido from "./screens/CrearPedido";
import PaymentOk from "./screens/PaymentOk";
import Promociones from "./screens/Promociones";
import PerfilCliente from "./screens/PerfilCliente";
import VisualizarEstadistica from "./screens/VisualizarEstadistica";
import DetallePedido from "./screens/DetallePedido";
import ListarHistoricoRestaurante from "./screens/ListarHistoricoRestaurante";
import ListarHistoricoReclamos from "./screens/ListarHistoricoReclamos";
import CambiarClave from "./screens/CambiarClave";

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route
            exact
            path={rutas.listar_historico_reclamos}
            render={({ props }) => <ListarHistoricoReclamos {...props} />}
          />
          <Route
            exact
            path={rutas.listar_historico_restaurante}
            render={({ props }) => <ListarHistoricoRestaurante {...props} />}
          />
          <Route
            exact
            path={rutas.detalle_pedido}
            render={({ props }) => <DetallePedido {...props} />}
          />
          <Route
            exact
            path={rutas.aprobar_rechazar_restaurante}
            render={({ props }) => <AprobarRechazarRestaurante {...props} />}
          />
          <Route
            exact
            path={rutas.lista_restaurantes}
            render={({ props }) => <ListarRestaurantes {...props} />}
          />
          <Route
            exact
            path={rutas.direcciones_cliente}
            render={({ props }) => <DireccionesCliente {...props} />}
          />
          <Route
            exact
            path={rutas.recuperar_contrasena}
            render={({ props }) => <RecuperarContrasena {...props} />}
          />
          <Route
            exact
            path={rutas.crear_usuario_cliente}
            render={({ props }) => <CrearUsuarioCliente {...props} />}
          />
          <Route
            exact
            path={rutas.crear_usuario_goolge}
            render={({ props }) => <CrearUsuarioGoogle {...props} />}
          />
          <Route
            exact
            path={rutas.crear_usuario_restaurante}
            render={({ props }) => <CrearUsuarioRestaurante {...props} />}
          />
          <Route
            exact
            path={rutas.crear_usuario_administrador}
            render={({ props }) => <CrearUsuarioAdmin {...props} />}
          />
          <Route
            exact
            path={rutas.lista_usuarios}
            render={({ props }) => <ListarUsuariosRegistrados {...props} />}
          />
          <Route
            exact
            path={rutas.iniciar_sesion}
            render={(props) => <IniciarSesion {...props} />}
          />
          <Route
            exact
            path={rutas.alta_menu}
            render={(props) => <AltaMenu {...props} />}
          />

          <Route
            exact
            path={rutas.crear_pedido}
            render={({ props }) => <CrearPedido {...props} />}
          />
          <Route
            exact
            path={rutas.payment_ok}
            render={({ props }) => <PaymentOk {...props} />}
          />
          <Route
            exact
            path={rutas.crear_pedido}
            render={({ props }) => <CrearPedido {...props} />}
          />
          <Route
            exact
            path={rutas.menu}
            render={(props) => <Menu {...props} />}
          />
          <Route
            exact
            path={rutas.restaurante}
            render={(props) => <Restaurante {...props} />}
          />
          <Route
            exact
            path={rutas.balance_restaurante}
            render={({ props }) => <Balance {...props} />}
          />
          <Route
            exact
            path={rutas.payment_ok}
            render={({ props }) => <PaymentOk {...props} />}
          />
          <Route
            exact
            path={rutas.admin_promociones}
            render={({ props }) => <Promociones {...props} />}
          />
          <Route
            exact
            path={rutas.perfil_cliente}
            render={({ props }) => <PerfilCliente {...props} />}
          />
          <Route
            exact
            path={rutas.visualizar_estadisticas}
            render={({ props }) => <VisualizarEstadistica {...props} />}
          />
          <Route
            exact
            path={rutas.bandeja_entrada_pedidos}
            render={(props) => <BandejaEntradaPedidos {...props} />}
          />
          <Route
            exact
            path={rutas.listar_pedidos_rc}
            render={(props) => <ListarPedidosRC {...props} />}
          />
          <Route
            exact
            path={rutas.cambiar_clave}
            render={(props) => <CambiarClave {...props} />}
          />
          <Route
            exact
            path={rutas.raiz}
            render={({ props }) => <Inicio {...props} />}
          />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
