import React, { useState, useEffect } from "react";
import NavBar from "../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
  Grid,
  Card,
  Button,
  CardContent,
  Typography,
  Box,
  Autocomplete
} from "@mui/material";
import BackspaceIcon from "@mui/icons-material/Backspace";
import { getSession } from "../components/auth/auth";
import rutas from "../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "../assets/colors/Colors";
import GrillaBalance from "../components/GrillaBalance";
import Input from "../components/basicos/Input";
import TextField from '@mui/material/TextField';
import DateRangePicker from '@mui/lab/DateRangePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import moment from 'moment'

const Balance = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();
  const [abierto, setAbierto] = useState();
  const [loader, setLoader] = useState(false);
  const [data, setData] = useState([]);
  const [value, setValue] = React.useState([null, null]);
  const [datosBalance, setDatosBalance] = useState([]);
  const [originalDatosBalance, setOriginalDatosBalance] = useState([]);
  const [datosGanancias, setDatosGanancias] = useState(0);
  const [datosPerdidas, setDatosPerdidas] = useState(0);

  useEffect(() => {

    let dateFormat = "YYYY-MM-DD";
    let emailResto = getSession().email;
    let defaultHasta = moment().format(dateFormat);
    let defaultDesde = moment().subtract(30, "days").format(dateFormat);

    let body = {
        desde: defaultDesde,
        hasta: defaultHasta,
        emailResto: emailResto
    }
    

    AxiosRestaurante.post(
      `/balance`,
      body,
      { headers: { Authorization: `${token}` } }
    ).then(function (response) {
        let dataBalance = response.data;
        let dataGanancias = dataBalance.filter((item) => item.balance > 0).map((item) => item.balance).reduce((accumulator, valor) => accumulator + valor);
        let dataPerdidas = dataBalance.filter((item) => item.balance < 0).map((item) => item.balance).reduce((accumulator, valor) => accumulator + valor);
        setDatosGanancias(dataGanancias);
        setDatosPerdidas(dataPerdidas);
        setDatosBalance(dataBalance);
        setOriginalDatosBalance(dataBalance);
        
    }).catch(function (error) {
        console.log(error);
    });

    console.log("defaultHasta");
    console.log(defaultHasta);

    console.log("defaultDesde");
    console.log(defaultDesde);

    console.log("emailResto");
    console.log(emailResto);

  }, []);

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  const [mailFiltro, setMailFiltro] = useState('');
  const [pagoFiltro, setPagoFiltro] = useState('TODOS');
  const [hastaFiltro, setHastaFiltro] = useState();
  const [desdeFiltro, setDesdeFiltro] = useState();

  const borrarFiltros = () => {
    setDatosBalance(originalDatosBalance);
    setMailFiltro('');
    setPagoFiltro("TODOS");
  };

  const onChangeEmailCliente = (event) => {
    let emailCliente = event.target.value;
    setMailFiltro(emailCliente)
    let filteredDatosBalance = originalDatosBalance
    if(pagoFiltro != 'TODOS') {
      filteredDatosBalance = filteredDatosBalance.filter((item) => item.metodo == pagoFiltro);
    }
    if(hastaFiltro != null && desdeFiltro != null) {
      let hastaMoment = moment(hastaFiltro);
      let desdeMoment = moment(desdeFiltro);
      filteredDatosBalance = filteredDatosBalance.filter((item) => {
        let fechaMoment = moment(item.fecha);
        return fechaMoment.isBefore(hastaMoment) && fechaMoment.isAfter(desdeMoment);
      })
    }

    if(emailCliente == '') {
      setDatosBalance(filteredDatosBalance);
    }else {
      filteredDatosBalance = filteredDatosBalance.filter((item) =>item.email.toLowerCase().includes(emailCliente.toLowerCase()));
      setDatosBalance(filteredDatosBalance);
    }
  }

  const onChangeMetodoPago = (event) => {
    let metodoPago = event.target.value;
    setPagoFiltro(metodoPago)
    let filteredDatosBalance = originalDatosBalance
    if(mailFiltro != '') {
      filteredDatosBalance = filteredDatosBalance.filter((item) =>item.email.toLowerCase().includes(mailFiltro.toLowerCase()));
    }
    if(hastaFiltro != null && desdeFiltro != null) {
      let hastaMoment = moment(hastaFiltro);
      let desdeMoment = moment(desdeFiltro);
      filteredDatosBalance = filteredDatosBalance.filter((item) => {
        let fechaMoment = moment(item.fecha);
        return fechaMoment.isBefore(hastaMoment) && fechaMoment.isAfter(desdeMoment);
      })
    }

    if(metodoPago == "TODOS") {
      setDatosBalance(filteredDatosBalance);
    }else {
      filteredDatosBalance = filteredDatosBalance.filter((item) => item.metodo == metodoPago);
      setDatosBalance(filteredDatosBalance);
    }
  }

  const onChangeDatePicker = (values) => {
    let hasta = values[1];
    let desde = values[0];

    setHastaFiltro(hasta);
    setDesdeFiltro(desde);

    if(desde != null && hasta != null) {

      let filteredDatosBalance = originalDatosBalance
      if(pagoFiltro != 'TODOS') {
        filteredDatosBalance = filteredDatosBalance.filter((item) => item.metodo == pagoFiltro);
      }
      if(mailFiltro != '') {
        filteredDatosBalance = filteredDatosBalance.filter((item) =>item.email.toLowerCase().includes(mailFiltro.toLowerCase()));
      }

      let hastaMoment = moment(hasta);
      let desdeMoment = moment(desde);
      filteredDatosBalance = filteredDatosBalance.filter((item) => {
        let fechaMoment = moment(item.fecha);
        return fechaMoment.isBefore(hastaMoment) && fechaMoment.isAfter(desdeMoment);
      })
      setDatosBalance(filteredDatosBalance);
    }
  }

  return (
    <>
      <NavBar />
      <Container maxWidth="md"></Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
      <Container maxWidth="md">
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6, p: 2 }}>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
              <h1>Balance de Ventas</h1>
            </Grid>
          </Grid>
          <Box sx={{ width: "100%" }}>
            <Grid container spacing={2}>
              <Grid
                  item
                  xs={12}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                <h2>Filtros</h2>
              </Grid>
            </Grid>  
            <Grid container
                xs={10}
                //</Box>backgroundColor="black"
                >
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateRangePicker
                  startText="Desde"
                  endText="Hasta"
                  value={value}
                  onChange={(newValue) => {
                    onChangeDatePicker(newValue);
                    setValue(newValue);
                  }}
                  renderInput={(startProps, endProps) => (
                    <React.Fragment>
                      <TextField {...startProps} 
                          sx={{
                            mr: 6, // margin right
                            ml: 6, // margin left
                            color: "white",
                            borderBottom: "1px solid white", // borde blanco
                            "& label.Mui-focused": {
                              color: "white",
                            },
                            "& .MuiInput-underline:after": {
                              borderBottomColor: "white",
                            },
                            "& .MuiOutlinedInput-root": {
                              "& fieldset": {
                                borderColor: "white",
                              },
                              "&:hover fieldset": {
                                borderColor: "white",
                              },
                              "&.Mui-focused fieldset": {
                                borderColor: "white",
                              },
                            },
                          }}
                          style={{marginLeft: "10rem", color: "white"}}
                      />
                      <Box sx={{ mx: 2 }}> a </Box>
                      <TextField 
                      {...endProps} 
                      sx={{
                        color: 'white',
                        mr: 6, // margin right
                        ml: 6, // margin left
                        borderBottom: "1px solid white", // borde blanco
                        "& label.Mui-focused": {
                          color: "white",
                        },
                        "& .MuiInput-underline:after": {
                          borderBottomColor: "white",
                        },
                        "& .MuiOutlinedInput-root": {
                          "& fieldset": {
                            borderColor: "white",
                          },
                          "&:hover fieldset": {
                            borderColor: "white",
                          },
                          "&.Mui-focused fieldset": {
                            borderColor: "white",
                          },
                        },
                      }}
                      
                      />
                    </React.Fragment>
                  )}
                />
              </LocalizationProvider>
            </Grid>  
            <Grid
                item
                xs={12}
                display="flex"
                justifyContent="flex-end"
                alignItems="center"
                //backgroundColor="white"
              >
                <Button variant="contained" color="error" onClick={borrarFiltros}>
                  <BackspaceIcon sx={{ ml: 1 }} />
                </Button>
            </Grid>
            <Grid container style={{marginTop: "2rem"}}>
              <Grid item xs={6}>
                <TextField label="Email Cliente" 
                    variant="standard"
                    onChange={onChangeEmailCliente}
                    style={{width: '18rem'}}
                    sx={{
                      color: 'white',
                      mr: 6, // margin right
                      ml: 6, // margin left
                      borderBottom: "1px solid white", // borde blanco
                      "& label.Mui-focused": {
                        color: "white",
                      },
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "white",
                      },
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          borderColor: "white",
                        },
                        "&:hover fieldset": {
                          borderColor: "white",
                        },
                        "&.Mui-focused fieldset": {
                          borderColor: "white",
                        },
                      },
                    }}

                  />
              </Grid>  
              <Grid item xs={6}>
                <Autocomplete
                  options={["TODOS","EFECTIVO", "PAYPAL"]}
                  id="disable-close-on-select"
                  onSelect={onChangeMetodoPago}
                  renderInput={(params) => (
                    <TextField {...params} label="Metodo de pago" 
                    variant="standard"
                    style={{width: '18rem'}}
                    sx={{
                      color: 'white',
                      mr: 6, // margin right
                      ml: 6, // margin left
                      borderBottom: "1px solid white", // borde blanco
                      "& label.Mui-focused": {
                        color: "white",
                      },
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "white",
                      },
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          borderColor: "white",
                        },
                        "&:hover fieldset": {
                          borderColor: "white",
                        },
                        "&.Mui-focused fieldset": {
                          borderColor: "white",
                        },
                      },
                    }}
                    />
                  )}
                />
              </Grid>  
            </Grid>  
          </Box>  
          <Box sx={{ width: "100%" }}>
            <GrillaBalance 
                datosBalance={datosBalance}
              />
          </Box>  
            <Grid container style={{marginTop: '2rem'}}>
              <Grid item xs={6}>
                <Card sx={{ maxWidth: 400 }} style={{backgroundColor: '#5BD466', color: 'white'}}>
                      <CardContent>
                        <Typography sx={{ fontSize: 18 }} gutterBottom>
                          Ganancias
                        </Typography>
                        <Typography variant="h3" component="div">
                          {datosGanancias} UYU
                        </Typography>
                      </CardContent>
                </Card>
              </Grid>  
              <Grid item xs={6}>
                <Card sx={{ maxWidth: 400 }} style={{backgroundColor: colorCodes.red, color: 'white'}}>
                      <CardContent>
                        <Typography sx={{ fontSize: 18 }} gutterBottom>
                          Perdidas
                        </Typography>
                        <Typography variant="h3" component="div">
                          {datosPerdidas} UYU
                        </Typography>
                      </CardContent>
                </Card>
              </Grid> 
            </Grid> 
          </Box>
        </Container>
    </>
  );
};

export default Balance;
