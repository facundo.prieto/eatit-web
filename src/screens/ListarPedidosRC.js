import React, { useState, useEffect } from "react";
import {
  Container,
  Grid,
  Box,
  Link,
  Button,
  Paper,
  styled,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
} from "@mui/material";
import NavBar from "../components/NavBar/NavBar";
import { getSession } from "../components/auth/auth";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import colorCodes from "../assets/colors/Colors";
import TablePedidos from "../components/basicos/TablePedidos";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const ListarPedidosRC = (props) => {
  let history = useHistory();
  const [snackbar, setSnackbar] = useState(false);
  const [loader, setLoader] = useState(true);
  const [severity, setSeverity] = useState();
  const [pedidos, setPedidos] = useState([]);
  const [pedidosRechazados, setPedidosRechazados] = useState([]);
  const [pedidosFinalizados, setPedidosFinalizados] = useState([]);
  const [mje, setMje] = useState("");
  const [state, setState] = useState({
    idRestaurante: "",
    nombreRestaurante: "",
    // precio: "",
    // descripcion: "",
  });

  const handleClose = () => {
    setSnackbar(false);
  };
  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }
  let email = "";
  if (getSession() != null && getSession().email != null) {
    // setState({email: getSession().email});
    email = getSession().email;
  }

  const handleVolver = () => {
    history.push(rutas.bandeja_entrada_pedidos);
  };

  useEffect(() => {
    AxiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      let id = response.data.idRestaurante;
      setState({
        idRestaurante: response.data.idRestaurante,
        nombreRestaurante: response.data.nombre,
      });

      AxiosRestaurante.get(`/pedidos/listar/${id}`)
        .then(function (response) {
          console.log(response.data);
          if (response.status == 200) {
            let pedidosRechazadosAux = pedidos.slice();
            let pedidosFinalizadosAux = pedidos.slice();

            response.data
              .filter((f) => {
                if (f.estado === "RECHAZADO") {
                  return f;
                } else if (f.estado === "FINALIZADO") {
                  return f;
                } else return null;
              })
              .map((m) => {
                if (m.estado === "RECHAZADO") {
                  pedidosRechazadosAux.push(m);
                  setPedidosRechazados(pedidosRechazadosAux);
                } else if (m.estado === "FINALIZADO") {
                  pedidosFinalizadosAux.push(m);
                  setPedidosFinalizados(pedidosFinalizadosAux);
                }
              });
            setSeverity("success");
            setPedidos(response.data); // sin esto no anda
            setLoader(false);
            console.log("Pedidos cargados!");
          } else {
            setSeverity("error");
            setPedidos(null);
            setMje(`No hay pedidos`);
          }
        })
        .catch(function (error) {
          console.log(error);
          setSeverity("error");
          setSnackbar(true);
          setMje(`Ocurrió un error en el sistema`);
          setLoader(false);
        });
    });
  }, [email]);

  return (
    <>
      <NavBar />
      <Container maxWidth="xl">
        <Box bgcolor={colorCodes.backBox} sx={{ marginY: 6 }}>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            padding={3}
          >
            <Grid item xs={6}>
              <Item>
                <Paper
                  sx={{
                    width: "100%",
                    overflow: "hidden",
                    backgroundColor: "#c1c1c1",
                  }}
                >
                  <TablePedidos
                    data={pedidosRechazados}
                    estado={"RECHAZADOS"}
                  />
                </Paper>
              </Item>
            </Grid>
            <Grid item xs={6}>
              <Item>
                <Paper
                  sx={{
                    width: "100%",
                    overflow: "hidden",
                    backgroundColor: "#c1c1c1",
                  }}
                >
                  <TablePedidos
                    data={pedidosFinalizados}
                    estado={"FINALIZADOS"}
                  />
                </Paper>
              </Item>
            </Grid>
          </Grid>
        </Box>
        <Grid container>
          <Grid item xs={12} justifyContent="right" display="flex">
            <Link to={rutas.bandeja_entrada_pedidos}>
              <Item sx={{ backgroundColor: colorCodes.green, mr: 2 }}>
                <Button
                  size="small"
                  sx={{ color: "white" }}
                  onClick={() => {
                    handleVolver();
                  }}
                >
                  Ir a Bandeja de entrada
                </Button>
              </Item>
            </Link>
          </Grid>
        </Grid>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mje}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListarPedidosRC;
