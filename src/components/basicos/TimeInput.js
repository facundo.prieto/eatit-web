import { TextField } from "@mui/material";
import React from "react";

const TimeInput = (props) => {
  const { id, type = "time", defaultValue, onChange } = props;
  const style = {
    borderBottom: "1px solid white",
    backgroundColor: "white",
    color: "white",
    "& .MuiFormControlLabel-label": {
      color: "white",
    },
    "& label.Mui-focused": {
      color: "black",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "white",
    },
    "& .MuiOutlinedInput-input": {
      color: "black",
    },
    "& .MuiOutlinedInput-root": {
      "&:hover fieldset": {
        borderColor: "white",
      },
      "&.Mui-focused fieldset": {
        borderColor: "white",
      },
    },
  };

  return (
    <TextField
      id={id}
      fullWidth // todo el ancho
      type={type}
      defaultValue={defaultValue}
      value={defaultValue}
      InputLabelProps={{
        shrink: true,
      }}
      inputProps={{
        step: 300, // 5 min
      }}
      onChange={onChange}
      sx={style}
    />
  );
};

export default TimeInput;
