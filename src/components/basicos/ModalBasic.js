import { React, useEffect, useState } from "react";
import { Modal, Box } from "@mui/material";
import colorCodes from "../../assets/colors/Colors";

const ModalBasic = (props) => {
  const { open, onClose, content } = props;
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 800,
    bgcolor: colorCodes.black,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>{content}</Box>
    </Modal>
  );
};

export default ModalBasic;
