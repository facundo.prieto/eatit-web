import React, { useState } from "react";
import { Container, Grid, Box, Button, Alert, Snackbar } from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import axiosCliente from "../components/axios/AxiosCliente";
import axiosUsuario from "../components/axios/AxiosUsuario";
import LogoLetras from "../components/logo/LogoLetras";
import Input from "./../components/basicos/Input";
import { setSession } from "../components/auth/auth";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";

const CrearUsuarioCliente = ({ navigation }) => {
  const [alertaError, setAlertaError] = useState(false);
  const [severity, setSeverity] = useState("");
  const [msgErr, setMsgErr] = useState("");

  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [email, setEmail] = useState("");
  const [contrasena, setContrasena] = useState("");
  const [repcontrasena, setRepContrasena] = useState("");

  const handleNombre = (event) => setNombre(event.target.value);
  const handleApellido = (event) => setApellido(event.target.value);
  const handleEmail = (event) => setEmail(event.target.value);
  const handleContrasena = (event) => setContrasena(event.target.value);
  const handleRepContrasena = (event) => setRepContrasena(event.target.value);

  let history = useHistory();

  const handleComenzar = (event) => {
    //Controlo que se haya cargado la misma contraseña dos veces
    if (contrasena !== repcontrasena) {
      setAlertaError(true);
      setSeverity("error");
      setMsgErr("Debe ingresar la misma contraseña dos veces");
    } else {
      //Controlo que se hayan cargado todos los datos
      if (
        nombre === "" ||
        apellido === "" ||
        email === "" ||
        contrasena === ""
      ) {
        //Si se registra con google no controlo la contraseña
        setAlertaError(true);
        setSeverity("error");
        setMsgErr("Debe completar todos los datos solicitados");
      } else {
        //Mando los datos del state al crearUsuarioCliente del ClienteController
        let token = "";
        axiosCliente
          .post(
            "/crear",
            {
              nombre: nombre,
              apellido: apellido,
              email: email,
              password: contrasena,
              direcciones: [],
            },
            {
              headers: {
                Authorization: `${token}`,
              },
            }
          )
          .then(function (response) {
            //console.log(response);
            if (response.status == 200) {
              setAlertaError(true);
              setSeverity("success");
              setMsgErr(response.data);

              axiosUsuario
                .post("/login", {
                  email: email,
                  password: contrasena,
                })
                .then(function (response) {
                  if (response.status == 200) {
                    setSession({
                      nombre: response.data.nombre,
                      email: response.data.email,
                      token: response.data.token,
                      direcciones: response.data.direcciones,
                      rol: response.data.rol,
                    });
                    history.push(rutas.direcciones_cliente);
                  } else {
                    setAlertaError(true);
                    setSeverity("error");
                    setMsgErr(response.data);
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
            } else {
              setAlertaError(true);
              setSeverity("error");
              setMsgErr(response.data);
            }
          })
          .catch(function (error) {
            if (error.response) {
              // Request made and server responded
              setAlertaError(true);
              setSeverity("error");
              setMsgErr(error.response.data);
            } else {
              // Something happened in setting up the request that triggered an Error
              setAlertaError(true);
              setSeverity("error");
              setMsgErr(error.message);
            }
          });
      }
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAlertaError(false);
    setSeverity("error");
    setMsgErr("");
  };

  return (
    <>
      <Container maxWidth="md">
        <LogoLetras width="200" />
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            {/* Título */}
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Bienvenido a EatIT</h1>
            </Grid>

            {/* Campos */}
            <>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input id="nombre" label="Nombre" onChange={handleNombre} />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="apellido"
                  label="Apellido"
                  onChange={handleApellido}
                />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="email"
                  type="email"
                  label="Correo electrónico"
                  onChange={handleEmail}
                />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="contrasena"
                  label="Contraseña"
                  type="password"
                  onChange={handleContrasena}
                />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="repcontrasena"
                  label="Repetir contraseña"
                  type="password"
                  onChange={handleRepContrasena}
                />
              </Grid>
            </>

            {/* Botón */}
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="error"
                sx={{ mb: 2, my: 4 }}
                onClick={handleComenzar}
              >
                COMENZAR
              </Button>
            </Grid>
          </Grid>
        </Box>

        <Snackbar
          open={alertaError}
          autoHideDuration={6000}
          onClose={handleClose}
        >
          <Alert
            onClose={handleClose}
            severity={severity}
            sx={{ width: "100%" }}
          >
            {msgErr}
          </Alert>
        </Snackbar>
      </Container>
    </>
  );
};

export default CrearUsuarioCliente;
