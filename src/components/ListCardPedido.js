import React, { useState, useEffect } from "react";

import defaultImage from "./../assets/images/defaultImage.png";
import {
  CardContent,
  Typography,
  Card,
  CardActions,
  Button,
  Grid,
  List,
  ListItem,
  ListItemText,
  Divider,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
} from "@mui/material";
import colorCodes from "./../assets/colors/Colors";
import { getSession } from "../components/auth/auth";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";
import AxiosPedido from "./axios/AxiosPedido";
import { loadScript } from "@paypal/paypal-js";
import ModalBasic from "./basicos/ModalBasic";

const ListCardPedido = (props) => {
  let history = useHistory();
  const {
    data,
    costoTotal = 0,
    comentarios = "",
    menus = [],
    emailRestaurante = "",
    direccionEnvio = "",
    costoEnvio = 0,
  } = props;
  const [alertaError, setAlertaError] = useState(false);
  const [severity, setSeverity] = useState("");
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");

  const [loader, setLoader] = useState(false);
  const [emailCliente, setEmailCliente] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [arrayMenus, setArrayMenus] = useState([]);
  useEffect(() => {
    //console.log("useEffect:", menus);
    let arrayIds = new Array();
    if (menus.length !== 0) {
      menus.map((value, key) => {
        //console.log("map: ", value);
        if (value.cantidad === undefined) {
          arrayIds.push(value.id);
        } else {
          for (let i = 0; i < parseInt(value.cantidad); i++)
            arrayIds.push(value.id);
        }
      });
      //console.log(arrayIds);
      setArrayMenus(arrayIds);
    }
  }, [menus]);
  //console.log(arrayMenus);
  const [openModal, setOpenModal] = React.useState(false);
  const handleOpen = () => {
    cargarDataEnStorage();
    setOpenModal(true);
  };
  const handleCloseModal = () => setOpenModal(false);

  useEffect(() => {
    loadScript({
      "client-id":
        "AZJPuw1X4UhqZTO7P5Oqztmm9fgFPI5QQEIRPJxYV1OfSfFaD8zhu1M7cbCyv4qHaJYPxtgsABgqMWAS",
    })
      .then((paypal) => {
        paypal
          .Buttons({
            createOrder: function (data, actions) {
              return actions.order.create({
                purchase_units: [
                  {
                    amount: {
                      value: localStorage.getItem("__montoPaypal"),
                    },
                  },
                ],
              });
            },
            onApprove: function (data, actions) {
              // This function captures the funds from the transaction.
              return actions.order.capture().then(function (details) {
                // This function shows a transaction success message to your buyer.

                window.location.href = "/payment-ok";
              });
            },
          })
          .render("#paypal-container")
          .catch((error) => {
            console.error("failed to render the PayPal Buttons", error);
          });
      })
      .catch((error) => {
        console.error("failed to load the PayPal JS SDK script", error);
      });
  }, [openModal]);

  const cargarDataEnStorage = () => {
    let dataEnviar = {
      comentarios: comentarios,
      emailCliente: emailCliente,
      emailRestaurante: emailRestaurante,
      estaPago: true,
      menus: arrayMenus,
      metodoPago: "PAYPAL",
      monto: costoTotal + costoEnvio,
      tiempoEstimado: 0,
      direccion: direccionEnvio,
    };
    localStorage.setItem("__pedido_realizado", JSON.stringify(dataEnviar));
  };
  const handleClose = (event, reason) => {
    setAlertaError(false);
    setSeverity("error");
    setMjeError("");
  };
  const realizarPedido = () => {
    setLoader(true);

    let dataEnviar = {
      comentarios: comentarios,
      emailCliente: emailCliente,
      emailRestaurante: emailRestaurante,
      estaPago: false,
      menus: arrayMenus,
      metodoPago: "EFECTIVO",
      monto: costoTotal + costoEnvio,
      tiempoEstimado: 0,
      direccion: direccionEnvio,
    };

    AxiosPedido.post(`/crear/`, dataEnviar, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        if (response.status == 200) {
          //console.log(response);
          setSeverity("success");
          setSnackbar(true);
          setMjeError(`El pedido fue realizado`);

          setTimeout(() => {
            history.push(rutas.lista_restaurantes);
          }, 3000);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
        }
      })
      .catch(function (error) {
        console.log(error);

        setLoader(false);
      });
    setLoader(false);
  };
  return (
    <>
      <Card sx={{ minWidth: 275 }}>
        <CardContent>
          <Grid display="flex" justifyContent="center" align="center">
            <img
              src={data.imagen === "" ? defaultImage : data.imagen}
              width="100"
              height="100"
              justify="center"
              alt="EatIT"
            />
          </Grid>
          <Typography variant="h5" component="div">
            {data.nombre}
          </Typography>
          <List
            sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
            alignItems="center"
          >
            <ListItem secondaryAction={<p>$ {costoTotal}</p>} disablePadding>
              <ListItemText primary={"Subtotal"} />
            </ListItem>
            {data.costoTotal === 0 ? null : (
              <ListItem secondaryAction={<p>$ {costoEnvio}</p>} disablePadding>
                <ListItemText primary={"Costo de envío"} />
              </ListItem>
            )}
            <Divider />
            <ListItem
              secondaryAction={<b>$ {costoTotal + costoEnvio}</b>}
              disablePadding
            >
              <ListItemText primary={"Total"} />
            </ListItem>
          </List>
        </CardContent>
        <CardActions>
          <Button
            onClick={handleOpen}
            variant="contained"
            color="error"
            fullWidth
          >
            Continuar
          </Button>
          <ModalBasic
            open={openModal}
            onClose={handleCloseModal}
            content={
              <>
                <h1 align="center">M&eacute;todo de pago</h1>
                <Grid container>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ ml: 4, mr: 4, mb: 4 }}
                  >
                    <Button
                      variant="contained"
                      color="error"
                      fullWidth
                      onClick={() => realizarPedido()}
                    >
                      Pagar Efectivo
                    </Button>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ ml: 4, mr: 4, mb: 4 }}
                  >
                    <div id="paypal-container"></div>
                  </Grid>
                </Grid>
              </>
            }
          />
        </CardActions>
      </Card>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListCardPedido;
