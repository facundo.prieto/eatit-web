import React from "react";

import { AppBar, Container, Typography, Toolbar } from "@mui/material";
import colorCodes from "./../../assets/colors/Colors";
import LogoLetras from "./../logo/LogoLetras";

export default function Footer() {
  return (
    <AppBar
      position="static"
      sx={{
        backgroundColor: colorCodes.black,
        position: "absolute",
        bottom: "0",
        mt: 4,
      }}
    >
      <Container maxWidth="md">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <LogoLetras width="80" margin="0" />
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
