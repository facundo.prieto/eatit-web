import React, { Component } from "react";
import {
  CircularProgress,
  Grid,
  Box,
  Alert,
  Snackbar,
  Container,
  Tabs,
  Tab,
  Typography,
} from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import ListaUsuarios from "../components/ListaUsuarios";
import ListaRestaurantes from "../components/ListaRestaurantes";
import PropTypes from "prop-types";
import NavBar from "./../components/NavBar/NavBar";
import PersonIcon from "@mui/icons-material/Person";
import HomeIcon from "@mui/icons-material/Home";

import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import AxiosCliente from "../components/axios/AxiosCliente";
import { getSession } from "./../components/auth/auth";

import ListaUsuariosData from "../assets/data/ListaUsuariosData";
import ListaRestaurantesData from "../assets/data/ListaRestaurantesData";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

class ListarUsuariosRegistrados extends Component {
  constructor() {
    super();
    this.state = {
      listaUsuariosData: ListaUsuariosData,
      listaRestaurantesData: ListaRestaurantesData,
      clientesCargados: false,
      restaurantesCargados: false, //PARA VER LOS DATOS DE PRUEBA ESTO VA EN FALSE Y COMENTAR EL componentDidMount()
      value: 0,
      alertaError: false,
      severity: "",
      msgErr: "",
    };
  }

  cargarRestaurantes = () => {
    //console.log("cargarRestaurantes");
    var t = this;
    let token = "";
    if (getSession() != null && getSession().token != null) {
      token = getSession().token;
    }
    this.setState({ restaurantesCargados: false });
    AxiosRestaurante.get("/listar", {
      headers: {
        Authorization: `${token}`,
      },
    })
      .then(function (response) {
        //console.log(response.status);
        if (response.status == 200) {
          t.setState({
            listaRestaurantesData: response.data,
            restaurantesCargados: true,
          });
        } else {
          t.setState({
            alertaError: true,
            severity: "error",
            msgErr: response.data,
          });
        }
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          t.setState({
            alertaError: true,
            severity: "error",
            msgErr: error.response.data,
          });
        } else {
          // Something happened in setting up the request that triggered an Error
          t.setState({
            alertaError: true,
            severity: "error",
            msgErr: error.message,
          });
        }
      });
  };

  cargarCliente = () => {
    var t = this;
    let token = "";
    if (getSession() != null && getSession().token != null) {
      token = getSession().token;
    }
    this.setState({ clientesCargados: false });
    AxiosCliente.get("/listar", {
      headers: {
        Authorization: `${token}`,
      },
    })
      .then(function (response) {
        //console.log(response.status);
        if (response.status == 200) {
          t.setState({
            listaUsuariosData: response.data,
            clientesCargados: true,
          });
        } else {
          t.setState({
            alertaError: true,
            severity: "error",
            msgErr: response.data,
          });
        }
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          t.setState({
            alertaError: true,
            severity: "error",
            msgErr: error.response.data,
          });
        } else {
          // Something happened in setting up the request that triggered an Error
          t.setState({
            alertaError: true,
            severity: "error",
            msgErr: error.message,
          });
        }
      });
  };

  componentDidMount() {
    // console.log(this.props);
    this.cargarRestaurantes();
    this.cargarCliente();
  }

  handleTab = (event, value) => {
    this.setState({ value: value });
  };

  render() {
    return (
      <>
        <NavBar />
        <Container maxWidth="md">
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6, p: 2 }}>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <h1>Usuarios registrados en el sistema</h1>
              </Grid>
              <Box sx={{ width: "100%" }}>
                <Tabs
                  value={this.state.value}
                  onChange={this.handleTab}
                  centered
                  textColor="inherit"
                  indicatorColor="inherit"
                >
                  <Tab
                    label="Clientes"
                    icon={<PersonIcon />}
                    {...a11yProps(0)}
                  />
                  <Tab
                    label="Restaurantes"
                    icon={<HomeIcon />}
                    {...a11yProps(1)}
                  />
                </Tabs>
                <TabPanel value={this.state.value} index={0}>
                  {this.state.clientesCargados ? (
                    <ListaUsuarios
                      listaUsuarios={this.state.listaUsuariosData}
                      cargarCliente={this.cargarCliente}
                    />
                  ) : (
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <CircularProgress style={{ color: "red" }} />
                    </div>
                  )}
                </TabPanel>
                <TabPanel value={this.state.value} index={1}>
                  {this.state.restaurantesCargados ? (
                    <Grid
                      item
                      xs={12}
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <ListaRestaurantes
                        listaUsuarios={this.state.listaRestaurantesData}
                        cargarRestaurantes={this.cargarRestaurantes}
                      />
                    </Grid>
                  ) : (
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <CircularProgress style={{ color: "red" }} />
                    </div>
                  )}
                </TabPanel>
              </Box>
            </Grid>
          </Box>

          <Snackbar
            open={this.state.alertaError}
            autoHideDuration={6000}
            onClose={this.handleClose}
          >
            <Alert
              onClose={this.handleClose}
              severity={this.severity}
              sx={{ width: "100%" }}
            >
              {this.state.msgErr}
            </Alert>
          </Snackbar>
        </Container>
      </>
    );
  }
}

export default ListarUsuariosRegistrados;
