import React, { useState, useEffect } from "react";
import { Container, Grid, Box, Paper, styled, IconButton } from "@mui/material";
import NavBar from "../components/NavBar/NavBar";
import colorCodes from "../assets/colors/Colors";
import TableReclamos from "../components/basicos/TableReclamos";
import AxiosReclamo from "../components/axios/AxiosReclamo";
import { useHistory } from "react-router-dom";
import { getSession } from "../components/auth/auth";
import SyncIcon from "@mui/icons-material/Sync";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const ListarReclamos = (props) => {
  let history = useHistory();
  const [snackbar, setSnackbar] = useState(false);
  const [loader, setLoader] = useState(true);
  const [severity, setSeverity] = useState();

  const [reclamos, setReclamos] = useState([]);
  const [reclamosSD, setReclamosSD] = useState([]);
  const [reclamosConDevolucion, setReclamosConDevolucion] = useState([]);
  const [reclamosSinDevolucion, setReclamosSinDevolucion] = useState([]);
  const [mje, setMje] = useState("");

  const [nuevosDatos, setNuevosDatos] = useState(false);
  const handleNuevosDatos = () => {
    setNuevosDatos(!nuevosDatos);
  };

  const [diferencia, setDiferencia] = useState();

  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }
  let email = "";
  if (getSession() != null && getSession().email != null) {
    email = getSession().email;
  }

  const [count, setCount] = useState();

  useEffect(() => {
    AxiosReclamo.get(`/listar/${email}`)
      .then(function (response) {
        if (response.status == 200) {
          //console.log(response.data);
          let reclamoCD = [];
          let reclamoSD = [];
          response.data.map((val) => {
            if (val.estadoReclamo === "ABIERTO") {
              reclamoCD.push(val);
            } else {
              setNuevosDatos(true);
              reclamoSD.push(val);
            }
          });
          //setReclamosConDevolucion(response.data); // sin esto no anda
          setReclamosSinDevolucion(reclamoSD);
          setReclamosConDevolucion(reclamoCD);
        }
      })
      .catch(function (error) {
        console.log(error);
        setSeverity("error");
        setSnackbar(true);
        setMje(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
  }, [nuevosDatos]);

  return (
    <>
      <NavBar />
      <Container maxWidth="xl">
        <Box bgcolor={colorCodes.backBox} sx={{ marginY: 6 }}>
          <Grid container spacing={2} padding={3}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Grid container justifyContent="left">
                <h1>Reclamos de Clientes</h1>
              </Grid>
              <Grid container justifyContent="right">
                <IconButton onClick={handleNuevosDatos} sx={{ color: "white" }}>
                  <SyncIcon />
                </IconButton>
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <Item>
                <Paper sx={{ width: "100%", overflow: "hidden" }}>
                  <TableReclamos
                    data={reclamosConDevolucion}
                    estado={"POR DEVOLUCION"}
                    token={token}
                    setMje={setMje}
                    setSeverity={setSeverity}
                    setSnackbar={setSnackbar}
                    handleNuevosDatos={handleNuevosDatos}
                    setNuevosDatos={setNuevosDatos}
                  />
                </Paper>
              </Item>
            </Grid>
            <Grid item xs={6}>
              <Item>
                <Paper sx={{ width: "100%", overflow: "hidden" }}>
                  <TableReclamos
                    data={reclamosSinDevolucion}
                    estado={"OTROS"}
                    token={token}
                    setMje={setMje}
                    setSeverity={setSeverity}
                    setSnackbar={setSnackbar}
                    handleNuevosDatos={handleNuevosDatos}
                    count={count}
                    setNuevosDatos={setNuevosDatos}
                  />
                </Paper>
              </Item>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};
export default ListarReclamos;
