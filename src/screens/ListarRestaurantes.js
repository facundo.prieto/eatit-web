import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
  Grid,
  Container,
  Box,
  CircularProgress,
  Snackbar,
  Alert,
  Typography,
  Slider,
  Backdrop,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Button,
} from "@mui/material";
import Input from "./../components/basicos/Input";
import { getSession } from "../components/auth/auth";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import CardRestaurante from "./../components/basicos/CardRestaurante";
import colorCodes from "./../assets/colors/Colors";
import SearchIcon from "@mui/icons-material/Search";
import StarIcon from "@mui/icons-material/Star";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import DeliveryDiningIcon from "@mui/icons-material/DeliveryDining";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";
import categoriasAlimentos from "../assets/data/CategoriasAlimentos.json";
import BackspaceIcon from "@mui/icons-material/Backspace";

const ListarRestaurantes = () => {
  let history = useHistory();
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();
  const [loader, setLoader] = useState(true);
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const [dataRestaurantes, setDataRestaurantes] = useState([]);
  const [dataRestaurantesCerrados, setDataRestaurantesCerrados] = useState([]);
  const [nuevosDatos, setNuevosDatos] = useState([false]);
  const [selectedCategoria, setSelectedCategoria] = React.useState("");

  useEffect(() => {
    localStorage.removeItem("__carrito");
    localStorage.removeItem("__montoPaypal");
    localStorage.removeItem("__restaurante");
    localStorage.removeItem("__email_restoran");
    localStorage.removeItem("__detalle_pedido");
    AxiosRestaurante.get(
      `/listar/full`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        console.log(response.data);

        let data = response.data.filter((val) => {
          return val.estado === "ACTIVO";
        });

        let abiertos = data.filter((val) => {
          return val.abierto ? val : "";
        });
        setDataRestaurantes(abiertos);
        let cerrados = data.filter((val) => {
          return !val.abierto ? val : "";
        });
        setDataRestaurantesCerrados(cerrados);
        setLoader(false);
      })
      .catch(function (error) {
        //console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
  }, [nuevosDatos]);

  const handleCloseSnackbar = () => setSnackbar(false);
  const handleSliderChange = (event, value) => {
    setCostoEnvio(value);
  };
  const handleChangeSelect = (event) => {
    //setCostoEnvio(event.target.value);
    //console.log(event.target.value);
    setSelectedCategoria(event.target.value);
  };

  //buscar
  const [buscar, setBuscar] = useState("");
  const handleBuscar = (event) => setBuscar(event.target.value);

  // costo envio
  const [costoEnvio, setCostoEnvio] = useState(200);
  const handleCostoEnvio = (event) => setCostoEnvio(event.target.value);

  const borrarFiltros = () => {
    setBuscar("");
    setSelectedCostoEnvio(false);
    setSelectedCalificacion(false);
    setSelectedCategoria("");
    setCostoEnvio(200);
  };

  // calificacion
  const [selectedCalificacion, setSelectedCalificacion] = useState(false);
  const changeSelectedCalificacion = () => {
    setSelectedCalificacion(!selectedCalificacion);
    setSelectedCostoEnvio(selectedCalificacion);
  };

  const [selectedCostoEnvio, setSelectedCostoEnvio] = useState(false);
  const changeSelectedCostoEnvio = () => {
    setSelectedCostoEnvio(!selectedCostoEnvio);
    setSelectedCalificacion(selectedCostoEnvio);
  };

  return (
    <>
      <NavBar />
      <Grid display="flex" justifyContent="center">
        <Container maxWidth="xs" sx={{ mt: 4, mr: 0 }}>
          <Grid
            xs={12}
            justifyContent="center"
            alignItems="center"
            margin="2"
            bgcolor={colorCodes.backBox}
            sx={{ p: 2 }}
          >
            <Grid item xs={12} display="flex">
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="buscar"
                  label="Buscar restaurantes..."
                  onChange={handleBuscar}
                  value={buscar}
                />
              </Grid>
              <Grid
                item
                xs={2}
                display="flex"
                justifyContent="left"
                alignItems="center"
              >
                <SearchIcon />
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mt: 6 }}
            >
              <Typography component="legend">Ordenar por:</Typography>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 1, mr: 1 }}
            >
              <Grid
                item
                xs={6}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ mt: 4 }}
              >
                <Button
                  variant="text"
                  sx={{ color: "white" }}
                  onClick={changeSelectedCalificacion}
                  className={selectedCalificacion ? "btn-selected" : ""}
                >
                  <StarIcon sx={{ mr: 1 }} />
                  Mejor calificados
                </Button>
              </Grid>
              <Grid
                item
                xs={6}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ mt: 4 }}
              >
                <Button
                  variant="text"
                  sx={{ color: "white" }}
                  onClick={changeSelectedCostoEnvio}
                  className={selectedCostoEnvio ? "btn-selected" : ""}
                >
                  <DeliveryDiningIcon sx={{ mr: 1 }} /> Menor costo de envio
                </Button>
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mt: 6 }}
            >
              <Typography component="legend">Filtrar por:</Typography>
            </Grid>
            <Grid
              item
              xs={12}
              display="column"
              justifyContent="center"
              alignItems="center"
              sx={{ mb: 6, ml: 1, mr: 1 }}
            >
              <Typography id="input-slider" gutterBottom>
                Costo de envío
              </Typography>
              <Grid container spacing={2} alignItems="center">
                <Grid item xs={2} align="right">
                  <MonetizationOnIcon />
                </Grid>
                <Grid item xs={8}>
                  <Slider
                    value={typeof costoEnvio === "number" ? costoEnvio : 0}
                    onChange={handleSliderChange}
                    aria-labelledby="input-slider"
                    color="error"
                    max={200}
                    min={0}
                    step={5}
                  />
                </Grid>
                <Grid item xs={2} sx={{ pb: 1 }}>
                  $ {costoEnvio}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              display="column"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 1, mr: 1 }}
            >
              <Grid container spacing={2} alignItems="center">
                <FormControl fullWidth sx={{ mb: 6, ml: 3, mr: 3 }} error>
                  <InputLabel id="demo-simple-select-label">
                    Categoria
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectedCategoria}
                    label="Categoria"
                    onChange={handleChangeSelect}
                    color="error"
                  >
                    {categoriasAlimentos.map((option, key) => {
                      return (
                        <MenuItem key={key} value={option.descripcion}>
                          {option.descripcion}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mb: 6 }}
            >
              <Button variant="contained" color="error" onClick={borrarFiltros}>
                Borrar filtros
                <BackspaceIcon sx={{ ml: 1 }} />
              </Button>
            </Grid>
          </Grid>
        </Container>

        <Container maxWidth="md" sx={{ mt: 4, ml: 0 }}>
          <Box bgcolor={colorCodes.backBox} sx={{ p: 2 }}>
            <Box sx={{ mr: 2, ml: 2 }}>
              <h2>Restaurantes</h2>
            </Box>
            <Box sx={{ pt: 1, pb: 1 }}>
              {!loader ? (
                <CardRestaurante
                  data={dataRestaurantes}
                  buscar={buscar}
                  costoEnvio={costoEnvio}
                  mejorCalificados={selectedCalificacion}
                  filtroCostoEnvio={selectedCostoEnvio}
                  categoria={selectedCategoria}
                />
              ) : (
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <CircularProgress style={{ color: "red" }} />
                </div>
              )}
            </Box>
            <Box sx={{ mr: 2, ml: 2 }}>
              <h2>Restaurantes Cerrados</h2>
            </Box>
            <Box sx={{ pt: 1, pb: 1 }}>
              {!loader ? (
                <CardRestaurante
                  data={dataRestaurantesCerrados}
                  buscar={buscar}
                  costoEnvio={costoEnvio}
                  mejorCalificados={selectedCalificacion}
                  filtroCostoEnvio={selectedCostoEnvio}
                  categoria={selectedCategoria}
                  cerrado={true}
                />
              ) : (
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <CircularProgress style={{ color: "red" }} />
                </div>
              )}
            </Box>
          </Box>
        </Container>
      </Grid>

      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};
export default ListarRestaurantes;
