import React, { useState, useEffect } from "react";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import Typography from "@mui/material/Typography";
import { Container, Grid, Box, Button } from "@mui/material";
import Input from "../components/basicos/Input";
import axiosRestaurante from "../components/axios/AxiosRestaurante";

const ModificarPromocion = (props) => {
  const {
    datos,
    handleOpenModal,
    handleNuevosDatosPromocion,
    setMje,
    handleSnackBar,
    setSeverity,
  } = props;

  const [descripcion, setDescripcion] = useState("");
  const [descuento, setDescuento] = useState("");

  useEffect(() => {
    setDescripcion(datos.promocion.descripcion);
    setDescuento(datos.promocion.descuento);
  }, [datos]);

  let history = useHistory();

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const handleClick = () => {
    if (descripcion && descuento) {
      if (descuento > 0 && descuento <= 100) {
        let data = {
          id: datos.promocion.id,
          descripcion: descripcion,
          descuento: descuento,
        };
        axiosRestaurante
          .put(`/promocion/actualizar/`, data, {
            headers: {
              Authorization: `${token}`,
            },
          })
          .then(function (response) {
            if (response.status === 200) {
              setMje("La promoción se modificó correctamente");
              handleOpenModal();
              handleNuevosDatosPromocion();

              setSeverity("success");
            } else {
              setMje(response.data);
              setSeverity("error");
              setMje("Ocurrio un error en el sistema");
            }
          })

          .catch(function (error) {
            //setSeverity("error");
            //setMje("Error en el servidor");
          });
      } else {
        setSeverity("error");
        setMje("El valor del descuento debe ser entre 1 y 100");
      }
      handleSnackBar();
    } else {
      setSeverity("error");
      setMje("Los campos que tienen * son obligatorios");
      handleSnackBar();
    }
  };

  const handleChangeDescripcion = (event) => setDescripcion(event.target.value);
  const handleChangeDescuento = (event) => setDescuento(event.target.value);

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: colorCodes.black,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <>
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Modificar Promoci&oacute;n
          </Typography>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            ></Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="descripcion"
                label="Descripci&oacute;n *"
                value={descripcion}
                onChange={handleChangeDescripcion}
                variant="standard"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                sx={{ mr: 6, ml: 6, mb: 2 }}
                fullWidth
                id="descuento"
                label="Descuento (%) *"
                value={descuento}
                onChange={handleChangeDescuento}
                variant="standard"
                type="number"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            ></Grid>

            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 6, mr: 6, mb: 2 }}
            ></Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            ></Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={handleClick}
              >
                Modificar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default ModificarPromocion;
