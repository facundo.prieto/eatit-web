import * as React from "react";
import { useState } from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import Typography from "@mui/material/Typography";
import defaultImage from "../../assets/images/defaultImage.png";
import ModalBasic from "./ModalBasic";
import Menu from "../../screens/Menu";

const CardPromo = (props) => {
  const { data, agregarAlCarrito, filtroPrecio = 1000, categoria = "" } = props;

  const [menu, setMenu] = useState("");
  const [openModal, setOpenModal] = useState(false);

  const handleModal = (value) => {
    value.desc =
      value.precio - (value.precio * value.promocion.descuento) / 100;
    setMenu(value);
    console.log(value);
    setOpenModal(true);
  };
  const handleCloseModal = () => setOpenModal(false);
  return (
    <>
      {data
        .filter((val) => {
          let coincideCat = false;

          if (filtroPrecio >= 1000) {
            if (categoria === "") {
              return true;
            } else {
              var i = 0;
              while (i < val.categorias.length && !coincideCat) {
                if (val.categorias[i].descripcion === categoria) {
                  coincideCat = true;
                }
                i++;
              }
              return coincideCat;
            }
          } else {
            if (categoria === "") {
              return val.precio <= filtroPrecio;
            } else {
              var i = 0;
              while (i < val.categorias.length && !coincideCat) {
                if (
                  val.categorias[i].descripcion === categoria &&
                  val.precio <= filtroPrecio
                ) {
                  coincideCat = true;
                }
                i++;
              }
              return coincideCat;
            }
          }
        })
        .map((value) => (
          <Card
            sx={{
              overflow: "visible",
              position: "relative",
              minWidth: "30%",
              m: 2,
            }}
          >
            <CardActionArea
              sx={{ display: "flex", flexDirection: "column" }}
              onClick={() => handleModal(value)}
            >
              <CardMedia
                component="img"
                height="140"
                image={value.imagen === "" ? defaultImage : value.imagen}
                alt="Imagen no disponible"
              />
              <Box
                sx={{ display: "flex", flexDirection: "row", width: "100%" }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: "50%",
                  }}
                >
                  <CardContent sx={{ flex: "1 0 auto", mt: 2 }}>
                    <Typography component="div" variant="h6">
                      {value.titulo}
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      color="text.secondary"
                      component="div"
                    >
                      {value.promocion.descripcion}
                    </Typography>
                  </CardContent>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: "50%",
                  }}
                >
                  <CardContent sx={{ flex: "1 0 auto", mt: 2 }}>
                    <Typography
                      component="div"
                      variant="subtitle2"
                      color="text.secondary"
                      textAlign="right"
                      style={{ textDecoration: "line-through" }}
                    >
                      $ {value.precio}
                    </Typography>
                    <Typography
                      component="div"
                      variant="h6"
                      color="red"
                      textAlign="right"
                    >
                      ${" "}
                      {value.precio -
                        (value.precio * value.promocion.descuento) / 100}
                    </Typography>
                  </CardContent>
                </Box>
              </Box>
            </CardActionArea>
          </Card>
        ))}

      <ModalBasic
        open={openModal}
        onClose={handleCloseModal}
        content={
          <Menu
            menu={menu}
            setOpenModal={setOpenModal}
            agregarAlCarrito={agregarAlCarrito}
          />
        }
      />
    </>
  );
};

export default CardPromo;
