import React, { useState, useEffect } from "react";
import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  Card,
  IconButton,
  Tooltip,
  CardContent,
  Backdrop,
  Snackbar,
  Alert,
  CardActions,
  Typography,
  MenuItem,
  Button,
  Chip,
  CircularProgress,
} from "@mui/material";

import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import BackspaceIcon from "@mui/icons-material/Backspace";
import moment from "moment";
import { useHistory } from "react-router-dom";
import AxiosRestaurante from "./axios/AxiosRestaurante";
import AxiosPedido from "./axios/AxiosPedido";
import { getSession } from "./auth/auth";
import rutas from "../assets/const/rutas";
import MonetizationOnOutlinedIcon from "@mui/icons-material/MonetizationOnOutlined";
import colorCodes from "../assets/colors/Colors";
import ModalBasic from "./basicos/ModalBasic";
import EstadoReclamo from "./EstadoReclamo";
import GestionarDevolucion from "./GestionarDevolucion";
import ModalConfirmacion from "./basicos/ModalConfirmacion";

const ListaReclamosRestaurante = ({
  data,
  nuevosDatos,
  handleNuevosDatos,
  setSearchTerm,
}) => {
  let history = useHistory();

  //
  const [openModalDevolucion, setOpenModalDevolucion] = useState(false);
  const handleOpenModalDevolucion = () =>
    setOpenModalDevolucion(!openModalDevolucion);
  const handleCloseModalDevolucion = () => setOpenModalDevolucion(false);

  const handleModalDevolucion = (value) => {
    setMenu(value);
    setOpenModalDevolucion(true);
  };
  //

  const [snackbar, setSnackbar] = useState(false);
  const [loader, setLoader] = useState(true);
  const [severity, setSeverity] = useState();
  const [mje, setMje] = useState("");

  const [desde, setDesde] = useState(null);
  const [hasta, setHasta] = useState(null);
  const [selectedCliente, setSelectedRestaurante] = useState("");

  const [selectedEstadoReclamo, setSelectedEstadoReclamo] = useState("TODOS");
  const [clientesUsados, setClientesUsados] = useState([]);

  //
  const [menu, setMenu] = useState("");

  const [openModalReclamo, setOpenModalReclamo] = useState("");
  const handleOpenModalReclamo = () => setOpenModalReclamo(!openModalReclamo);
  const handleCloseModalReclamo = () => setOpenModalReclamo(false);

  const handleModalReclamo = (value) => {
    setMenu(value);
    setOpenModalReclamo(true);
  };

  const handleClose = () => {
    setSnackbar(false);
  };

  const getTituloReclamo = (str) => {
    var regExp = /\[([^)]+)\]/;
    var match = regExp.exec(str);
    return match[1];
  };

  const getDescripcionReclamo = (str) => {
    var regExp = /\[([^)]+)\] (.*)/;
    var match = regExp.exec(str);
    return match[2];
  };

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );

  const handleChangeSelectEstadoReclamo = (event) => {
    setSelectedEstadoReclamo(event.target.value);
  };

  useEffect(() => {
    // uso esto para quedarme con los restaurantes que uso el cliente sin repetidos
    let clientes = [];
    data.map((value, key) => {
      clientes.push(value.cliente);
    });

    let uniqueCliente = [...new Set(clientes)];

    setClientesUsados(uniqueCliente);
    setLoader(false);
    //////////////////////////////////////////////////////////////////////////////
  }, [data]);

  const handleChangeSelectCliente = (event) => {
    console.log(event.target.value);
    setSelectedRestaurante(event.target.value);
  };

  const borrarFiltros = () => {
    setDesde(null);
    setHasta(null);
    setSelectedRestaurante("");
    setSelectedEstadoReclamo("TODOS");
    setSearchTerm("");
  };

  function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : "0" + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : "0" + day;

    return day + "/" + month + "/" + year;
  }

  return (
    <>
      <Grid
        item
        xs={12}
        display="flex"
        justifyContent="center"
        alignItems="center"
        sx={{ ml: 14, mr: 1 }}
      >
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Grid
            item
            xs={6}
            display="column"
            justifyContent="center"
            alignItems="center"
          >
            <h4>Desde</h4>
            <DatePicker
              inputFormat="dd/MM/yyyy"
              value={desde}
              onChange={(newValue) => {
                setDesde(newValue);
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
        </LocalizationProvider>

        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Grid
            item
            xs={6}
            display="column"
            justifyContent="center"
            alignItems="center"
          >
            <h4>Hasta</h4>
            <DatePicker
              inputFormat="dd/MM/yyyy"
              value={hasta}
              onChange={(newValue) => {
                setHasta(newValue);
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
        </LocalizationProvider>
      </Grid>
      <Grid
        item
        xs={12}
        display="flex"
        justifyContent="center"
        alignItems="center"
        sx={{ mt: 2 }}
      >
        <Grid
          item
          xs={5}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ ml: 3, mr: 1 }}
        >
          <FormControl fullWidth error>
            <InputLabel id="demo-simple-select-label">Cliente</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectedCliente}
              label="Cliente"
              onChange={handleChangeSelectCliente}
              color="error"
            >
              {clientesUsados.map((option, key) => {
                return (
                  <MenuItem key={key} value={option}>
                    {option}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid
          item
          xs={5}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ ml: 3, mr: 1 }}
        >
          <FormControl fullWidth error>
            <InputLabel id="demo-simple-select-label">
              Estado del Reclamo
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectedEstadoReclamo}
              label="Estado del Reclamo"
              onChange={handleChangeSelectEstadoReclamo}
              color="error"
            >
              <MenuItem key={1} value="ABIERTO">
                Abierto
              </MenuItem>
              <MenuItem key={2} value="RESUELTO">
                Resuelto
              </MenuItem>
              <MenuItem key={3} value="NO_RESUELTO">
                No Resuelto
              </MenuItem>
              <MenuItem key={4} value="TODOS">
                Todos
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid
          item
          xs={1}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ ml: 3, mr: 2 }}
        >
          <Button variant="contained" color="error" onClick={borrarFiltros}>
            <BackspaceIcon sx={{ ml: 1 }} />
          </Button>
        </Grid>
      </Grid>

      {data
        .filter((val) => {
          let check = "";
          let from = "";
          let to = "";

          if (
            val.estadoReclamo === "ABIERTO" ||
            val.estadoReclamo === "RESUELTO" ||
            val.estadoReclamo === "NO_RESUELTO"
          ) {
            if (
              selectedCliente === "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde === null &&
              hasta === null
            ) {
              console.log(val.fecha);
              console.log(val);
              return val;
            } else if (
              selectedCliente === "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde !== null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              console.log(val.fecha);
              console.log(
                moment(val.fecha, "DD/MM/YYYY").isBetween(
                  moment(getFormattedDate(desde), "DD/MM/YYYY"),
                  moment(getFormattedDate(hasta), "DD/MM/YYYY"),
                  undefined,
                  []
                )
              );

              return val ? check.isBetween(from, to, undefined, []) : "";
            } else if (
              selectedCliente === "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde !== null &&
              hasta === null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");

              console.log(val ? check.isSameOrAfter(from, undefined) : "");

              return val ? check.isSameOrAfter(from, undefined) : "";
            } else if (
              selectedCliente === "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde === null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              console.log(val ? check.isSameOrBefore(to, undefined) : "");

              return val ? check.isSameOrBefore(to, undefined) : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde === null &&
              hasta === null
            ) {
              return val ? val.cliente === selectedCliente : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde !== null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.cliente === selectedCliente &&
                    check.isBetween(from, to, undefined, [])
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde !== null &&
              hasta === null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");

              return val
                ? val.cliente === selectedCliente &&
                    check.isSameOrAfter(from, undefined)
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo === "TODOS" &&
              desde === null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.cliente === selectedCliente &&
                    check.isSameOrBefore(to, undefined)
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo !== "TODOS" &&
              desde === null &&
              hasta === null
            ) {
              return val
                ? val.cliente === selectedCliente &&
                    val.estadoReclamo === selectedEstadoReclamo
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo !== "TODOS" &&
              desde !== null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.cliente === selectedCliente &&
                    val.estadoReclamo === selectedEstadoReclamo &&
                    check.isBetween(from, to, undefined, [])
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo !== "TODOS" &&
              desde !== null &&
              hasta === null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              from = moment(getFormattedDate(desde), "DD/MM/YYYY");

              return val
                ? val.cliente === selectedCliente &&
                    val.estadoReclamo === selectedEstadoReclamo &&
                    check.isSameOrAfter(from, undefined)
                : "";
            } else if (
              selectedCliente !== "" &&
              selectedEstadoReclamo !== "TODOS" &&
              desde === null &&
              hasta !== null
            ) {
              check = moment(val.fecha, "DD/MM/YYYY");
              to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

              return val
                ? val.cliente === selectedCliente &&
                    val.estadoReclamo === selectedEstadoReclamo &&
                    check.isSameOrBefore(to, undefined)
                : "";
            } else if (
              selectedCliente === "" &&
              selectedEstadoReclamo !== "TODOS"
            ) {
              return val ? val.estadoReclamo === selectedEstadoReclamo : "";
            }
          } else return null;
        })
        .map((value, key) => (
          <Card sx={{ display: "flex", mt: 2, mr: 3, ml: 3 }}>
            <CardContent sx={{ flex: "1 0 auto" }}>
              <Typography component="div" variant="h5">
                {value.nombreCliente}{" "}
              </Typography>
              <Typography component="div" variant="subtitle1" color="error">
                Reclamo #{value.numeroReclamo}
                {" - "}Pedido #{value.numeroPedido}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Motivo: {getTituloReclamo(value.descripcion)}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                {value.fecha} -{" "}
                {value.estadoReclamo === "ABIERTO" ? "Abierto" : "Resuelto"}
              </Typography>
              <Typography component="div" variant="subtitle1">
                Descripcion:{" "}
                {getDescripcionReclamo(value.descripcion).length === 0
                  ? "S/D"
                  : value.descripcion.slice(
                      value.descripcion.indexOf("] ") + 1
                    )}
              </Typography>
            </CardContent>

            <CardActions disableSpacing align="center">
              <Grid
                item
                xs={12}
                display="column "
                justifyContent="center"
                alignItems="center"
              >
                <Typography component="div" variant="h5" align="center">
                  {value.estadoReclamo === "ABIERTO" && (
                    <Tooltip title="Generar devolución">
                      <IconButton
                        variant="contained"
                        color="primary"
                        fullWidth
                        onClick={() => handleModalDevolucion(value)}
                      >
                        <MonetizationOnOutlinedIcon
                          onClick={value.onClick}
                          fontSize="large"
                          justify="center"
                          alignItems="center"
                        />
                      </IconButton>
                    </Tooltip>
                  )}
                  {value.estadoReclamo === "ABIERTO" ? (
                    <Chip
                      label="Abierto"
                      color="success"
                      onClick={() => handleModalReclamo(value)}
                      value={value.numeroReclamo}
                    />
                  ) : value.estadoReclamo === "RESUELTO" ? (
                    <Chip
                      label="Resuelto"
                      color="primary"
                      type="button"
                      disabled
                    />
                  ) : (
                    <Chip
                      label="No resuelto"
                      color="error"
                      type="button"
                      disabled
                    />
                  )}
                  <ModalBasic
                    open={openModalReclamo}
                    data={menu}
                    onClose={handleCloseModalReclamo}
                    content={
                      <EstadoReclamo
                        data={menu}
                        handleOpenModalReclamo={handleOpenModalReclamo}
                        handleNuevosDatos={handleNuevosDatos}
                        numeroReclamo={value.numeroReclamo}
                        setLoader={setLoader}
                        setMje={setMje}
                        setSnackbar={setSnackbar}
                        setSeverity={setSeverity}
                      />
                    }
                  />

                  <ModalConfirmacion
                    open={openModalDevolucion}
                    onClose={handleCloseModalDevolucion}
                    content={
                      <GestionarDevolucion
                        datos={menu}
                        setOpenModalDevolucion={setOpenModalDevolucion}
                        handleOpenModalDevolucion={handleOpenModalDevolucion}
                        handleNuevosDatos={handleNuevosDatos}
                        numeroPedido={value.numeroPedido}
                        setLoader={setLoader}
                        setMje={setMje}
                        setSnackbar={setSnackbar}
                        setSeverity={setSeverity}
                      />
                    }
                  />
                </Typography>
              </Grid>
            </CardActions>
          </Card>
        ))}
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mje}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListaReclamosRestaurante;
