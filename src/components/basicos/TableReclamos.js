import React, { useState, useEffect } from "react";
import {
  Button,
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Card,
  CardContent,
  Typography,
} from "@mui/material";
import { useHistory } from "react-router-dom";
import AxiosRestaurante from "./../axios/AxiosRestaurante";
import AxiosPedido from "./../axios/AxiosPedido";

const TableReclamos = (props) => {
  const {
    data,
    estado,
    token,
    setMje,
    setSeverity,
    setSnackbar,
    handleNuevosDatos,
  } = props;
  let history = useHistory();
  const [nombreCliente, setNombreCliente] = useState([]);
  const getTituloReclamo = (str) => {
    var regExp = /\[([^)]+)\]/;
    var match = regExp.exec(str);
    return match[1];
  };

  const gestionarDevolucion = (data) => {
    AxiosPedido.get(`/listar/${data.numeroPedido}`).then(function (response) {
      let body = {
        id: response.data.idPedido,
        monto: response.data.monto,
        fecha: null,
      };
      AxiosRestaurante.post(
        `/devolucion/gestionar/${data.numeroReclamo}`,
        body,
        { headers: { Authorization: `${token}` } }
      )
        .then(function (response) {
          console.log(response);
          if (response.status == 200) {
            handleNuevosDatos();
            setMje(`Se realizó la devolución correctamente`);
            setSeverity("success");
            setSnackbar(true);
          } else {
            setMje(
              `Ocurrio un error al querer realizar la devolución para el reclamo ${data.numeroReclamo}`
            );
            setSeverity("error");
            setSnackbar(true);
          }
        })
        .catch(function (error) {
          if (error.response) {
            // Request made and server responded
            setMje(error);
            setSeverity("error");
            setSnackbar(true);
          } else {
            // Something happened in setting up the request that triggered an Error
            setMje(`Ocurrió un error en el sistema, intente nuevamente...`);
            setSeverity("error");
            setSnackbar(true);
          }
        });
    });
  };

  return (
    <>
      <TableContainer sx={{ maxHeight: 440, minHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center">
                {estado} ( {data.length} )
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.length === 0 && <p>No hay pedidos para procesar</p>}
            {data.map((dataItem, key) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={key}>
                  <TableCell>
                    <Card sx={{ display: "flex" }}>
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          width: "100%",
                        }}
                      >
                        <CardContent>
                          <Typography
                            component="div"
                            variant="subtitle1"
                            color="error"
                          >
                            Reclamo #{dataItem.numeroReclamo} {estado}
                          </Typography>
                          <Typography
                            variant="subtitle2"
                            color="text.secondary"
                            component="div"
                          >
                            {dataItem.fecha}
                          </Typography>
                          <Typography
                            variant="subtitle2"
                            color="text.secondary"
                            component="div"
                          >
                            Pedido #{dataItem.numeroPedido}
                          </Typography>
                          <Typography
                            component="div"
                            variant="h6"
                            sx={{ mt: 1 }}
                          >
                            {getTituloReclamo(dataItem.descripcion)}
                          </Typography>
                          <Typography component="div" variant="subtitle1">
                            {dataItem.descripcion.slice(
                              dataItem.descripcion.indexOf("] ") + 1
                            )}
                          </Typography>

                          {dataItem.estadoReclamo === "ABIERTO" && (
                            <Button
                              variant="contained"
                              color="error"
                              sx={{ mt: 3 }}
                              size="small"
                              fullWidth
                              onClick={() => gestionarDevolucion(dataItem)}
                            >
                              Gestionar Devolucion
                            </Button>
                          )}
                        </CardContent>
                      </Box>
                    </Card>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default TableReclamos;
