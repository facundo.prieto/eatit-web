import React, { useState } from "react";
import { Box, Snackbar, Alert } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import defaultImage from "../../assets/images/defaultImage.png";
import StarIcon from "@mui/icons-material/Star";
import { useHistory } from "react-router-dom";
import rutas from "./../../assets/const/rutas";
const CardRestaurante = (props) => {
  let history = useHistory();
  const {
    data,
    buscar = "",
    costoEnvio = 200,
    mejorCalificados = false,
    filtroCostoEnvio = false,
    categoria = "",
  } = props;
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();

  const handleIrRestaurante = (email) => {
    localStorage.setItem("__email_restoran", email);
    history.push(rutas.restaurante);
  };

  const handleCloseSnackbar = () => setSnackbar(false);
  const handleRestauranteCerrado = () => {
    setSeverity("error");
    setSnackbar(true);
    setMjeError(`El restaurante está actualmente cerrado`);
  };
  return (
    <>
      {data
        .filter((val) => {
          //console.log(val);
          if (!mejorCalificados && !filtroCostoEnvio) {
            //console.log("no hay filtros");
          } else if (mejorCalificados) {
            //console.log("se filtra por calificacion");
            //ordeno por costo envio

            data.sort((a1, b1) => {
              const diff = a1.calificacion - b1.calificacion;
              // if (diff === 0) return 0;

              const sign = Math.abs(diff) / diff;

              return mejorCalificados ? -sign : sign;
            });
          } else {
            //console.log("se filtra por costo envio");
            //ordeno por costo envio
            data.sort((a, b) => {
              const diff = a.costoEnvio - b.costoEnvio;
              if (diff === 0) return 0;

              const sign = Math.abs(diff) / diff;

              return mejorCalificados ? -sign : sign;
            });
          }

          if (buscar === "" && costoEnvio >= 200) {
            // sin filtros
            // chequeo por categoria si es necesario
            if (categoria === "") {
              return val;
            } else {
              console.log(val.categorias);
              for (var i = 0; i < val.categorias.length; i++) {
                return val ? val.categorias[i].descripcion === categoria : "";
              }
            }
            // termina chequeo por categoria si es necesario
          } else if (
            val.nombre.toLowerCase().includes(buscar.toLowerCase()) &&
            costoEnvio >= val.costoEnvio
          ) {
            // chequeo por categoria si es necesario
            if (categoria === "") {
              return val;
            } else {
              console.log(val.categorias);
              for (var i = 0; i < val.categorias.length; i++) {
                return val ? val.categorias[i].descripcion === categoria : "";
              }
            }
            // termina chequeo por categoria si es necesario
          }
        })
        .map((value, key) => (
          <Card
            sx={{ display: "flex", m: 2 }}
            key={key}
            onClick={
              !value.abierto
                ? handleRestauranteCerrado
                : () => handleIrRestaurante(value.email)
            }
            id="card-restaurante"
          >
            <CardMedia
              component="img"
              sx={{
                width: 100,
                m: 2,
                height: 100,
                border: "1px solid black",
                borderRadius: 2,
              }}
              image={value.imagen === "" ? defaultImage : value.imagen}
              alt="Imagen no disponible"
            />
            <Box
              sx={{ display: "flex", flexDirection: "column", width: "100%" }}
            >
              <CardContent sx={{ flex: "1 0 auto", mt: 2 }}>
                <Typography component="div" variant="subtitle1">
                  <b>{value.nombre}</b>
                </Typography>
                <Typography
                  variant="subtitle2"
                  color="text.secondary"
                  component="div"
                >
                  Env&iacute;o $ {value.costoEnvio}
                </Typography>
              </CardContent>
            </Box>
            <Box
              sx={{
                textAlign: "right",
                my: "auto",
                mr: 6,
                fontSize: 10,
              }}
              width="100%"
            >
              <h1 style={{ color: "green" }}>
                <StarIcon size="large" sx={{ fontSize: 15 }} />
                {value.calificacion ? value.calificacion : "Nuevo"}
              </h1>
            </Box>
          </Card>
        ))}
      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default CardRestaurante;
