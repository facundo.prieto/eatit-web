export function setSession(session){
    const now = new Date();
    const newSession = {
        ...session,
        expire: now.getTime() + 3600000 //1 hora de sesion
    }
    localStorage.setItem("__session", JSON.stringify(newSession));
}

export function getSession(){
    const now = new Date();

    const sessionStr = localStorage.getItem("__session");
    if (!sessionStr){
        return null;
    }
    const session = JSON.parse(sessionStr);
        //Comparo la fecha de expiracion con ahora

        if (now.getTime() > session.expire){
            localStorage.removeItem("__session");
            return null;
        }
        return session;
}

export function endSession(){
    localStorage.removeItem("__session");
    return null;
}

export function setGoogleUser(user){
    localStorage.setItem("__googleUser", JSON.stringify(user));
}

export function getGoogleUser(){
    const user = localStorage.getItem("__googleUser");
    if (!user){
        return null;
    }
    return JSON.parse(user);
}

export function removeGoogleUser(){
    localStorage.removeItem("__googleUser");
    return null;
}