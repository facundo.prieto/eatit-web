import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
    Grid,
    Container,
    Box,
    CircularProgress,
    Snackbar,
    Alert,
    Typography,
    Slider,
    Backdrop,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    Button,
  } from "@mui/material";
  import colorCodes from "../assets/colors/Colors";

const ListarReclamosClientes = () => {

    return (
        <>
            <NavBar/>
            <Container maxWidth="md"> 
                <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
                    <Grid container spacing={2}>
                        <Grid
                        item
                        xs={12}
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        >
                        <h1>Reclamos de Clientes</h1>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </>
    );
};
export default ListarReclamosClientes;