import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import rutas from "../assets/const/rutas";
import NavBar from "./../components/NavBar/NavBar";
import colorCodes from "./../assets/colors/Colors";
import {
  Container,
  Grid,
  Snackbar,
  Alert,
  Backdrop,
  CircularProgress,
  TextareaAutosize,
} from "@mui/material";
import FormControl from "@mui/material/FormControl";
import axiosCliente from "../components/axios/AxiosCliente";
import { getSession } from "../components/auth/auth";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import ListCardPedido from "./../components/ListCardPedido";

const CrearPedido = ({ navigation }) => {
  let history = useHistory();
  const [snackbar, setSnackbar] = useState(false);
  const [severity, setSeverity] = useState("");
  const [mjeError, setMjeError] = useState("");
  const [datos, setDatos] = useState([]);
  const [loader, setLoader] = useState(false);

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const [dataDirecciones, setDataDirecciones] = useState([]);
  const [nuevosDatos, setNuevosDatos] = useState(false);
  const [restaurante, setRestaurante] = useState([]);

  useEffect(() => {
    setLoader(true);
    axiosCliente
      .get(
        `/direcciones/${getSession().email}`,
        {},
        { headers: { Authorization: `${token}` } }
      )
      .then(function (response) {
        cargarVariables();
        setDataDirecciones(response.data);
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
  }, [nuevosDatos]);

  const cargarVariables = () => {
    const d = JSON.parse(localStorage.getItem("__carrito"));
    const restaurante = localStorage.getItem("__restaurante");

    setDatos(d);
    //console.log(d.costoTotal);
    //console.log(d.emailRestaurante);
    //console.log(d.costoEnvio);

    const costoEnvio = d.costoEnvio ? d.costoEnvio : 0;
    const costoFinal = d.costoTotal + costoEnvio;
    //console.log(costoFinal);
    setRestaurante(JSON.parse(restaurante));
    localStorage.setItem("__montoPaypal", costoFinal);
  };

  const handleCloseSnackbar = () => setSnackbar(false);

  const [direccion, setDireccion] = useState("");

  const handleChangeDireccion = (event) => {
    setDireccion(event.target.value);
  };
  const [comentarios, setComentarios] = useState("");
  const handleCangeComentarios = (event) => {
    setComentarios(event.target.value);
  };

  return (
    <>
      <NavBar />
      <Container>
        <Grid container spacing={2} sx={{ mt: 2, mb: 2 }}>
          <Grid
            xs={6}
            justifyContent="center"
            alignItems="center"
            margin="2"
            bgcolor={colorCodes.backBox}
          >
            <h1 align="center">Direcci&oacute;n de entrega</h1>
            {!dataDirecciones.length ? (
              "No tiene direcciones cargadas en el sistema"
            ) : (
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ ml: 4, mr: 4, mb: 4 }}
              >
                <FormControl fullWidth error>
                  <InputLabel id="demo-simple-select-label">
                    Seleccione una direccion
                  </InputLabel>
                  <Select
                    color="error"
                    labelId="direccion"
                    id="direccion"
                    value={direccion}
                    label="Seleccione una direccion"
                    onChange={handleChangeDireccion}
                  >
                    {dataDirecciones.map((value, key) => {
                      return (
                        <MenuItem value={value.calle + " " + value.numero}>
                          {value.calle} {value.numero}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
            )}
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 4, mr: 4, mb: 4 }}
            >
              <TextareaAutosize
                minRows={12}
                placeholder="Comentarios..."
                style={{ width: 2000, borderColor: colorCodes.red }}
                onChange={handleCangeComentarios}
              />
            </Grid>
          </Grid>
          <Grid
            xs={6}
            justifyContent="center"
            alignItems="center"
            margin="2"
            bgcolor={colorCodes.backBox}
            sx={{ p: 4 }}
          >
            <Grid
              xs={12}
              justifyContent="center"
              alignItems="center"
              margin="2"
              bgcolor="white"
            >
              <ListCardPedido
                data={restaurante}
                costoTotal={datos.costoTotal}
                menus={datos.menus}
                comentarios={comentarios}
                emailRestaurante={restaurante.email}
                direccionEnvio={direccion}
                costoEnvio={datos.costoEnvio}
              />
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};
export default CrearPedido;
