import * as React from "react";
import { useState } from "react";
import { IconButton, Box, ListItemSecondaryAction } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import Typography from "@mui/material/Typography";
import defaultImage from "../../assets/images/defaultImage.png";
import ModalBasic from "./ModalBasic";
import DeleteIcon from "@mui/icons-material/Delete";
import CreateIcon from "@mui/icons-material/Create";
import ModificarPromocion from "../ModificarPromocion";
import BajaPromocion from "../BajaPromocion";
import ModalConfirmacion from "./ModalConfirmacion";
import Tooltip from "@mui/material/Tooltip";

const CardPromo2 = (props) => {
  const {
    data,
    handleNuevosDatosPromocion,
    setMje,
    handleSnackBar,
    setSeverity,
  } = props;

  const [openModal, setOpenModal] = useState(false);
  const handleOpenModal = () => setOpenModal(!openModal);
  const handleCloseModal = () => setOpenModal(false);

  const [openModalDelete, setOpenModalDelete] = useState(false);
  const handleOpenModalDelete = () => setOpenModalDelete(!openModalDelete);
  const handleCloseModalDelete = () => setOpenModalDelete(false);

  const [menu, setMenu] = useState("");

  const handleModal = (value) => {
    setMenu(value);
    setOpenModal(true);
  };

  const handleModalDelete = (value) => {
    setMenu(value);
    setOpenModalDelete(true);
  };

  return (
    <>
      {data.map((value) => (
        <Card sx={{ m: 2 }}>
          <CardActionArea sx={{ display: "flex" }}>
            <CardMedia
              component="img"
              sx={{
                width: 100,
                m: 2,
                height: 100,
                border: "1px solid black",
                borderRadius: 2,
              }}
              image={value.imagen === "" ? defaultImage : value.imagen}
              alt="Imagen no disponible"
            />
            <Box
              sx={{ display: "flex", flexDirection: "column", width: "100%" }}
            >
              <CardContent sx={{ flex: "1 0 auto", mt: 2 }}>
                <Typography component="div" variant="h6">
                  {value.titulo}
                </Typography>
                <Typography
                  variant="subtitle2"
                  color="text.secondary"
                  component="div"
                >
                  {value.descripcion}
                </Typography>
              </CardContent>
            </Box>
            <Box
              sx={{ display: "flex", flexDirection: "column", width: "20%" }}
            >
              <CardContent sx={{ flex: "1 0 auto", mt: 2 }}>
                <ListItemSecondaryAction>
                  <Typography
                    component="div"
                    variant="subtitle2"
                    color="text.secondary"
                    textAlign="right"
                  >
                    {value.promocion.descripcion}
                  </Typography>

                  <Typography
                    component="div"
                    variant="subtitle2"
                    color="text.secondary"
                    textAlign="right"
                    style={{ textDecoration: "line-through" }}
                  >
                    $ {value.precio}
                  </Typography>
                  <Typography
                    component="div"
                    variant="h6"
                    color="red"
                    textAlign="right"
                  >
                    $
                    {Math.round(
                      value.precio -
                        (value.precio * value.promocion.descuento) / 100
                    )}
                  </Typography>
                  <Tooltip title="Modificar promoción">
                    <IconButton
                      variant="contained"
                      onClick={() => handleModal(value)}
                    >
                      <CreateIcon
                        onClick={props.onClick}
                        justify="center"
                        alignItems="center"
                      />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Eliminar promoción">
                    <IconButton
                      variant="contained"
                      onClick={() => handleModalDelete(value)}
                    >
                      <DeleteIcon
                        onClick={props.onClick}
                        justify="center"
                        alignItems="center"
                      />
                    </IconButton>
                  </Tooltip>
                </ListItemSecondaryAction>
              </CardContent>
            </Box>
          </CardActionArea>
        </Card>
      ))}

      <ModalBasic
        open={openModal}
        onClose={handleCloseModal}
        content={
          <ModificarPromocion
            datos={menu}
            handleOpenModal={handleOpenModal}
            handleNuevosDatosPromocion={handleNuevosDatosPromocion}
            setMje={setMje}
            handleSnackBar={handleSnackBar}
            setSeverity={setSeverity}
          />
        }
      />

      <ModalConfirmacion
        open={openModalDelete}
        onClose={handleCloseModalDelete}
        content={
          <BajaPromocion
            datos={menu}
            setOpenModalDelete={setOpenModalDelete}
            handleOpenModalDelete={handleOpenModalDelete}
            handleNuevosDatosPromocion={handleNuevosDatosPromocion}
            setMje={setMje}
            handleSnackBar={handleSnackBar}
            setSeverity={setSeverity}
          />
        }
      />
    </>
  );
};

export default CardPromo2;
