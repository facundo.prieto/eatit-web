import React, { useState, useEffect } from "react";
import { Grid, Box, Button, Alert, Snackbar, Container } from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import axiosCliente from "../components/axios/AxiosCliente";
import axiosUsuario from "../components/axios/AxiosUsuario";
import LogoLetras from "../components/logo/LogoLetras";
import Input from "./../components/basicos/Input";
import { setSession, getGoogleUser } from "../components/auth/auth";
import { useHistory, Link } from "react-router-dom";
import rutas from "./../assets/const/rutas";

const CrearUsuarioCliente = () => {
  const [alertaError, setAlertaError] = useState(false);
  const [severity, setSeverity] = useState("");
  const [msgErr, setMsgErr] = useState("");

  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [email, setEmail] = useState("");
  const [googleToken, serGoogleToken] = useState("");

  const handleNombre = (event) => setNombre(event.target.value);
  const handleApellido = (event) => setApellido(event.target.value);

  let history = useHistory();

  useEffect(() => {
    let googleUser = getGoogleUser();
    //console.log(googleUser);
    setNombre(googleUser.nombre);
    setApellido(googleUser.apellido);
    setEmail(googleUser.email);
    serGoogleToken(googleUser.googleToken);
  }, []);

  const handleComenzar = (event) => {
    //Controlo que se hayan cargado todos los datos
    if (nombre === "" || apellido === "") {
      //Si se registra con google no controlo la contraseña
      setAlertaError(true);
      setSeverity("error");
      setMsgErr("Debe completar todos los datos solicitados.");
    } else {
      //Mando los datos del state al crearUsuarioCliente del ClienteController
      let token = "";
      axiosCliente
        .post(
          "/crear",
          {
            nombre: nombre,
            apellido: apellido,
            email: email,
            password: "",
            googleUser: true,
            direcciones: [],
          },
          {
            headers: {
              Authorization: `${token}`,
            },
          }
        )
        .then(function (response) {
          //console.log(response);
          if (response.status == 200) {
            setAlertaError(true);
            setSeverity("success");
            setMsgErr(response.data);

            axiosUsuario
              .post("/login/google", {
                token: googleToken,
              })
              .then(function (response) {
                //console.log(response.data);
                if (response.status == 200) {
                  setSession({
                    nombre: response.data.nombre,
                    email: response.data.email,
                    token: response.data.token,
                    direcciones: response.data.direcciones,
                    rol: response.data.rol,
                  });
                  history.push(rutas.direcciones_cliente);
                } else {
                  setAlertaError(true);
                  setSeverity("error");
                  setMsgErr(response.data);
                }
              })
              .catch(function (error) {
                console.log(error);
              });
          } else {
            setAlertaError(true);
            setSeverity("error");
            setMsgErr(response.data);
          }
        })
        .catch(function (error) {
          console.log(error.response);
          if (error.response) {
            // Request made and server responded
            setAlertaError(true);
            setSeverity("error");
            setMsgErr(error.response.data);
          } else {
            // Something happened in setting up the request that triggered an Error
            setAlertaError(true);
            setSeverity("error");
            setMsgErr(error.message);
          }
        });
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAlertaError(false);
    setSeverity("error");
    setMsgErr("");
  };

  return (
    <>
      <Container maxWidth="md">
        <LogoLetras height="80" withMargin="true" />
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            {/* Título */}
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Bienvenido {nombre}!</h1>
            </Grid>

            {/* Campos */}
            <>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input id="nombre" value={nombre} onChange={handleNombre} />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="apellido"
                  value={apellido}
                  onChange={handleApellido}
                />
              </Grid>
            </>

            {/* Botón */}

            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Link to={rutas.iniciar_sesion}>
                <Button variant="contained" color="error" sx={{ mb: 2, m: 4 }}>
                  Volver al inicio
                </Button>
              </Link>

              <Button
                variant="contained"
                color="error"
                sx={{ mb: 2, m: 4 }}
                onClick={handleComenzar}
              >
                COMENZAR
              </Button>
            </Grid>
          </Grid>
        </Box>

        <Snackbar
          open={alertaError}
          autoHideDuration={6000}
          onClose={handleClose}
        >
          <Alert
            onClose={handleClose}
            severity={severity}
            sx={{ width: "100%" }}
          >
            {msgErr}
          </Alert>
        </Snackbar>
      </Container>
    </>
  );
};

export default CrearUsuarioCliente;
