import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";
import { red } from "@mui/material/colors";

const CheckboxBasic = (props) => {
  const { label, name, onChange, checked = false } = props;
  return (
    <FormControlLabel
      control={
        <Checkbox
          sx={{
            color: red[800],
            "&.Mui-checked": {
              color: red[600],
            },
          }}
          checked={checked}
          onChange={onChange}
          name={name}
        />
      }
      label={label}
    />
  );
};

export default CheckboxBasic;
