import React, { useState } from "react";
import {
  Container,
  Grid,
  Button,
  Box,
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import { getSession } from "./auth/auth";
import rutas from "../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "../assets/colors/Colors";
import AxiosReclamo from "./axios/AxiosReclamo";

const EstadoReclamo = (props) => {
  const {
    data,
    handleNuevosDatos,
    numeroReclamo,
    setLoader,
    setMje,
    setSnackbar,
    setSeverity,
    handleOpenModalReclamo,
  } = props;

  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [estadoReclamo, setEstadoReclamo] = React.useState("");

  const handelRadioChange = (event, value) => {
    setEstadoReclamo(value);
  };

  const cambiarEstado = () => {
    AxiosReclamo.post(
      `/estado/cambiar/${data.numeroReclamo}/${estadoReclamo}`,
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        console.log(response);
        if (response.status == 200) {
          setMje(
            `El estado del reclamo #${numeroReclamo} paso a  ${estadoReclamo}`
          );
          setSeverity("success");
          setLoader(false);
          setSnackbar(true);
        } else {
          setMje(
            `Ocurrio un error al querer cambiar de estado para el reclamo ${numeroReclamo}`
          );
          setSeverity("error");
          setSnackbar(true);
          setLoader(false);
        }
      })
      .catch(function (error) {
        if (error.response) {
          setMje(error);
          setSeverity("error");
          setSnackbar(true);
          setLoader(false);
        } else {
          setMje(`Ocurrió un error en el sistema, intente nuevamente...`);
          setSeverity("error");
          setSnackbar(true);
          setLoader(false);
        }
      });
    handleOpenModalReclamo();
    handleNuevosDatos();
  };
  return (
    <>
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 4 }}
            >
              <h4>Indica el estado del reclamo</h4>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 4 }}
            >
              <FormControl component="fieldset">
                <RadioGroup
                  defaultValue="Resuelto"
                  name="radio-buttons-group"
                  onChange={handelRadioChange}
                  value={estadoReclamo}
                >
                  <FormControlLabel
                    value="RESUELTO"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="Resuelto"
                  />
                  <FormControlLabel
                    value="NO_RESUELTO"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="No resuelto"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={() => cambiarEstado()}
              >
                Confirmar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default EstadoReclamo;
