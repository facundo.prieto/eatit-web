import React, { Component } from "react";
import { Grid, Box, Button, Alert, Snackbar } from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import AxiosUsuario from './axios/AxiosUsuario';

class UsuarioDetalle extends Component{
    constructor(){
        super();
        this.state = {
            nombre: "",
            apellido: "",
            email: "",
            bloqueado: false,
            alertaError: false,
            msgErr:""
        }
    }

    handleBloquear = (event) => {
        var t = this;
        AxiosUsuario.post(`/bloquearUsuario/${this.state.email}`)
        .then(function (response) {
            console.log(response);
            if(response.data.error !== null){
                t.setState({ alertaError: true, msgErr:response.data.error });
            }else{
                t.setState({ alertaError: true, msgErr:response.data, bloqueado:true });
            }
        })
    }

    handleDesbloquear = (event) => {
        var t = this;
        AxiosUsuario.post(`/desbloquearUsuario/${this.state.email}`)
        .then(function (response) {
            console.log(response);
            if(response.data.error !== null){
                t.setState({ alertaError: true, msgErr:response.data.error });
            }else{
                t.setState({ alertaError: true, msgErr:response.data, bloqueado:false });
            }
        })
    }

    handleClose = (event, reason) => {
      if (reason === "clickaway") {
        return;
      }
      this.setState({ alertaError: false, msgErr:"" });
    };


    componentDidMount(){
        //console.log(this.props);
        this.setState({ 
            nombre: this.props.usuario.nombre,
            apellido: this.props.usuario.apellido,
            email: this.props.usuario.email,
            bloqueado: (this.props.usuario.estado === "BLOQUEADO")
        });
    }

    render(){
        return (
            <>
            <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
                <Grid container spacing={2}>
                    <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                        <h2>Usuario {this.state.nombre}  {this.state.apellido}</h2>
                    </Grid>
                    
                    <Grid item xs={12} display="flex" justifyContent="center" alignItems="center"  marginY={1}>
                        <label>Correo: {this.state.email}</label>
                    </Grid>
                    
	                {!this.state.bloqueado ? 
                        <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                            <Button variant="contained" color="success" sx={{ mb: 2 }} onClick={this.handleBloquear}>
                                Bloquear
                            </Button>
                        </Grid>
                    : 
                        <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                            <Button variant="contained" color="success" sx={{ mb: 2 }} onClick={this.handleDesbloquear}>
                                Desbloquear
                            </Button>
                        </Grid>
                    }
                </Grid>
            </Box>

            <Snackbar open={this.state.alertaError} autoHideDuration={6000} onClose={this.handleClose}>
                <Alert onClose={this.handleClose} severity="error" sx={{ width: "100%" }}>
                    {this.state.msgErr}
                </Alert>
            </Snackbar>
            </>
        );
    }
}

export default UsuarioDetalle;