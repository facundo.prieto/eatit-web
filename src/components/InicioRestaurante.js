import React, { useState, useEffect } from "react";
import AxiosRestaurante from "./axios/AxiosRestaurante";
import { getSession } from "./auth/auth";
import { useHistory } from "react-router-dom";
import rutas from "../assets/const/rutas";
import { Button } from "@mui/material";

const InicioRestaurante = (props) => {
  const { setLoader, setSnackbar, setSeverity, setMjeError } = props;
  const [data, setData] = useState([]);
  let history = useHistory();
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [abierto, setAbierto] = useState();
  useEffect(() => {
    setLoader(true);

    AxiosRestaurante.get(
      `/buscar/full/${getSession().email}`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        console.log(response.data);
        setData(response.data);
        setAbierto(response.data.abierto);
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
  }, []);

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };

  const abrir = (email) => {
    setLoader(true);
    AxiosRestaurante.post(
      `/apertura/${email}`,
      {},
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        console.log(response);
        setAbierto(!abierto);
        setSeverity("success");
        setSnackbar(true);
        setMjeError(`El restaurante ahora está abierto`);
        setLoader(false);
      })
      .catch(function (error) {
        //console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
    setLoader(false);
  };

  const cerrar = (email) => {
    setLoader(true);
    AxiosRestaurante.post(
      `/cierre/${email}`,
      {},
      {
        headers: { Authorization: `${token}` },
      }
    )
      .then(function (response) {
        console.log(response);
        setAbierto(!abierto);
        setSeverity("success");
        setSnackbar(true);
        setMjeError(`El restaurante ahora está cerrado`);
        setLoader(false);
      })
      .catch(function (error) {
        //console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
    setLoader(false);
  };
  return (
    <>
      {abierto ? (
        <Button
          variant="contained"
          color="error"
          sx={{ mb: 2 }}
          onClick={() => cerrar(data.email)}
        >
          Cerrar Restaurante
        </Button>
      ) : (
        <Button
          variant="contained"
          color="success"
          sx={{ mb: 2 }}
          onClick={() => abrir(data.email)}
        >
          Abrir Restaurante
        </Button>
      )}
    </>
  );
};

export default InicioRestaurante;
