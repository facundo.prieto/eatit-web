import React, { useEffect, useState } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
  Container,
  Grid,
  Snackbar,
  Alert,
  Backdrop,
  CircularProgress,
  Button,
} from "@mui/material";
import colorCodes from "./../assets/colors/Colors";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";
import AxiosPedido from "./../components/axios/AxiosPedido";
import { getSession } from "../components/auth/auth";

const PaymentOk = () => {
  let history = useHistory();
  const handleContinuar = () => {
    history.push(rutas.lista_restaurantes);
  };

  const [snackbar, setSnackbar] = useState(false);
  const [severity, setSeverity] = useState("");
  const [mjeError, setMjeError] = useState("");
  const [loader, setLoader] = useState(false);

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  useEffect(() => {
    setLoader(true);
    let datos = JSON.parse(localStorage.getItem("__pedido_realizado"));

    let dataEnviar = {
      comentarios: datos.comentarios,
      emailCliente: datos.emailCliente,
      emailRestaurante: datos.emailRestaurante,
      estaPago: datos.metodoPago === "PAYPAL" ? true : false,
      menus: datos.menus,
      metodoPago: datos.metodoPago,
      monto: datos.monto,
      tiempoEstimado: 0,
      direccion: datos.direccion,
    };

    console.log(dataEnviar);

    AxiosPedido.post(`/crear/`, dataEnviar, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        if (response.status == 200) {
          //console.log(response);
          setSeverity("success");
          setSnackbar(true);
          setMjeError(`El pedido fue realizado`);

          setTimeout(() => {
            history.push(rutas.lista_restaurantes);
          }, 3000);
        } else {
          setSeverity("error");
          setSnackbar(true);
          setMjeError(response.message);
        }
      })
      .catch(function (error) {
        console.log(error);
        setLoader(false);
      });
    setLoader(false);

    localStorage.removeItem("__carrito");
    localStorage.removeItem("__montoPaypal");
    localStorage.removeItem("__restaurante");
    localStorage.removeItem("__email_restoran");
    localStorage.removeItem("__pedido_realizado");
  }, []);

  const handleCloseSnackbar = () => setSnackbar(false);
  return (
    <>
      <NavBar />
      <Container>
        <Grid container spacing={2} sx={{ mt: 2, mb: 2 }} display="flex">
          <Grid
            xs={12}
            justifyContent="center"
            alignItems="center"
            margin="2"
            bgcolor={colorCodes.backBox}
          >
            <h1 align="center">Pago realizado correctamente</h1>
          </Grid>
          <Grid
            xs={12}
            justifyContent="center"
            alignItems="center"
            display="flex"
            margin="2"
            bgcolor={colorCodes.backBox}
            sx={{ pb: 4 }}
          >
            <Button
              color="error"
              variant="contained"
              onClick={() => handleContinuar()}
            >
              Continuar
            </Button>
          </Grid>
        </Grid>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default PaymentOk;
