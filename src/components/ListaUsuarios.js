import React, { useState } from "react";
import {
  TableBody,
  Button,
  Snackbar,
  Alert,
  TableRow,
  TableCell,
  InputAdornment,
} from "@mui/material";
import useTable from "./useTable";
import { Search } from "@mui/icons-material";
import Input from "./../components/basicos/Input";
import DoneIcon from "@mui/icons-material/Done";
import BlockIcon from "@mui/icons-material/Block";
import Grid from "@mui/material/Grid";
import DeleteIcon from "@mui/icons-material/Delete";
import AxiosUsuario from "./axios/AxiosUsuario";
import AxiosAdmin from "./axios/AxiosAdmin";
import { getSession } from "./../components/auth/auth";

const headCells = [
  { id: "nombre", label: "Nombre", disableSorting: true },
  { id: "apellido", label: "Apellido", disableSorting: true },
  { id: "email", label: "Correo", disableSorting: true },
  { id: "calificacion", label: "Calificación" },
  { id: "cantidadPedidos", label: "Cant. de pedidos" },
  { id: "estado", label: "Estado", disableSorting: true },
];

const ListaUsuarios = ({ listaUsuarios, cargarCliente }) => {
  const [userSelected, setUserSelected] = useState(null);
  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }
  const [records, setRecords] = useState(listaUsuarios);
  const [filterFn, setFilterFn] = useState({
    fn: (items) => {
      return items;
    },
  });

  const [alertaError, setAlertaError] = useState(false);
  const [msgErr, setMsgErr] = useState("");
  const [severity, setSeverity] = useState("error");

  const { TblContainer, TblHead, TblPagination, recordsAfterPagingAndSorting } =
    useTable(records, headCells, filterFn);

  const handleSearch = (e) => {
    let target = e.target;
    setFilterFn({
      fn: (items) => {
        if (target.value == "") return items;
        else
          return items.filter(
            (x) =>
              x.nombre.toLowerCase().includes(target.value) ||
              x.nombre.includes(target.value) ||
              x.apellido.toLowerCase().includes(target.value) ||
              x.apellido.includes(target.value) ||
              x.email.toLowerCase().includes(target.value) ||
              x.email.includes(target.value)
          );
      },
    });
  };

  const handleSelect = (user) => {
    //console.log(user)
    setUserSelected(user);
  };

  const handleBorrar = () => {
    AxiosAdmin.delete(`/eliminar/${userSelected.email}`, {
      headers: { Authorization: `${token}` },
    })
      .then(function (response) {
        console.log(response);
        if (response.status == 200) {
          setAlertaError(true);
          setMsgErr(
            `El cliente ${userSelected.nombre} ${userSelected.apellido} fue eliminado`
          );
          setSeverity("success");
          console.log("yyyyy");
          cargarCliente();
        } else {
          setAlertaError(true);
          setMsgErr(response.data);
          setSeverity("error");
        }
      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          // Request made and server responded
          setAlertaError(true);
          setMsgErr(error.response.data);
          setSeverity("error");
        } else {
          // Something happened in setting up the request that triggered an Error
          setAlertaError(true);
          setMsgErr(error.message);
          setSeverity("error");
        }
      });
  };
  const handleBloquear = () => {
    AxiosUsuario.put(
      `/bloquear/${userSelected.email}`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        //console.log(response.data);
        if (response.status == 200) {
          setAlertaError(true);
          setMsgErr(response.data);
          setSeverity("success");
          cargarCliente();
        } else {
          setAlertaError(true);
          setMsgErr(response.data);
          setSeverity("error");
        }
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          setAlertaError(true);
          setMsgErr(error.response.data);
          setSeverity("error");
        } else {
          // Something happened in setting up the request that triggered an Error
          setAlertaError(true);
          setMsgErr(error.message);
          setSeverity("error");
        }
      });
  };

  const handleDesbloquear = () => {
    let token = "";
    if (getSession() != null && getSession().token != null) {
      token = getSession().token;
    }

    AxiosUsuario.put(
      `/desbloquear/${userSelected.email}`,
      {},
      { headers: { Authorization: `${token}` } }
    )
      .then(function (response) {
        console.log(response.data);
        if (response.status == 200) {
          setAlertaError(true);
          setMsgErr(response.data);
          setSeverity("success");
          cargarCliente();
        } else {
          setAlertaError(true);
          setMsgErr(response.data);
          setSeverity("error");
        }
      })
      .catch(function (error) {
        //console.log(error.response);
        if (error.response) {
          // Request made and server responded
          setAlertaError(true);
          setMsgErr(error.response.data);
          setSeverity("error");
        } else {
          // Something happened in setting up the request that triggered an Error
          setAlertaError(true);
          setMsgErr(error.message);
          setSeverity("error");
        }
      });
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAlertaError(false);
    setMsgErr("");
  };

  return (
    <>
      <Grid container spacing={2}>
        <Grid
          item
          xs={12}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Input
            id="nombre-local"
            label="Buscar usuario por nombre o correo"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              ),
            }}
            onChange={handleSearch}
          />
        </Grid>
        <Grid
          item
          xs={12}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <TblContainer>
            <TblHead />
            <TableBody>
              {recordsAfterPagingAndSorting().map((item, key) => (
                <TableRow key={key} onClick={() => handleSelect(item)}>
                  <TableCell>{item.nombre}</TableCell>
                  <TableCell>{item.apellido}</TableCell>
                  <TableCell>{item.email}</TableCell>
                  <TableCell align="center">{item.calificacion}</TableCell>
                  <TableCell align="center">{item.cantidadPedidos}</TableCell>
                  <TableCell align="center">
                    {item.estado !== "BLOQUEADO" ? (
                      <DoneIcon color="success" />
                    ) : (
                      <BlockIcon color="error" />
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </TblContainer>
        </Grid>
        <Grid item xs={12} alignItems="center">
          <TblPagination />
        </Grid>

        <Grid item xs={12} alignItems="center" textAlign="center">
          {userSelected != null && userSelected.estado == "BLOQUEADO" ? (
            <Button
              variant="contained"
              color="success"
              onClick={handleDesbloquear}
            >
              <DoneIcon /> Desbloquear a {userSelected.nombre}{" "}
              {userSelected.apellido}
            </Button>
          ) : null}
          {userSelected != null && userSelected.estado != "BLOQUEADO" ? (
            <Button variant="contained" color="error" onClick={handleBloquear}>
              <BlockIcon /> Bloquear a {userSelected.nombre}{" "}
              {userSelected.apellido}
            </Button>
          ) : null}

          {userSelected ? (
            <Button
              variant="contained"
              color="error"
              onClick={handleBorrar}
              sx={{ ml: 2 }}
            >
              <DeleteIcon /> Eliminar a {userSelected.nombre}{" "}
              {userSelected.apellido}
            </Button>
          ) : null}
        </Grid>
      </Grid>
      <Snackbar
        open={alertaError}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {msgErr}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListaUsuarios;
