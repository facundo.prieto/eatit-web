import React, { useState } from "react";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import Typography from "@mui/material/Typography";
import AddIcon from "@mui/icons-material/Add";
import CloseIcon from "@mui/icons-material/Close";
import { Container, Grid, Box, Button } from "@mui/material";
import CheckIcon from "@mui/icons-material/Check";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import AxiosPedido from "../components/axios/AxiosPedido";

const GestionarDevolucion = (props) => {
  const {
    setOpenModalDevolucion,
    setSnackbar,
    setLoader,
    datos,
    numeroPedido,
    handleOpenModalDevolucion,
    handleNuevosDatos,
    setMje,
    setSeverity,
  } = props;

  let history = useHistory();

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const handleClose = () => {
    setOpenModalDevolucion(false);
  };

  const handleClick = () => {
    AxiosPedido.get(`/listar/${numeroPedido}`).then(function (response) {
      let body = {
        id: response.data.idPedido,
        monto: response.data.monto,
        fecha: null,
      };
      AxiosRestaurante.post(
        `/devolucion/gestionar/${datos.numeroReclamo}`,
        body,
        { headers: { Authorization: `${token}` } }
      )
        .then(function (response) {
          if (response.status == 200) {
            setMje(`Se realizó la devolución correctamente`);
            setSeverity("success");
            setLoader(false);
            setSnackbar(true);
            datos.estadoReclamo = "RESUELTO";
            handleOpenModalDevolucion();
            handleNuevosDatos();
          } else {
            setMje(
              `Ocurrio un error al querer realizar la devolución para el reclamo ${datos.numeroReclamo}`
            );
            setSeverity("error");
            setSnackbar(true);
            setLoader(false);
          }
        })
        .catch(function (error) {
          if (error.response) {
            setMje(error);
            setSeverity("error");
            setSnackbar(true);
            setLoader(false);
          } else {
            setMje(`Ocurrió un error en el sistema, intente nuevamente...`);
            setSeverity("error");
            setSnackbar(true);
            setLoader(false);
          }
        });
    });
  };

  return (
    <>
      <Container maxWidth="md">
        <Box sx={{ mb: 4 }}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Confirmar Devoluci&oacute;n
          </Typography>
        </Box>

        <Grid
          display="flex"
          justifyContent="center"
          alignItems="center"
          vertical-align="bottom"
        >
          <Button
            size="small"
            variant="contained"
            color="error"
            sx={{ mb: -1, mr: 2 }}
            onClick={handleClose}
          >
            <CloseIcon />
            Cancelar
          </Button>
          <Button
            size="small"
            variant="contained"
            color="success"
            sx={{ mb: -1 }}
            onClick={handleClick}
          >
            <CheckIcon />
            Confirmar
          </Button>
        </Grid>
      </Container>
    </>
  );
};

export default GestionarDevolucion;
