import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import { Container, Box, Grid, Button } from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import ModalPedidosEnCurso from "./../components/ModalPedidosEnCurso";
import InfoIcon from "@mui/icons-material/Info";
import ModalBasic from "./../components/basicos/ModalBasic";
import RealizarReclamo from "./../components/RealizarReclamo";

const Inicio = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );
  const handleIrAlReclamo = (id) => {
    history.push(rutas.realizar_reclamo);
    //alert("Pedido id: ".id);
  };

  const [idPedido, setIdPedido] = useState(419);

  const [openModalMenu, setOpenModalMenu] = useState(false);
  const handleCloseModalMenu = () => setOpenModalMenu(false);
  return (
    <>
      <NavBar />
      <Container maxWidth="md"></Container>
    </>
  );
};

export default Inicio;
