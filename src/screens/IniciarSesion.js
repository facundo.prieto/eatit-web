import React, { Component } from "react";
import { Grid, Box, TextField, Button, Alert, Snackbar } from "@mui/material";
import { Link } from "react-router-dom";
import LogoLetras from "../components/logo/LogoLetras";
import LogoGloboHilo from "../components/logo/LogoGloboHilo";
import colorCodes from "../assets/colors/Colors";
import GoogleIcon from "@mui/icons-material/Google";
import firebase from "../firebaseConfig";
import {
  setSession,
  setGoogleUser,
  removeGoogleUser,
} from "../components/auth/auth";
import axiosUsuario from "../components/axios/AxiosUsuario";
import LogoGloboCuadrado from "../components/logo/LogoGloboCuadrado";
import rutas from "./../assets/const/rutas";

class IniciarSesion extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      contrasena: "",
      alertaError: false,
      msgErr: "",
    };

    this.handleAuth = this.handleAuth.bind(this);
  }

  handleEmail = (event) => {
    this.setState({ email: event.target.value });
  };

  handleContrasena = (event) => {
    this.setState({ contrasena: event.target.value });
  };

  handleComenzar = (event) => {
    if (this.state.email === "" || this.state.contrasena === "") {
      //&& !this.state.singUpGoogle))  //si se registra con google no es necesario que se ingrese la contraseña
      this.setState({
        alertaError: true,
        msgErr: "Debe completar todos los datos solicitados.",
      });
    } else {
      var t = this;
      axiosUsuario
        .post("/login", {
          email: this.state.email,
          password: this.state.contrasena,
        })
        .then(function (response) {
          //console.log(response);
          if (response.status == 200) {
            setSession({
              nombre: response.data.nombre,
              email: response.data.email,
              token: response.data.token,
              direcciones: response.data.direcciones,
              rol: response.data.rol,
            });
            console.log(response.data.rol);
            if (response.data.rol === "CLIENTE") {
              t.props.history.push(rutas.lista_restaurantes);
            } else if (response.data.rol === "RESTAURANTE") {
              t.props.history.push(rutas.bandeja_entrada_pedidos);
            } else if (response.data.rol === "ADMINISTRADOR") {
              t.props.history.push(rutas.crear_usuario_administrador);
            }
          } else {
            t.setState({
              alertaError: true,
              msgErr: response.data,
            });
          }
        })
        .catch(function (error) {
          if (error.response) {
            // Request made and server responded
            t.setState({
              alertaError: true,
              msgErr: error.response.data,
            });
          } else {
            // Something happened in setting up the request that triggered an Error
            t.setState({
              alertaError: true,
              msgErr: error.message,
            });
          }
        });
    }
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alertaError: false, msgErr: "" });
  };

  //Función de logueo de la documentación de Firebase, con modificaciones para que luego de loguearse grabe el usuario en una base de datos
  onSignIn = (googleUser) => {
    //console.log(googleUser)
    //const idToken = await firebase.auth().currentUser.getIdToken()

    const accessToken = googleUser.credential.accessToken;

    // Si es un usuario nuevo, se crea en la base de datos (para la prueba es la Realtime Database de Firebase, pero deberíamos usar la api interna)
    if (googleUser.additionalUserInfo.isNewUser) {
      //console.log(` isNewUser `)
      // Redirijo al crear usuario cliente pasándole los datos ya obtenidos por parámetro
      setGoogleUser({
        nombre: googleUser.additionalUserInfo.profile.given_name,
        apellido: googleUser.additionalUserInfo.profile.family_name,
        email: googleUser.user.email,
        googleToken: accessToken,
      });
      this.props.history.push(rutas.crear_usuario_goolge);
    } else {
      // Si no es un usuario nuevo, se solicita un token al backend y se almacena en el local storage
      var t = this;
      axiosUsuario
        .post("/login/google", {
          token: accessToken,
        })
        .then(function (response) {
          //console.log(response.data);
          if (response.status == 200) {
            setSession({
              nombre: response.data.nombre,
              email: response.data.email,
              token: response.data.token,
              direcciones: response.data.direcciones,
              rol: response.data.rol,
            });
            if (response.data.rol === "CLIENTE") {
              t.props.history.push(rutas.lista_restaurantes);
            } else if (response.data.rol === "RESTAURANTE") {
              t.props.history.push(rutas.raiz);
            } else if (response.data.rol === "ADMINISTRADOR") {
              t.props.history.push(rutas.crear_usuario_administrador);
            }
          } else {
            t.setState({
              alertaError: true,
              msgErr: response.data,
            });
          }
        })
        .catch(function (error) {
          console.log(error.response);
          if (error.response) {
            // Request made and server responded
            t.setState({
              alertaError: true,
              msgErr: error.response.data,
            });
          } else {
            // Something happened in setting up the request that triggered an Error
            t.setState({
              alertaError: true,
              msgErr: error.message,
            });
          }
        });
    }
  };

  handleAuth() {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithPopup(provider)
      // .then(result => this.signInWithGoogleAsync(result))
      .then((result) => this.onSignIn(result))
      // console.log(`${result.user.email} ha iniciado sesion`)
      .catch((error) => console.log(`Error ${error.code}: ${error.message}`));
  }

  handleRegistrar = () => {
    removeGoogleUser();
    //this.props.history.push(`/crear-usuario-cliente`);
    this.props.history.push(rutas.crear_usuario_cliente);
  };

  render() {
    return (
      <>
        <Grid container spacing={2} sx={{ mb: 2 }}>
          <Grid
            item
            xs={8}
            justifyContent="center"
            alignItems="center"
            margin="2"
          >
            <LogoGloboHilo height="730" />
          </Grid>
          <Grid
            item
            xs={3}
            display="flex"
            justifyContent="center"
            alignItems="center"
            direction="column"
          >
            <LogoLetras height="50" width="auto" margin="40" />
            <Box
              bgcolor={colorCodes.backBox}
              sx={{ mt: 2, mb: 2, mx: 100, p: 2 }}
            >
              <Grid container spacing={2}>
                <Grid
                  item
                  xs={12}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                  direction="column"
                  sx={{ mt: 4 }}
                >
                  <TextField
                    sx={{
                      width: "227px",
                      mr: 6, // margin right
                      ml: 6, // margin left
                      mb: 2,
                      borderBottom: "1px solid white", // borde blanco
                      "& label.Mui-focused": {
                        color: "white",
                      },
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "white",
                      },
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          borderColor: "white",
                        },
                        "&:hover fieldset": {
                          borderColor: "white",
                        },
                        "&.Mui-focused fieldset": {
                          borderColor: "white",
                        },
                      },
                    }}
                    id="email"
                    label="Ingresa tu e-mail"
                    variant="standard"
                    onChange={this.handleEmail}
                  />
                  <TextField
                    sx={{
                      width: "227px",
                      mr: 6, // margin right
                      ml: 6, // margin left
                      mb: 2,
                      borderBottom: "1px solid white", // borde blanco
                      "& label.Mui-focused": {
                        color: "white",
                      },
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "white",
                      },
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          borderColor: "white",
                        },
                        "&:hover fieldset": {
                          borderColor: "white",
                        },
                        "&.Mui-focused fieldset": {
                          borderColor: "white",
                        },
                      },
                    }}
                    id="contrasena"
                    type="password"
                    id="pass"
                    label="Ingresa tu contrase&ntilde;a"
                    variant="standard"
                    onChange={this.handleContrasena}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Button
                    color="error"
                    variant="contained"
                    sx={{ height: "45px", width: "227px", mb: 2 }}
                    onClick={this.handleComenzar}
                  >
                    Iniciar Sesi&oacute;n
                  </Button>
                </Grid>
                <Grid
                  item
                  xs={12}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                  sx={{ mb: 2 }}
                >
                  <Link
                    to={rutas.recuperar_contrasena}
                    style={{ textDecoration: "underline" }}
                  >
                    ¿Olvidaste tu contrase&ntilde;a?
                  </Link>
                </Grid>
              </Grid>
            </Box>
            <Grid container spacing={2} sx={{ mb: 2 }}>
              <Grid
                item
                xs={12}
                sx={{ mt: 2, mb: 2 }}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <span>¿Aún no te registraste?</span>
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Button
                  variant="contained"
                  color="error"
                  sx={{ width: "227px" }}
                  onClick={this.handleRegistrar}
                >
                  <Grid item xs={1} sx={{ mt: 1, ml: 0 }} justifyContent="left">
                    <LogoGloboCuadrado />
                  </Grid>
                  <Grid item xs={11}>
                    <span>Registrate aqu&iacute;</span>
                  </Grid>
                </Button>
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <span> o </span>
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Button
                  variant="contained"
                  sx={{ height: "45px", width: "227px" }}
                  startIcon={<GoogleIcon />}
                  onClick={this.handleAuth}
                >
                  Ingresa con Google
                </Button>
              </Grid>
              <Grid
                item
                xs={12}
                sx={{ mt: 2 }}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <h4>
                  {" "}
                  Registra tu restaurante{" "}
                  <Link
                    to={rutas.crear_usuario_restaurante}
                    style={{ textDecoration: "underline" }}
                  >
                    {" "}
                    aqu&iacute;{" "}
                  </Link>{" "}
                </h4>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Snackbar
          open={this.state.alertaError}
          autoHideDuration={6000}
          onClose={this.handleClose}
        >
          <Alert
            onClose={this.handleClose}
            severity="error"
            sx={{ width: "100%" }}
          >
            {this.state.msgErr}
          </Alert>
        </Snackbar>
      </>
    );
  }
}

export default IniciarSesion;
