import React, { useState, useEffect } from "react";
import { Grid, Box, Button, Alert, Snackbar } from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import { Container } from "@mui/material";
import { useHistory } from "react-router-dom";
import NavBar from "../components/NavBar/NavBar";
import Input from "../components/basicos/Input";
import { getSession } from "../components/auth/auth";
import axiosUser from "../components/axios/AxiosUsuario";
import rutas from "../assets/const/rutas";

const CambiarClave = (props) => {
  let history = useHistory();
  const [alertaError, setAlertaError] = useState(false);
  const [severity, setSeverity] = useState("");
  const [snackbar, setSnackbar] = React.useState(false);
  const [mjeError, setMjeError] = React.useState("");
  const [contrasena, setContrasena] = useState("");
  const [repcontrasena, setRepContrasena] = useState("");
  const [email, setEmail] = React.useState(getSession() ? getSession().email : history.push(rutas.iniciar_sesion));
  const [token, setToken] = React.useState(getSession() ? getSession().token : history.push(rutas.iniciar_sesion));
  const [loader, setLoader] = React.useState(false);
  useEffect(() => {
    setLoader(true);

    
  }, []);

  const cambioClave = () => {
    setLoader(true);
    if (contrasena !== repcontrasena) {
      setAlertaError(true);
      setSeverity("error");
      setMjeError("Debe ingresar la misma contraseña dos veces");
    }else{
      axiosUser
      .put(
        `/cambiarClave/${getSession().email}`,
        {
          nuevaClave: contrasena,
        },
        { headers: { Authorization: `${token}` } }
      )
      .then(function (response) {
        if (response.status === 200) {
          setMjeError(`Se cambio la clave correctamente`);
          setSnackbar(true);
          setSeverity("success");
        }
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
    }
  }

  const handleContrasena = (event) => setContrasena(event.target.value);
  const handleRepContrasena = (event) => setRepContrasena(event.target.value);

 
  const handleCloseSnackbar = () => setSnackbar(false);


  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: colorCodes.black,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <>
      <NavBar /> 
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Cambiar contraseña</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="colum"
              justifyContent="center"
              alignItems="center"
            >
          <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="contrasena"
                  label="Contraseña"
                  type="password"
                  onChange={handleContrasena}
                />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Input
                  id="repcontrasena"
                  label="Repetir contraseña"
                  type="password"
                  onChange={handleRepContrasena}
                />
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{mb:6}}
              >
              <Button
                variant="contained"
                color="error"
                sx={{ mb: 2, my: 4 }}
                onClick={()=>cambioClave()}
              >
                CONFIRMAR
              </Button>
              </Grid>
              </Grid>
          </Grid>
        </Box>
      </Container>

      

      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default CambiarClave;
