// import * as firebase from 'firebase';
import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAkQd8YID4IxQVEJ2nZ5FBL0HH-KmDdNDU",
  authDomain: "login-a4077.firebaseapp.com",
  projectId: "login-a4077",
  storageBucket: "login-a4077.appspot.com",
  messagingSenderId: "148814033726",
  appId: "1:148814033726:web:214bbfc63c08cff0cd726f",
  measurementId: "G-63T93Q9TVW"
};

if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
}

export default firebase;