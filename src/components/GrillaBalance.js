import React, { useState } from "react";
import {
  TableBody,
  Button,
  Snackbar,
  Alert,
  TableRow,
  TableCell,
  InputAdornment,
} from "@mui/material";
import useTable from "./useTable";
import { Search } from "@mui/icons-material";
import Input from "./../components/basicos/Input";
import DoneIcon from "@mui/icons-material/Done";
import BlockIcon from "@mui/icons-material/Block";
import Grid from "@mui/material/Grid";
import DeleteIcon from "@mui/icons-material/Delete";
import AxiosUsuario from "./axios/AxiosUsuario";
import AxiosAdmin from "./axios/AxiosAdmin";
import { getSession } from "./../components/auth/auth";

const headCells = [
  { id: "id", label: "ID Pedido", disableSorting: true },
  { id: "emailCliente", label: "Email Cliente" },
  { id: "fecha", label: "Fecha", disableSorting: true },
  { id: "metodo", label: "Metodo Pago", disableSorting: true },
  { id: "devolucion", label: "Devolucion", disableSorting: true },
  { id: "monto", label: "Monto" },
  { id: "balance", label: "Balance", disableSorting: true },
];

const GrillaBalance = (props) => {
  const { datosBalance } = props;
  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }
  const [records, setRecords] = useState([]);
  const [filterFn, setFilterFn] = useState({
    fn: (items) => {
      return items;
    },
  });

  const [alertaError, setAlertaError] = useState(false);
  const [msgErr, setMsgErr] = useState("");
  const [severity, setSeverity] = useState("error");

  const { TblContainer, TblHead, TblPagination, recordsAfterPagingAndSorting } =
    useTable(datosBalance, headCells, filterFn);

  const handleSelect = (user) => {
    //console.log(user)
    //setUserSelected(user);
  };
     

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAlertaError(false);
    setMsgErr("");
  };

  const buildRow = (item, key) => {
    return (
      <TableRow key={key} onClick={() => handleSelect(item)}>
        <TableCell>{item.id}</TableCell>
        <TableCell align="center">{item.email}</TableCell>
        <TableCell>{item.fecha}</TableCell>
        <TableCell>{item.metodo}</TableCell>
        <TableCell>{item.devolucion ? "Si" : "No"}</TableCell>
        <TableCell align="center">{item.monto}</TableCell>
        <TableCell>{item.balance}</TableCell>
      </TableRow>
    )
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid
          item
          xs={12}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
        </Grid>
        <Grid
          item
          xs={12}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <TblContainer>
            <TblHead />
            <TableBody>
            {recordsAfterPagingAndSorting().map((item, key) => 
              buildRow(item, key)
            )}
            </TableBody>
          </TblContainer>
        </Grid>
        <Grid item xs={12} alignItems="center">
          <TblPagination />
        </Grid>
      </Grid>
      <Snackbar
        open={alertaError}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {msgErr}
        </Alert>
      </Snackbar>
    </>
  );
};

export default GrillaBalance;
