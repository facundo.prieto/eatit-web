import React, { useState } from "react";
import LogoLetras from "./../components/logo/LogoLetras";
import colorCodes from "../assets/colors/Colors";
import Input from "./../components/basicos/Input";
import TimeInput from "../components/basicos/TimeInput";
import {
  Container,
  Fab,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Grid,
  Box,
  Button,
  Alert,
  Snackbar,
  TableCell,
  Paper,
} from "@mui/material";
import CheckboxBasic from "../components/basicos/CheckboxBasic";
import ModalBasic from "./../components/basicos/ModalBasic";
import MapaAgregarDireccion from "./../components/Maps/MapaAgregarDireccion";
import { useForm } from "react-hook-form";
import { useDropzone } from "react-dropzone";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import AxiosImagen from "../components/axios/AxiosImagen";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import AltaMenu from "./AltaMenu";
import CardBasic from "../components/basicos/CardBasic.js";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";

const CrearUsuarioRestaurante = () => {
  let history = useHistory();
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const { handleSubmit } = useForm();
  const [dias, setDias] = useState({
    lunes: false,
    martes: false,
    miercoles: false,
    jueves: false,
    viernes: false,
    sabado: false,
    domingo: false,
  });
  const [checkLunes, setCheckLunes] = useState(false);
  const [checkMartes, setCheckMartes] = useState(false);
  const [checkMiercoles, setCheckMiercoles] = useState(false);
  const [checkJueves, setCheckJueves] = useState(false);
  const [checkViernes, setCheckViernes] = useState(false);
  const [checkSabado, setCheckSabado] = useState(false);
  const [checkDomingo, setCheckDomingo] = useState(false);
  const { lunes, martes, miercoles, jueves, viernes, sabado, domingo } = dias;
  const [openModalMenu, setOpenModalMenu] = useState(false);
  const [severity, setSeverity] = useState();
  const [image, setImage] = useState("");
  const [nombre, setNombre] = useState("");
  const [email, setEmail] = useState("");
  const [calle, setCalle] = useState("");
  const [numero, setNumero] = useState("");
  const [esquina, setEsquina] = useState("");
  const [barrio, setBarrio] = useState("");
  const [contrasena, setContrasena] = useState("");
  const [latitud, setLatitud] = useState(6);
  const [longitud, setLongitud] = useState(0);
  const [cobertura, setCobertura] = useState(0);
  const [costoEnvio, setCostoEnvio] = useState(0);
  const horaInicio = "08:00";
  const horaFin = "23:00";
  const [lunesInicio, setLunesInicio] = useState(horaInicio);
  const [martesInicio, setMartesInicio] = useState(horaInicio);
  const [miercolesInicio, setMiercolesInicio] = useState(horaInicio);
  const [juevesInicio, setJuevesInicio] = useState(horaInicio);
  const [viernesInicio, setViernesInicio] = useState(horaInicio);
  const [sabadoInicio, setSabadoInicio] = useState(horaInicio);
  const [domingoInicio, setDomingoInicio] = useState(horaInicio);
  const [lunesFin, setLunesFin] = useState(horaFin);
  const [martesFin, setMartesFin] = useState(horaFin);
  const [miercolesFin, setMiercolesFin] = useState(horaFin);
  const [juevesFin, setJuevesFin] = useState(horaFin);
  const [viernesFin, setViernesFin] = useState(horaFin);
  const [sabadoFin, setSabadoFin] = useState(horaFin);
  const [domingoFin, setDomingoFin] = useState(horaFin);

  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    onDrop: (file) => {
      setImage(
        Object.assign(file[0], {
          file: file[0],
          filename: file[0].name,
          preview: URL.createObjectURL(file[0]),
        })
      );
    },
  });

  const handleClose = (event, reason) => {
    setSnackbar(false);
  };
  const handleChangeDias = (e) => {
    setDias({
      ...dias,
      [e.target.name]: e.target.checked,
    });
  };
  const handleOpenModalMenu = () => setOpenModalMenu(true);
  const handleCloseModalMenu = () => setOpenModalMenu(false);
  const handleNombre = (event) => setNombre(event.target.value);
  const handleEmail = (event) => setEmail(event.target.value);
  const handleCalle = (event) => setCalle(event.target.value);
  const handleNumero = (event) => setNumero(event.target.value);
  const handleEsquina = (event) => setEsquina(event.target.value);
  const handleBarrio = (event) => setBarrio(event.target.value);
  const handleContrasena = (event) => setContrasena(event.target.value);
  const handleCoordenadas = (lat = 0, lon = 0) => {
    setLatitud(lat);
    setLongitud(lon);
  };
  const handleCobertura = (event) => setCobertura(event.target.value);
  const handleCostoEnvio = (event) => setCostoEnvio(event.target.value);
  const handleLunesInicio = (event) => setLunesInicio(event.target.value);
  const handleLunesFin = (event) => setLunesFin(event.target.value);
  const handleMartesInicio = (event) => setMartesInicio(event.target.value);
  const handleMartesFin = (event) => setMartesFin(event.target.value);
  const handleMiercolesInicio = (event) =>
    setMiercolesInicio(event.target.value);
  const handleMiercolesFin = (event) => setMiercolesFin(event.target.value);
  const handleJuevesInicio = (event) => setJuevesInicio(event.target.value);
  const handleJuevesFin = (event) => setJuevesFin(event.target.value);
  const handleViernesInicio = (event) => setViernesInicio(event.target.value);
  const handleViernesFin = (event) => setViernesFin(event.target.value);
  const handleSabadoInicio = (event) => setSabadoInicio(event.target.value);
  const handleSabadoFin = (event) => setSabadoFin(event.target.value);
  const handleDomingoInicio = (event) => setDomingoInicio(event.target.value);
  const handleDomingoFin = (event) => setDomingoFin(event.target.value);
  const handleCheckLunes = () => setCheckLunes(!checkLunes);
  const handleCheckMartes = () => setCheckMartes(!checkMartes);
  const handleCheckMiercoles = () => setCheckMiercoles(!checkMiercoles);
  const handleCheckJueves = () => setCheckJueves(!checkJueves);
  const handleCheckViernes = () => setCheckViernes(!checkViernes);
  const handleCheckSabado = () => setCheckSabado(!checkSabado);
  const handleCheckDomingo = () => setCheckDomingo(!checkDomingo);
  const [menu1, setMenu1] = useState({
    titulo: "",
    id_resto: "",
    precio: "",
    descripcion: "",
    imagen: "",
    categorias: [],
  });
  const [menu2, setMenu2] = useState({
    titulo: "",
    id_resto: "",
    precio: "",
    descripcion: "",
    imagen: "",
    categorias: [],
  });
  const [menu3, setMenu3] = useState({
    titulo: "",
    id_resto: "",
    precio: "",
    descripcion: "",
    imagen: "",
    categorias: [],
  });
  const handleCrearMenu1 = (data) => setMenu1(data);
  const handleCrearMenu2 = (data) => setMenu2(data);
  const handleCrearMenu3 = (data) => setMenu3(data);

  const handleBorrarMenu1 = () => {
    setMenu1({
      titulo: "",
      precio: "",
      descripcion: "",
      imagen: "",
      categorias: [],
    });
  };
  const handleBorrarMenu2 = () => {
    setMenu2({
      titulo: "",
      precio: "",
      descripcion: "",
      imagen: "",
      categorias: [],
    });
  };
  const handleBorrarMenu3 = () => {
    setMenu3({
      titulo: "",
      precio: "",
      descripcion: "",
      imagen: "",
      categorias: [],
    });
  };

  const crearRestaurant = () => {
    console.log("restaurant");

    if (
      nombre === "" ||
      email === "" ||
      calle === "" ||
      numero === "" ||
      esquina === "" ||
      barrio === "" ||
      latitud === 0 ||
      longitud === 0 ||
      cobertura === 0 ||
      costoEnvio === 0 ||
      image === "" ||
      menu1.titulo === "" ||
      menu2.titulo === "" ||
      menu3.titulo === ""
    ) {
      setSeverity("error");
      setSnackbar(true);
      setMjeError(`Debe completar todos los campos, intente de nuevo`);
    } else {
      var imgData = new FormData();
      imgData.append("filename", image.name);
      imgData.append("file", image.file);

      AxiosImagen.post("/restaurante", imgData, {
        headers: {
          Authorization: `token`,
        },
      })
        .then(function (responseImg) {
          if (responseImg.status == 200) {
            let data = {
              cobertura: parseInt(cobertura),
              costoEnvio: parseInt(costoEnvio),
              direccion: {
                calle: calle,
                numero: numero,
                esquina: esquina,
                barrio: barrio,
              },
              email: email,
              imagen: responseImg.data,
              nombre: nombre,
              password: contrasena,
              ubicacion: {
                latitud: latitud,
                longitud: longitud,
              },
              horarios: [
                {
                  dia: "lunes",
                  horaApertura: checkLunes ? lunesInicio : "",
                  horaCierre: checkLunes ? lunesFin : "",
                },
                {
                  dia: "martes",
                  horaApertura: checkMartes ? martesInicio : "",
                  horaCierre: checkMartes ? martesFin : "",
                },
                {
                  dia: "miercoles",
                  horaApertura: checkMiercoles ? miercolesInicio : "",
                  horaCierre: checkMiercoles ? miercolesFin : "",
                },
                {
                  dia: "jueves",
                  horaApertura: checkJueves ? juevesInicio : "",
                  horaCierre: checkJueves ? juevesFin : "",
                },
                {
                  dia: "viernes",
                  horaApertura: checkViernes ? viernesInicio : "",
                  horaCierre: checkViernes ? viernesFin : "",
                },
                {
                  dia: "sabado",
                  horaApertura: checkSabado ? sabadoInicio : "",
                  horaCierre: checkSabado ? sabadoFin : "",
                },
                {
                  dia: "domingo",
                  horaApertura: checkDomingo ? domingoInicio : "",
                  horaCierre: checkDomingo ? domingoFin : "",
                },
              ],
              menus: [menu1, menu2, menu3],
            };
            console.log(data);

            AxiosRestaurante.post("/crear", data)
              .then(function (response) {
                if (responseImg.status == 200) {
                  setSeverity("success");
                  setSnackbar(true);
                  setMjeError(`Se creó el restaurant ${nombre} correctamente`);

                  setTimeout(() => {
                    history.push(rutas.iniciar_sesion);
                  }, 3000);
                } else {
                  setSeverity("error");
                  setSnackbar(true);
                  setMjeError(response.message);
                }
              })
              .catch(function (error) {
                console.log(error);
                setSeverity("error");
                setSnackbar(true);
                setMjeError("Ocurrió un error en el sistema");
              });
          }
        })
        .catch(function (error) {
          console.log(error);
          setSeverity("error");
          setSnackbar(true);
          setMjeError("Ocurrió un error en el sistema");
        });
    }
  };

  return (
    <>
      <Container maxWidth="md">
        <LogoLetras height="80" withMargin="true" />
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Crear Restaurante</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <h2>
                Ingresa los datos requeridos para ser enviado a un administrador
              </h2>
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="nombre-local"
                label="Nombre del local"
                onChange={handleNombre}
              />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="email"
                label="Correo electronico"
                onChange={handleEmail}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <h2>Selecciona una imagen para el local</h2>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 6, mr: 6, mb: 2 }}
            >
              {image ? (
                <Grid container>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ ml: 6, mr: 6, mb: 2 }}
                  >
                    <Alert severity="success" sx={{ width: "100%" }}>
                      Imagen cargada correctamente
                    </Alert>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    justifyContent="left"
                    alignItems="left"
                    sx={{ ml: 6, mr: 6, mb: 2 }}
                  >
                    <img
                      src={image.preview}
                      style={{
                        display: "block",
                        width: "auto",
                        height: "100px",
                        border: "2px solid white",
                      }}
                    />
                    <div
                      {...getRootProps()}
                      style={{
                        display: "flex",
                        width: "100%",
                        alignItems: "center",
                        justifyContent: "center",
                        padding: "0px 10px",
                        border: "1px dashed white",
                        borderRadius: "5px",
                        marginLeft: "2em",
                      }}
                    >
                      <input {...getInputProps()} />
                      <p>
                        {" "}
                        Haz click aqui o arrastra un archivo para cambiarlo{" "}
                      </p>
                      <div sx={{ m: "1em" }}>
                        <FileUploadIcon />
                      </div>
                    </div>
                  </Grid>
                </Grid>
              ) : (
                <div
                  {...getRootProps()}
                  style={{
                    display: "flex",
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "0px 10px",
                    border: "1px dashed white",
                    borderRadius: "5px",
                  }}
                >
                  <input {...getInputProps()} />
                  <p> Haz click aqui o arrastra un archivo para subirlo </p>
                  <div sx={{ m: "1em" }}>
                    <FileUploadIcon />
                  </div>
                </div>
              )}
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input id="calle" label="Calle" onChange={handleCalle} />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="numero"
                label="Número"
                type="number"
                onChange={handleNumero}
              />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input id="esquina" label="Esquina" onChange={handleEsquina} />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input id="barrio" label="Barrio" onChange={handleBarrio} />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="contrasena"
                label="Contrase&ntilde;a"
                type="password"
                onChange={handleContrasena}
              />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="repetir-contrasena"
                label="Repetir contrase&ntilde;a"
                type="password"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <h2>Seleccionar en el mapa la ubicacion del restaurante</h2>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <MapaAgregarDireccion handleCoordenadas={handleCoordenadas} />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              {latitud === 0 ? (
                <p>Todavia no ha seleccionado una ubicaci&oacute;n</p>
              ) : (
                <>
                  <p>
                    Latitud: {latitud} Longitud: {longitud}
                  </p>
                </>
              )}
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="cobertura"
                type="number"
                label="Distancia m&aacute;xima en metros"
                onChange={handleCobertura}
              />
            </Grid>
            <Grid
              item
              xs={6}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="costo_envio"
                label="Costo de envio"
                onChange={handleCostoEnvio}
                type="number"
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h2>Horarios de atenci&oacute;n</h2>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <TableContainer
                component={Paper}
                sx={{
                  backgroundColor: "inherit",
                  color: "white",
                  "& .MuiTableCell-root": {
                    borderBottom: "0px",
                  },
                }}
              >
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">D&iacute;as</TableCell>
                      <TableCell align="center">Apertura</TableCell>
                      <TableCell align="center">Cierre</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkLunes}
                          onChange={handleCheckLunes}
                          name="lunes"
                          label="Lunes"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-lunes-apertura"
                          type="time"
                          defaultValue={lunesInicio}
                          onChange={handleLunesInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-lunes-cierre"
                          type="time"
                          defaultValue={lunesFin}
                          onChange={handleLunesFin}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkMartes}
                          onChange={handleCheckMartes}
                          name="martes"
                          label="Martes"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-martes-apertura"
                          type="time"
                          defaultValue={martesInicio}
                          onChange={handleMartesInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-martes-cierre"
                          type="time"
                          defaultValue={martesFin}
                          onChange={handleMartesFin}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkMiercoles}
                          onChange={handleCheckMiercoles}
                          name="miercoles"
                          label="Miercoles"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-miercoles-apertura"
                          type="time"
                          defaultValue={miercolesInicio}
                          onChange={handleMiercolesInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-miercoles-cierre"
                          type="time"
                          defaultValue={miercolesFin}
                          onChange={handleMiercolesFin}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkJueves}
                          onChange={handleCheckJueves}
                          name="jueves"
                          label="Jueves"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-jueves-apertura"
                          type="time"
                          defaultValue={juevesInicio}
                          onChange={handleJuevesInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-jueves-cierre"
                          type="time"
                          defaultValue={juevesFin}
                          onChange={handleJuevesFin}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkViernes}
                          onChange={handleCheckViernes}
                          name="viernes"
                          label="Viernes"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-viernes-apertura"
                          type="time"
                          defaultValue={viernesInicio}
                          onChange={handleViernesInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-viernes-cierre"
                          type="time"
                          defaultValue={viernesFin}
                          onChange={handleViernesFin}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkSabado}
                          onChange={handleCheckSabado}
                          name="sabado"
                          label="Sabado"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-sabado-apertura"
                          type="time"
                          defaultValue={sabadoInicio}
                          onChange={handleSabadoInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-sabado-cierre"
                          type="time"
                          defaultValue={sabadoFin}
                          onChange={handleSabadoFin}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <CheckboxBasic
                          checked={checkDomingo}
                          onChange={handleCheckDomingo}
                          name="domingo"
                          label="Domingo"
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-domingo-apertura"
                          type="time"
                          defaultValue={domingoInicio}
                          onChange={handleDomingoInicio}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TimeInput
                          id="time-domingo-cierre"
                          type="time"
                          defaultValue={domingoFin}
                          onChange={handleDomingoFin}
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <h2>Cargue tres men&uacute;s de ejemplo</h2>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  {menu1.titulo === "" ? (
                    <>
                      <CardBasic
                        data={menu1}
                        esNull={true}
                        handleOpenModalMenu={handleOpenModalMenu}
                        handleBorrarMenu={handleBorrarMenu1}
                      />
                      <ModalBasic
                        open={openModalMenu}
                        onClose={handleCloseModalMenu}
                        content={
                          <AltaMenu
                            setSnackbar={setSnackbar}
                            setMje={setMjeError}
                            setSeverity={setSeverity}
                            handleCloseModalMenu={handleClose}
                            isModal={true}
                            showNavBar={false}
                            handleCrearMenu={handleCrearMenu1}
                          />
                        }
                      />
                    </>
                  ) : (
                    <CardBasic
                      data={menu1}
                      esCreacion={true}
                      handleBorrarMenu={handleBorrarMenu1}
                    />
                  )}
                </Grid>
                <Grid item xs={12}>
                  {menu2.titulo === "" ? (
                    <>
                      <CardBasic
                        data={menu2}
                        esNull={true}
                        handleOpenModalMenu={handleOpenModalMenu}
                        handleBorrarMenu={handleBorrarMenu2}
                      />
                      <ModalBasic
                        open={openModalMenu}
                        onClose={handleCloseModalMenu}
                        content={
                          <AltaMenu
                            setSnackbar={setSnackbar}
                            setMje={setMjeError}
                            setSeverity={setSeverity}
                            handleCloseModalMenu={handleClose}
                            isModal={true}
                            showNavBar={false}
                            handleCrearMenu={handleCrearMenu2}
                          />
                        }
                      />
                    </>
                  ) : (
                    <CardBasic
                      data={menu2}
                      esCreacion={true}
                      handleBorrarMenu={handleBorrarMenu2}
                    />
                  )}
                </Grid>
                <Grid item xs={12}>
                  {menu3.titulo === "" ? (
                    <>
                      <CardBasic
                        data={menu3}
                        esNull={true}
                        handleOpenModalMenu={handleOpenModalMenu}
                        handleBorrarMenu={handleBorrarMenu3}
                      />
                      <ModalBasic
                        open={openModalMenu}
                        onClose={handleCloseModalMenu}
                        content={
                          <AltaMenu
                            setSnackbar={setSnackbar}
                            setMje={setMjeError}
                            setSeverity={setSeverity}
                            handleCloseModalMenu={handleClose}
                            isModal={true}
                            showNavBar={false}
                            handleCrearMenu={handleCrearMenu3}
                          />
                        }
                      />
                    </>
                  ) : (
                    <CardBasic
                      data={menu3}
                      esCreacion={true}
                      handleBorrarMenu={handleBorrarMenu3}
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="error"
                sx={{ mb: 2 }}
                onClick={handleSubmit(crearRestaurant)}
              >
                Enviar datos
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default CrearUsuarioRestaurante;
