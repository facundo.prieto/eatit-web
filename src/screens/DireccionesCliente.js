import React, { useState, useEffect } from "react";
import { Grid, Box, Button, Alert, Snackbar } from "@mui/material";
import colorCodes from "../assets/colors/Colors";
import { Container } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import NavBar from "../components/NavBar/NavBar";
import ListDirecciones from "../components/ListDirecciones";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Input from "../components/basicos/Input";
import CloseIcon from "@mui/icons-material/Close";
import { useForm } from "react-hook-form";
import { getSession } from "../components/auth/auth";
import axiosCliente from "../components/axios/AxiosCliente";

const DireccionesCliente = (props) => {
  const { esPagina = true } = props;
  const [openModal, setOpenModal] = useState(false);
  const [snackbar, setSnackbar] = React.useState(false);
  const [mjeError, setMjeError] = React.useState("");
  const { handleSubmit } = useForm();
  const [calle, setCalle] = React.useState("");
  const [numero, setNumero] = React.useState("");
  const [esquina, setEsquina] = React.useState("");
  const [barrio, setBarrio] = React.useState("");
  const [severity, setSeverity] = React.useState();
  const [email, setEmail] = React.useState(getSession().email);
  const [token, setToken] = React.useState(getSession().token);
  const [dataDirecciones, setDataDirecciones] = React.useState([]);
  const [nuevosDatos, setNuevosDatos] = React.useState(false);
  const [loader, setLoader] = React.useState(false);
  useEffect(() => {
    setLoader(true);

    axiosCliente
      .get(
        `/direcciones/${getSession().email}`,
        {},
        { headers: { Authorization: `${token}` } }
      )
      .then(function (response) {
        setDataDirecciones(response.data);
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);

        setSeverity("error");
        setSnackbar(true);
        setMjeError(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
  }, [nuevosDatos]);

  const onSubmit = () => {
    if (calle === "" || numero === "" || esquina === "" || barrio === "") {
      setOpenModal(false);
      setSeverity("error");
      setSnackbar(true);
      setMjeError(`Debe completar todos los campos, intente de nuevo`);
    } else {
      setLoader(true);
      axiosCliente
        .post(
          `/direccion/agregar/${email}`,
          {
            calle: calle,
            numero: numero,
            esquina: esquina,
            barrio: barrio,
          },
          {
            headers: { Authorization: `${token}` },
          }
        )
        .then(function (response) {
          //console.log(response);
          setNuevosDatos(!nuevosDatos);

          setOpenModal(false);
          setSeverity("success");
          setSnackbar(true);
          setMjeError(`La dirección se agregó correctamente`);
          setLoader(false);
        })
        .catch(function (error) {
          console.log(error);
          setOpenModal(false);
          setSeverity("error");
          setSnackbar(true);
          setMjeError(`Ocurrió un error en el sistema`);
          setLoader(false);
        });
    }
  };

  const handleLoader = () => {
    setLoader(!loader);
  };

  const handleCalle = (event) => {
    setCalle(event.target.value);
  };
  const handleNumero = (event) => {
    setNumero(event.target.value);
  };
  const handleEsquina = (event) => {
    setEsquina(event.target.value);
  };
  const handleBarrio = (event) => {
    setBarrio(event.target.value);
  };
  const handleNuevosDatos = () => {
    setNuevosDatos(!nuevosDatos);
  };
  const handleOpenModal = () => setOpenModal(true);
  const handleCloseSnackbar = () => setSnackbar(false);
  const handleCloseModal = () => setOpenModal(false);

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: colorCodes.black,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <>
      {esPagina ? <NavBar /> : null}
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Direcciones guardadas</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <ListDirecciones
                handleLoader={handleLoader}
                loader={loader}
                data={dataDirecciones}
                handleNuevosDatos={handleNuevosDatos}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2, mt: 2 }}
                onClick={handleOpenModal}
              >
                <AddIcon />
                Agregar direcci&oacute;n
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>

      <Modal
        open={openModal}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Agregar Direcci&oacute;n
          </Typography>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input id="calle" label="Calle" onChange={handleCalle} />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="numero"
                label="N&uacute;mero"
                onChange={handleNumero}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input id="esquina" label="Esquina" onChange={handleEsquina} />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input id="barrio" label="Barrio" onChange={handleBarrio} />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="error"
                sx={{ mb: 2, mr: 2 }}
                onClick={handleCloseModal}
              >
                <CloseIcon />
                Cancelar
              </Button>
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={handleSubmit(onSubmit)}
              >
                <AddIcon />
                Agregar
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>

      <Snackbar
        open={snackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default DireccionesCliente;
