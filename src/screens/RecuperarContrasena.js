import React, { useState } from "react";
import { Grid, Box, Button, Alert, Snackbar, Container } from "@mui/material";
import LogoLetras from "../components/logo/LogoLetras";
import colorCodes from "../assets/colors/Colors";
import Input from "./../components/basicos/Input";
import axiosUsuario from "../components/axios/AxiosUsuario";
import { Link } from "react-router-dom";
import rutas from "./../assets/const/rutas";

const RecuperarContrasena = () => {
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState();
  const [severity, setSeverity] = useState();
  const [email, setEmail] = useState("");

  const [emailEnviado, setEmailEnviado] = useState(false);

  const handleClick = () => {
    if (email !== "") {
      let token = "";
      axiosUsuario
        .put(
          `recuperarClave/${email}`,
          {},
          {
            headers: {
              Authorization: { token },
            },
          }
        )
        .then(function (response) {
          //console.log(response);
          if (response.status === 200) {
            setSeverity("success");
            setMjeError(
              "Se ha enviado a su casilla de correo la nueva contraseña"
            );
            setEmailEnviado(true);
          } else if (response.status === 204) {
            setSeverity("error");
            setMjeError("El mail ingresado no existe en el sistema");
          }
        })
        .catch(function (error) {
          setSeverity("error");
          setMjeError("Ocurrio un error en el sistema");
        });
    } else {
      setSeverity("error");
      setMjeError("Debe ingresar un correo");
    }

    setOpenSnackbar(true);
  };

  const handleClose = (event) => {
    setOpenSnackbar(false);
  };

  const handleEmail = (event) => {
    setEmail(event.target.value);
  };

  return (
    <>
      <Container maxWidth="sm">
        <LogoLetras withMargin={true} width="200" />
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Recuperar Contrase&ntilde;a</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ mr: 6, ml: 6 }}
            >
              <p>
                Ingresa tu direcci&oacute;n de e-mail para que te enviemos la
                nueva contrase&ntilde;a
              </p>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Input
                id="email"
                label="Ingresa tu e-mail"
                onChange={handleEmail}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              {emailEnviado ? (
                <Link to={rutas.raiz}>
                  <Button
                    variant="contained"
                    color="error"
                    sx={{ mb: 2, mr: 2 }}
                  >
                    Volver al inicio
                  </Button>
                </Link>
              ) : (
                <Button
                  variant="contained"
                  color="error"
                  sx={{ mb: 2 }}
                  onClick={handleClick}
                >
                  Continuar
                </Button>
              )}
            </Grid>
          </Grid>
        </Box>
      </Container>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default RecuperarContrasena;
