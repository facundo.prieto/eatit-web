import * as React from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActions } from "@mui/material";
import defaultImage from "../../assets/images/defaultImage.png";

const CardBasic = (props) => {
  const {
    data,
    esNull = false,
    esCreacion = false,
    esVista = false,
    handleOpenModalMenu,
    handleBorrarMenu,
    handleAgregarPedido,
  } = props;

  return (
    <Card sx={{ display: "flex", ml: 2, mr: 2 }}>
      {esNull ? (
        <>
          <CardMedia
            component="img"
            sx={{ width: 120, p: 2, height: 120 }}
            image={defaultImage}
            alt="Imagen no disponible"
          />
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <CardActions sx={{ my: "auto" }}>
              <Button
                size="small"
                color="success"
                variant="outlined"
                onClick={handleOpenModalMenu}
              >
                Cargar Menú
              </Button>
            </CardActions>
          </Box>
        </>
      ) : (
        <>
          <CardMedia
            component="img"
            sx={{ width: 151, p: 2, height: 151 }}
            image={data.imagen ? data.imagen : data.image}
            alt="Imagen no disponible"
          />
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <CardContent sx={{ flex: "1 0 auto" }}>
              <Typography component="div" variant="subtitle1">
                {data.titulo}
              </Typography>
              <Typography
                variant="subtitle2"
                color="text.secondary"
                component="div"
              >
                {data.descripcion}
              </Typography>
            </CardContent>
            <Box sx={{ display: "flex", alignItems: "center", pl: 2 }}>
              <Typography variant="subtitle1" component="div">
                <b>$ {data.precio}</b>
              </Typography>
            </Box>
            {esCreacion ? (
              <CardActions align="center">
                <Button size="small" color="error" onClick={handleBorrarMenu}>
                  Borrar
                </Button>
              </CardActions>
            ) : (
              <>
                {!esVista ? (
                  <CardActions sx={{ mb: 4 }}>
                    <Button size="small" color="success"></Button>
                  </CardActions>
                ) : (
                  <CardActions>
                    <Button
                      size="small"
                      color="success"
                      onClick={handleAgregarPedido}
                    >
                      Agregar al pedido
                    </Button>
                  </CardActions>
                )}
              </>
            )}
          </Box>
        </>
      )}
    </Card>
  );
};

export default CardBasic;
