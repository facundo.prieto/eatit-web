import React, { useState } from "react";
import {
  Container,
  Grid,
  Button,
  TextareaAutosize,
  Box,
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
  tabScrollButtonClasses,
} from "@mui/material";
import { getSession } from "./auth/auth";
import rutas from "../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "../assets/colors/Colors";
import axiosReclamo from "./axios/AxiosReclamo";

const RealizarReclamo = (props) => {
  const {
    idPedido,
    setLoader,
    setMjeError,
    handleSnackBar,
    setSeverity,
    setOpenModalMenu,
  } = props;
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [descripcion, setDescripcion] = React.useState("");
  const [motivo, setMotivo] = React.useState("");

  const handleDescripcion = (event) => {
    setDescripcion(event.target.value);
  };

  const handelRadioChange = (event, value) => {
    setMotivo(value);
  };

  const hacerReclamo = () => {
    setLoader(tabScrollButtonClasses);
    let datosEnviar = {
      descripcion: "[" + motivo + "] " + descripcion,
      email: `${email}`,
      pedido: idPedido,
    };
    axiosReclamo
      .post(`/crear/`, datosEnviar, {
        headers: { Authorization: `${token}` },
      })
      .then(function (response) {
        console.log(response);
        handleSnackBar();
        setMjeError("El reclamo fue realizado con éxito");
        setSeverity("success");
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);
        handleSnackBar();
        setSeverity("error");
        setMjeError("Ocurrió un error en el sistema");
        setLoader(false);
      });

    setOpenModalMenu(false);
    setLoader(false);
  };

  const handleCerrar = () => {
    setOpenModalMenu(false);
  };
  return (
    <>
      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <h1>Reclamo al pedido #{idPedido}</h1>
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="left"
              alignItems="left"
              sx={{ ml: 4 }}
            >
              <h4>Indica el motivo del reclamo</h4>
            </Grid>
            <Grid
              item
              xs={12}
              display="column"
              justifyContent="left"
              alignItems="left"
              sx={{ ml: 4 }}
            >
              <FormControl component="fieldset">
                <RadioGroup
                  defaultValue="Problema con el restaurante"
                  name="radio-buttons-group"
                  onChange={handelRadioChange}
                  value={motivo}
                >
                  <FormControlLabel
                    value="Problema con el restaurante"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="Problema con el restaurante"
                  />
                  <FormControlLabel
                    value="Inconformidad con la comida"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="Inconformidad con la comida"
                  />
                  <FormControlLabel
                    value="Dudas sobre el envío"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="Dudas sobre el envío"
                  />
                  <FormControlLabel
                    value="Reclamar devolución por un pedido cancelado"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="Reclamar devolución por un pedido cancelado"
                  />
                  <FormControlLabel
                    value="Otros"
                    control={
                      <Radio
                        sx={{
                          color: colorCodes.red,
                          "&.Mui-checked": {
                            color: colorCodes.red,
                          },
                        }}
                      />
                    }
                    label="Otros"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={12}
              display="column"
              justifyContent="center"
              alignItems="center"
              sx={{ ml: 4, mr: 4, mb: 4 }}
            >
              <h4>Contanos el problema:</h4>
              <TextareaAutosize
                minRows={12}
                placeholder="Descripción..."
                style={{ width: "100%", borderColor: colorCodes.red }}
                onChange={handleDescripcion}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="contained"
                color="error"
                sx={{ mb: 2, mr: 2 }}
                onClick={() => handleCerrar()}
              >
                Cancelar
              </Button>
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 2 }}
                onClick={() => hacerReclamo()}
              >
                Enviar reclamo
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default RealizarReclamo;
