import React, { useState, useEffect } from "react";
import NavBar from "./../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
  Grid,
  Box,
  Card,
  CardActions,
  CardContent,
  Typography,
  Rating,
} from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import AxiosAdmin from "../components/axios/AxiosAdmin";
import { AreaChart, Pie, PieChart, Cell, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Bar, BarChart } from 'recharts';


const VisualizarEstadistica = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMjeError] = useState("");
  const [severity, setSeverity] = useState();
  const [abierto, setAbierto] = useState();
  const [loader, setLoader] = useState(false);
  const [dataPedidosCliente, setDataPedidosCliente] = useState([]);
  const [dataRestoRechazados, setDataRestoRechazados] = useState([]);
  const [dataRestoEntregados, setDataRestoEntregados] = useState([]);
  const [showPedidosCliente, setShowPedidosCliente] = useState("none");
  const [showRestoRechazados, setShowRestoRechazados] = useState("none");
  const [showRestoEntregados, setShowRestoEntregados] = useState("none");
  const [nombreClientePedidos, setNombreClientePedidos] = useState("-")
  const [cantClientePedidos, setCantClientePedidos] = useState("-")
  const [nombreRestoRechazados, setNombreRestoRechazados] = useState("-")
  const [cantRestoRechazados, setCantRestoRechazados] = useState("-")
  const [nombreRestoEntregados, setNombreRestoEntregados] = useState("-")
  const [cantRestoEntregados, setCantRestoEntregados] = useState("-")


  const estadisticasPedidosRealizadosCliente = () => {
    AxiosAdmin.get(
      `/estadisticas/cliente/pedidos`,
      { headers: { Authorization: `${token}` } }
    ).then(function (response) {
      setDataPedidosCliente(response.data);
      setShowPedidosCliente("block");
      setLoader(false);

    }).catch(function (error) {
      //console.log(error);

      setSeverity("error");
      setSnackbar(true);
      setMjeError(`Ocurrió un error en el sistema`);
      setLoader(false);
    });
  }

  const estadisticasPedidosRechazadosRestos = () => {
    AxiosAdmin.get(
      `/estadisticas/restaurante/pedidos/rechazados`,
      { headers: { Authorization: `${token}` } }
    ).then(function (response) {
      setDataRestoRechazados(response.data);
      setShowRestoRechazados("block");
      setLoader(false);

    }).catch(function (error) {
      //console.log(error);

      setSeverity("error");
      setSnackbar(true);
      setMjeError(`Ocurrió un error en el sistema`);
      setLoader(false);
    });
  }

  const estadisticasPedidosEntregadosRestos = () => {
    AxiosAdmin.get(
      `/estadisticas/restaurante/pedidos/entregados`,
      { headers: { Authorization: `${token}` } }
    ).then(function (response) {
      setDataRestoEntregados(response.data);
      setShowRestoEntregados("block");
      setLoader(false);

    }).catch(function (error) {
      //console.log(error);

      setSeverity("error");
      setSnackbar(true);
      setMjeError(`Ocurrió un error en el sistema`);
      setLoader(false);
    });
  }

  const estadisticasGenerales = () => {
    AxiosAdmin.get(
      `/estadisticas`,
      { headers: { Authorization: `${token}` } }
    ).then(function (response) {
      let data = response.data;
      setNombreClientePedidos(data[0].nombre);
      setCantClientePedidos(data[0].cantidad);
      setNombreRestoRechazados(data[1].nombre);
      setCantRestoRechazados(data[1].cantidad);
      setNombreRestoEntregados(data[2].nombre);
      setCantRestoEntregados(data[2].cantidad);

      setLoader(false);

    }).catch(function (error) {
      //console.log(error);

      setSeverity("error");
      setSnackbar(true);
      setMjeError(`Ocurrió un error en el sistema`);
      setLoader(false);
    });
  }

  useEffect(() => {
    setLoader(true);
    estadisticasGenerales();
    estadisticasPedidosRealizadosCliente();
    estadisticasPedidosRechazadosRestos();
    estadisticasPedidosEntregadosRestos();
  }, []);


  const handleClose = (event, reason) => {
    setSnackbar(false);
  };
  return (
    <>
      <NavBar />
      <Container maxWidth="md"></Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
      <Container maxWidth="lg">
          <Box bgcolor={colorCodes} sx={{ mt: 6 }}>
          <Grid container spacing={{ xs: 2, md: 2 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Grid item xs={2} sm={4} md={4}>
              <Card sx={{ maxWidth: 400 }} style={{backgroundColor: colorCodes.red, color: 'white'}}>
                    <CardContent>
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Cliente con mas pedidos
                      </Typography>
                      <Typography variant="h4" component="div">
                        {nombreClientePedidos}
                      </Typography>
                      <Typography sx={{ fontSize: 16 }}>
                        {cantClientePedidos} pedidos
                      </Typography>
                    </CardContent>
              </Card>
              <Box 
                sx={{ maxWidth: 400, minHeight: 300 }} 
                display={showPedidosCliente}
                style={{backgroundColor: colorCodes.backBox, color: 'white'}}
              >
                <BarChart
                    width={350}
                    height={250}
                    data={dataPedidosCliente}
                    margin={{
                      top: 30,
                      right: 10,
                      left: 10,
                      bottom: 10,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="fecha" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="cantidad" fill="#33C1FF" />
                  </BarChart>
              </Box>
            </Grid>
            <Grid item xs={2} sm={4} md={4}>
              <Card sx={{ maxWidth: 400 }} style={{backgroundColor: colorCodes.red, color: 'white'}}>
                    <CardContent>
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Restaurante con mas pedidos rechazados
                      </Typography>
                      <Typography variant="h4" component="div">
                        {nombreRestoRechazados}
                      </Typography>
                      <Typography sx={{ fontSize: 16 }}>
                        {cantRestoRechazados} pedidos
                      </Typography>
                    </CardContent>
                </Card>
                <Box 
                  sx={{ maxWidth: 400, minHeight: 300 }} 
                  style={{backgroundColor: colorCodes.backBox, color: 'white'}}
                  display={showRestoRechazados}
                >
                <BarChart
                    width={350}
                    height={250}
                    data={dataRestoRechazados}
                    margin={{
                      top: 30,
                      right: 10,
                      left: 10,
                      bottom: 10,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="fecha" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="cantidad" fill="#33C1FF" />
                  </BarChart>
              </Box>
            </Grid>
            <Grid item xs={2} sm={4} md={4}>
              <Card sx={{ maxWidth: 400 }} style={{backgroundColor: colorCodes.red, color: 'white'}}>
                    <CardContent>
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Restaurante con mas pedidos entregados
                      </Typography>
                      <Typography variant="h4" component="div">
                        {nombreRestoEntregados}
                      </Typography>
                      <Typography sx={{ fontSize: 16 }}>
                        {cantRestoEntregados} pedidos
                      </Typography>
                    </CardContent>
                </Card>
                <Box 
                  sx={{ maxWidth: 400, minHeight: 300 }} 
                  style={{backgroundColor: colorCodes.backBox, color: 'white'}}
                  display={showRestoEntregados}
                >
                  <BarChart
                      width={350}
                      height={250}
                      data={dataRestoEntregados}
                      margin={{
                        top: 30,
                        right: 10,
                        left: 10,
                        bottom: 10,
                      }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="fecha" />
                      <YAxis />
                      <Tooltip />
                      <Legend />
                      <Bar dataKey="cantidad" fill="#33C1FF" />
                    </BarChart>
                  </Box>
            </Grid>
          </Grid>   
          </Box>
      </Container>        
    </>
  );
};

export default VisualizarEstadistica;
