import { TextField } from "@mui/material";
import React from "react";

const Input = (props) => {
  const {
    id,
    label,
    variant = "standard",
    type = "text",
    helperText = "",
    color = "white",
    value,
    inputProps,
    onChange,
    name
  } = props;

  return (
    <TextField
      sx={{
        mr: 6, // margin right
        ml: 6, // margin left
        borderBottom: "1px solid white", // borde blanco
        "& label.Mui-focused": {
          color: "white",
        },
        "& .MuiInput-underline:after": {
          borderBottomColor: "white",
        },
        "& .MuiOutlinedInput-root": {
          "& fieldset": {
            borderColor: "white",
          },
          "&:hover fieldset": {
            borderColor: "white",
          },
          "&.Mui-focused fieldset": {
            borderColor: "white",
          },
        },
      }}
      fullWidth // todo el ancho
      id={id}
      label={label}
      variant={variant}
      type={type}
      value={value}
      inputProps={inputProps}
      onChange={onChange}
      name={name}
    />
  );
};

export default Input;
