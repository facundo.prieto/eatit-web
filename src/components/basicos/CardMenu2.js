import * as React from "react";
import { useState } from "react";
import { IconButton, Box, Grid, ListItemSecondaryAction } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea, Modal } from "@mui/material";
import Typography from "@mui/material/Typography";
import defaultImage from "../../assets/images/defaultImage.png";
import ModalBasic from "./ModalBasic";
import AltaPromocion from "../AltaPromocion";
import DeleteIcon from "@mui/icons-material/Delete";
import CreateIcon from "@mui/icons-material/Create";
import AddIcon from "@mui/icons-material/Add";
import ModificarMenu from "../ModificarMenu";
import BajaMenu from "../BajaMenu";
import ModalConfirmacion from "./ModalConfirmacion";
import Tooltip from "@mui/material/Tooltip";

const CardMenu2 = (props) => {
  const {
    data,
    handleNuevosDatosMenu,
    handleNuevosDatosPromocion,
    setMje,
    handleSnackBar,
    setSeverity,
  } = props;

  const [menu, setMenu] = useState("");

  const [openModal, setOpenModal] = useState(false);
  const handleOpenModal = () => setOpenModal(!openModal);
  const handleCloseModal = () => setOpenModal(false);

  const [openModalUpdateMenu, setOpenModalUpdateMenu] = useState(false);
  const handleOpenModalUpdateMenu = () =>
    setOpenModalUpdateMenu(!openModalUpdateMenu);
  const handleCloseModalUpdateMenu = () => setOpenModalUpdateMenu(false);

  const [openModalDeleteMenu, setOpenModalDeleteMenu] = useState(false);
  const handleOpenModalDeleteMenu = () =>
    setOpenModalDeleteMenu(!openModalDeleteMenu);
  const handleCloseModalDeleteMenu = () => setOpenModalDeleteMenu(false);

  const handleModal = (value) => {
    setMenu(value);
    setOpenModal(true);
  };

  const handleModalUpdateMenu = (value) => {
    setMenu(value);
    setOpenModalUpdateMenu(true);
  };

  const handleModalDeleteMenu = (value) => {
    setMenu(value);
    setOpenModalDeleteMenu(true);
  };

  return (
    <>
      {data.map((value) => (
        <Card sx={{ m: 2 }}>
          <CardActionArea sx={{ display: "flex" }}>
            <CardMedia
              component="img"
              sx={{
                width: 100,
                m: 2,
                height: 100,
                border: "1px solid black",
                borderRadius: 2,
              }}
              image={value.imagen === "" ? defaultImage : value.imagen}
              alt="Imagen no disponible"
            />
            <Box
              sx={{ display: "flex", flexDirection: "column", width: "100%" }}
            >
              <CardContent sx={{ flex: "1 0 auto", mt: 2 }}>
                <Typography component="div" variant="h6">
                  {value.titulo}
                </Typography>
                <Grid item xs={11} display="flex" alignItems="right">
                  <Typography
                    variant="subtitle2"
                    color="text.secondary"
                    component="div"
                  >
                    {value.descripcion}
                  </Typography>
                </Grid>
              </CardContent>
            </Box>
            <Box
              sx={{ display: "flex", flexDirection: "column", width: "20%" }}
            >
              <CardContent sx={{ flex: "1 0 auto", mt: 2 }} align="right">
                <ListItemSecondaryAction>
                  <Typography component="div" variant="h6" color="red">
                    $ {value.precio}
                  </Typography>
                  <Tooltip title="Agregar promoción">
                    <IconButton variant="contained">
                      <AddIcon
                        onClick={() => handleModal(value)}
                        justify="center"
                        alignItems="center"
                      />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Modificar Menú">
                    <IconButton
                      variant="contained"
                      onClick={() => handleModalUpdateMenu(value)}
                    >
                      <CreateIcon
                        onClick={props.onClick}
                        justify="center"
                        alignItems="center"
                      />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Eliminar Menú">
                    <IconButton
                      variant="contained"
                      onClick={() => handleModalDeleteMenu(value)}
                    >
                      <DeleteIcon
                        onClick={props.onClick}
                        justify="center"
                        alignItems="center"
                      />
                    </IconButton>
                  </Tooltip>
                </ListItemSecondaryAction>
              </CardContent>
            </Box>
          </CardActionArea>
        </Card>
      ))}

      <ModalBasic
        open={openModal}
        datos={menu}
        onClose={handleCloseModal}
        content={
          <AltaPromocion
            datos={menu}
            isNavbar={true}
            handleOpenModal={handleOpenModal}
            handleCloseModal={handleCloseModal}
            handleNuevosDatosMenu={handleNuevosDatosMenu}
            handleNuevosDatosPromocion={handleNuevosDatosPromocion}
            setMje={setMje}
            handleSnackBar={handleSnackBar}
            setSeverity={setSeverity}
          />
        }
      />
      <ModalBasic
        open={openModalUpdateMenu}
        datos={menu}
        onClose={handleCloseModalUpdateMenu}
        content={
          <ModificarMenu
            datos={menu}
            handleNuevosDatosMenu={handleNuevosDatosMenu}
            handleOpenModalUpdateMenu={handleOpenModalUpdateMenu}
            setMje={setMje}
            handleSnackBar={handleSnackBar}
            setSeverity={setSeverity}
          />
        }
      />

      <ModalConfirmacion
        open={openModalDeleteMenu}
        onClose={handleCloseModalDeleteMenu}
        content={
          <BajaMenu
            datos={menu}
            setOpenModalDeleteMenu={setOpenModalDeleteMenu}
            handleOpenModalDeleteMenu={handleOpenModalDeleteMenu}
            handleNuevosDatosMenu={handleNuevosDatosMenu}
            setMje={setMje}
            handleSnackBar={handleSnackBar}
            setSeverity={setSeverity}
          />
        }
      />
    </>
  );
};

export default CardMenu2;
