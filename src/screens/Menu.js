import React, { useState } from "react";
import {
  Container,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  Alert,
  Snackbar,
} from "@mui/material";
import Input from "../components/basicos/Input";
import colorCodes from "../assets/colors/Colors";
import { getSession } from "./../components/auth/auth";
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket";
const Menu = (props) => {
  const { menu, setOpenModal, agregarAlCarrito } = props;
  console.log(menu);
  const [state, setState] = useState({
    id: menu.id,
    titulo: menu.titulo,
    precio: menu.promocion !== null ? menu.desc : menu.precio,
    descripcion: menu.descripcion,
    imagen: menu.imagen,
    categoria: menu.categoria,
    cantidad: 1,
  });

  const handleChangeState = (event) => {
    setState((prevState) => ({
      ...prevState,
      [event.target.id]: event.target.value,
    }));
  };

  const [open, setOpen] = useState(false);
  const [ok, setOk] = useState(true);
  const [error, setErrorMsg] = useState("");

  let token = "";
  if (getSession() != null && getSession().token != null) {
    token = getSession().token;
  }

  let id = "";
  if (getSession() != null && getSession().id != null) {
    id = getSession().id;
  }

  const handleClickAgregar = () => {
    agregarAlCarrito(state);
    setOpenModal(false);
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Container maxWidth="sm" style={{ padding: "24px" }}>
      <Card
        sx={{ maxWidth: "sm" }}
        style={{ backgroundColor: colorCodes.backBox }}
      >
        <CardMedia component="img" height="140" image={state.imagen} />
        <CardContent style={{ display: "flex", flexDirection: "row" }}>
          <CardContent sx={{ width: "80%" }}>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              style={{ color: "white" }}
            >
              {state.titulo}
            </Typography>
            <Typography variant="body2" style={{ color: "whitesmoke" }}>
              {state.descripcion}
            </Typography>
          </CardContent>
          <CardContent sx={{ width: "20%" }}>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              style={{ color: "red" }}
            >
              $ {state.precio}
            </Typography>
          </CardContent>
        </CardContent>

        <CardActions>
          <Input
            sx={{ mr: 6, ml: 6, mb: 2 }}
            fullWidth
            id="cantidad"
            label="Cantidad"
            type="number"
            inputProps={{ min: 1 }}
            value={state.cantidad}
            onChange={handleChangeState}
            variant="standard"
          />
        </CardActions>
        <CardContent
          style={{ display: "flex", flexDirection: "row", padding: "0" }}
        >
          <CardContent sx={{ width: "50%" }}>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              style={{ color: "white", textAlign: "right" }}
            >
              SubTotal:
            </Typography>
          </CardContent>
          <CardContent sx={{ width: "50%" }}>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              style={{ color: "red", textAlign: "left" }}
            >
              $ {state.precio * state.cantidad}
            </Typography>
          </CardContent>
        </CardContent>

        <CardActions style={{ justifyContent: "center" }}>
          <Button
            sx={{ mr: 4, ml: 4, mb: 2 }}
            variant="contained"
            color="error"
            onClick={handleClickAgregar}
          >
            Agregar y seguir comprando <ShoppingBasketIcon sx={{ ml: 2 }} />
          </Button>
        </CardActions>
      </Card>

      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert
          onClose={handleClose}
          severity={ok ? "success" : "error"}
          sx={{ width: "100%" }}
        >
          {ok ? "Agregado al pedido!" : error}
        </Alert>
      </Snackbar>
    </Container>
  );
};

export default Menu;
