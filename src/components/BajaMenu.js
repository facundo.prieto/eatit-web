import React, { useState, useEffect } from "react";
import { getSession } from "../components/auth/auth";
import rutas from "./../assets/const/rutas";
import { useHistory } from "react-router-dom";
import colorCodes from "./../assets/colors/Colors";
import Typography from "@mui/material/Typography";
import AddIcon from "@mui/icons-material/Add";
import CloseIcon from "@mui/icons-material/Close";
import { Container, Grid, Button, Box } from "@mui/material";
import axiosRestaurante from "../components/axios/AxiosRestaurante";
import CheckIcon from "@mui/icons-material/Check";

const BajaMenu = (props) => {
  const {
    setOpenModalDeleteMenu,
    datos,
    handleOpenModalDeleteMenu,
    handleNuevosDatosMenu,
    setMje,
    setSeverity,
  } = props;

  let history = useHistory();

  const [token, setToken] = useState(
    getSession() ? getSession().token : history.push(rutas.iniciar_sesion)
  );

  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );

  const handleClose = () => {
    setOpenModalDeleteMenu(false);
  };

  const handleClick = () => {
    axiosRestaurante
      .delete(`/${email}/menu/${datos.id}`, {
        headers: {
          Authorization: `${token}`,
        },
      })
      .then(function (response) {
        if (response.status === 200) {
          setMje("La promoción se eliminó correctamente");
          handleOpenModalDeleteMenu();
          handleNuevosDatosMenu();

          setSeverity("success");
        } else {
          setMje(response.data);
          setSeverity("error");
          setMje("Ocurrio un error en el sistema");
        }
      })
      .catch(function (error) {
        //setSeverity("error");
        //setMje("Error en el servidor");
      });
  };

  return (
    <>
      <Container maxWidth="md">
        <Box sx={{ mb: 4 }}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            align="center"
          >
            Eliminar Men&uacute;
          </Typography>
        </Box>

        <Grid
          display="flex"
          justifyContent="center"
          alignItems="center"
          vertical-align="bottom"
        >
          <Button
            size="small"
            variant="contained"
            color="error"
            sx={{ mb: -1, mr: 2 }}
            onClick={handleClose}
          >
            <CloseIcon />
            Cancelar
          </Button>
          <Button
            size="small"
            variant="contained"
            color="success"
            sx={{ mb: -1 }}
            onClick={handleClick}
          >
            <CheckIcon />
            Confirmar
          </Button>
        </Grid>
      </Container>
    </>
  );
};

export default BajaMenu;
