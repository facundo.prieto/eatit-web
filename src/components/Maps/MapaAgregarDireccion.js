import React, { useEffect } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import View from "ol/View";
import { Tile as TileLayer } from "ol/layer";
import { Draw, Modify, Snap } from "ol/interaction";
import OSM from "ol/source/OSM";
import VectorSource from "ol/source/Vector";
import { Vector as VectorLayer } from "ol/layer";
import { Style } from "ol/style";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import { Circle as CircleStyle } from "ol/style";
import colorCodes from "../../assets/colors/Colors";

const MapaAgregarDireccion = (props) => {
  const {
    posicion = [-6251681.1746235965, -4148062.303116895],
    handleCoordenadas,
  } = props;

  useEffect(() => {
    //mapa
    const mapa = new TileLayer({
      source: new OSM(),
    });

    var source = new VectorSource();
    const vector = new VectorLayer({
      source: source,
      style: new Style({
        fill: new Fill({
          color: colorCodes.red,
        }),
        stroke: new Stroke({
          color: colorCodes.red,
          width: 2,
        }),
        image: new CircleStyle({
          radius: 7,
          fill: new Fill({
            color: colorCodes.red,
          }),
        }),
      }),
    });
    const map = new Map({
      layers: [mapa, vector],
      target: "map",
      view: new View({
        center: posicion,
        zoom: 11,
      }),
    });
    var draw, snap; // global so we can remove them later
    var typeSelect = "Point";

    function addInteractions() {
      draw = new Draw({
        source: source,
        type: typeSelect,
      });
      map.addInteraction(draw);
      snap = new Snap({ source: source });
      map.addInteraction(snap);

      draw.on("drawend", function (e2) {
        var feature = e2.feature;
        var geometry = feature.getGeometry();

        var coords = geometry.getCoordinates();
        handleCoordenadas(coords[0], coords[1]);
      });
      handleCoordenadas();
    }

    addInteractions();
  }, []);

  return <div className="map" id="map"></div>;
};

export default MapaAgregarDireccion;
