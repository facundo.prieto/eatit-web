import React, { useState, useEffect } from "react";
import NavBar from "../components/NavBar/NavBar";
import {
  Container,
  Backdrop,
  CircularProgress,
  Snackbar,
  Alert,
  Grid,
  Box,
  Tooltip,
  IconButton,
} from "@mui/material";
import { getSession } from "../components/auth/auth";
import rutas from "../assets/const/rutas";
import { useHistory } from "react-router-dom";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import AxiosReclamo from "../components/axios/AxiosReclamo";
import colorCodes from "../assets/colors/Colors";
import ListaReclamosRestaurante from "../components/ListaReclamosRestaurante";
import Input from "./../components/basicos/Input";
import SearchIcon from "@mui/icons-material/Search";
import SyncIcon from "@mui/icons-material/Sync";

const ListarHistoricoReclamos = () => {
  let history = useHistory();
  const [email, setEmail] = useState(
    getSession() ? getSession().email : history.push(rutas.iniciar_sesion)
  );
  const [snackbar, setSnackbar] = useState(false);
  const [mjeError, setMje] = useState("");
  const [severity, setSeverity] = useState();
  const [loader, setLoader] = useState(false);
  const [reclamos, setReclamos] = useState([]);
  const [state, setState] = useState({
    idRestaurante: "",
    nombreRestaurante: "",
  });

  const [nuevosDatos, setNuevosDatos] = useState(true);
  const handleNuevosDatos = () => {
    setNuevosDatos(!nuevosDatos);
  };

  const handleClose = () => {
    setSnackbar(false);
  };
  const [reclamosAbiertos, setReclamosAbiertos] = useState([]);
  const [reclamosResueltos, setReclamosResueltos] = useState([]);
  const [reclamosNoResueltos, setReclamosNoResueltos] = useState([]);

  useEffect(() => {
    setLoader(true);
    AxiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      let id = response.data.idRestaurante;
      setState({
        idRestaurante: response.data.idRestaurante,
        nombreRestaurante: response.data.nombre,
      });

      AxiosReclamo.get(`/listar/${email}`)
        .then(function (response) {
          if (response.status == 200) {
            let reclamosAbiertosAux = reclamos.slice();
            let reclamosResueltosAux = reclamos.slice();
            let reclamosNoResueltosAux = reclamos.slice();

            response.data
              .filter((f) => {
                if (f.estadoReclamo === "ABIERTO") {
                  return f;
                } else if (f.estadoReclamo === "RESUELTO") {
                  return f;
                } else if (f.estadoReclamo === "NO_RESUELTO") {
                  return f;
                } else {
                  return null;
                }
              })
              .map((m) => {
                if (m.estadoReclamo === "ABIERTO") {
                  reclamosAbiertosAux.push(m);
                  setReclamosAbiertos(reclamosAbiertosAux);
                  setReclamos(reclamosAbiertosAux);
                } else if (m.estadoReclamo === "RESUELTO") {
                  reclamosResueltosAux.push(m);
                  setReclamosResueltos(reclamosResueltosAux);
                } else if (m.estadoReclamo === "NO_RESUELTO") {
                  reclamosNoResueltosAux.push(m);
                  setReclamosNoResueltos(reclamosNoResueltosAux);
                }
              });
            setSeverity("success");
            setReclamos(response.data);
            setLoader(false);
          } else {
            setSeverity("error");
            setReclamos(null);
            setMje(`No hay reclamos`);
            setLoader(false);
          }
        })
        .catch(function (error) {
          console.log(error);
          setSeverity("error");
          setSnackbar(true);
          setMje(`Ocurrió un error en el sistema`);
          setLoader(false);
        });
      setLoader(false);
    });
  }, [email, nuevosDatos]);

  //
  const handleRefrescar = () => {
    AxiosReclamo.get(`/listar/${email}`)
      .then(function (response) {
        console.log(response.data);
        if (response.status == 200) {
          let reclamosAbiertosAux = reclamos.slice();
          let reclamosResueltosAux = reclamos.slice();
          let reclamosNoResueltosAux = reclamos.slice();

          response.data
            .filter((f) => {
              if (f.estadoReclamo === "ABIERTO") {
                return f;
              } else if (f.estadoReclamo === "RESUELTO") {
                return f;
              } else if (f.estadoReclamo === "NO_RESUELTO") {
                return f;
              } else {
                return null;
              }
            })
            .map((m) => {
              if (m.estadoReclamo === "ABIERTO") {
                reclamosAbiertosAux.push(m);
                setReclamosAbiertos(reclamosAbiertosAux);
                setReclamos(reclamosAbiertosAux);
              } else if (m.estadoReclamo === "RESUELTO") {
                reclamosResueltosAux.push(m);
                setReclamosResueltos(reclamosResueltosAux);
              } else if (m.estadoReclamo === "NO_RESUELTO") {
                reclamosNoResueltosAux.push(m);
                setReclamosNoResueltos(reclamosNoResueltosAux);
              }
            });
          setSeverity("success");
          setReclamos(response.data);
          setLoader(false);
        } else {
          setSeverity("error");
          setReclamos(null);
          setMje(`No hay reclamos`);
          setLoader(false);
        }
      })
      .catch(function (error) {
        console.log(error);
        setSeverity("error");
        setSnackbar(true);
        setMje(`Ocurrió un error en el sistema`);
        setLoader(false);
      });
    setLoader(false);
  };

  //BARRA DE BUSQUEDA
  const [searchTerm, setSearchTerm] = useState("");
  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  let reclamosFiltrados = reclamos.filter(
    (item) =>
      item.numeroReclamo.toString().toLowerCase().includes(searchTerm) ||
      item.numeroReclamo.toString().includes(searchTerm) ||
      item.nombreCliente.toLowerCase().includes(searchTerm) ||
      item.nombreCliente.includes(searchTerm)
  );

  return (
    <>
      <NavBar />

      <Container maxWidth="md">
        <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              display="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ pl: 2, pr: 2 }}
              >
                <h1 align="center">Historico de Reclamos</h1>
                <Grid
                  item
                  xs={1}
                  display="flex"
                  justifyContent="right"
                  alignItems="center"
                >
                  <IconButton onClick={handleRefrescar} sx={{ color: "white" }}>
                    <SyncIcon />
                  </IconButton>
                </Grid>
              </Grid>
              <Grid item xs={12} display="flex">
                <Grid
                  item
                  xs={11}
                  display="flex"
                  justifyContent="left"
                  alignItems="left"
                >
                  <Input
                    fullWidth
                    id="buscar"
                    label="Buscar reclamo"
                    value={searchTerm}
                    onChange={handleSearch}
                  />
                </Grid>
                <Grid
                  item
                  xs={1}
                  display="flex"
                  justifyContent="left"
                  alignItems="center"
                >
                  <Tooltip title="Busqueda por #Reclamo o Cliente">
                    <SearchIcon />
                  </Tooltip>
                </Grid>
              </Grid>
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
              >
                <ListaReclamosRestaurante
                  data={reclamosFiltrados}
                  nuevosDatos={nuevosDatos}
                  handleNuevosDatos={handleNuevosDatos}
                  setSearchTerm={setSearchTerm}
                />
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <Backdrop
        sx={{
          color: colorCodes.red,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={loader}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar open={snackbar} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: "100%" }}>
          {mjeError}
        </Alert>
      </Snackbar>
    </>
  );
};

export default ListarHistoricoReclamos;
