import React, { useState, useEffect } from "react";
import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  Box,
  Card,
  CardContent,
  CardMedia,
  Typography,
  MenuItem,
  Button,
  Chip,
  Link,
} from "@mui/material";

import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import BackspaceIcon from "@mui/icons-material/Backspace";
import moment from "moment";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";

const ListaPedidos = ({ data }) => {
  let history = useHistory();
  const [desde, setDesde] = useState(null);
  const [hasta, setHasta] = useState(null);
  const [selectedRestaurante, setSelectedRestaurante] = useState("");
  const [selectedTipoPago, setSelectedTipoPago] = useState("TODOS");
  const [restaurantesUsados, setRestaurantesUsados] = useState([]);

  useEffect(() => {
    // uso esto para quedarme con los restaurantes que uso el cliente sin repetidos
    let restaurante = [];
    data.map((value, key) => {
      restaurante.push(value.restauranteNombre);
    });

    let uniqueRestaurante = [...new Set(restaurante)];

    setRestaurantesUsados(uniqueRestaurante);
    //////////////////////////////////////////////////////////////////////////////
  }, [data]);

  const calcularCantidad = (menus) => {
    let array = [];
    let grouped = [];
    let salida = "";
    menus.map((val, k) => {
      array.push(val.titulo);
    });

    grouped = array.reduce((r, v, i, a) => {
      if (v === a[i - 1]) {
        r[r.length - 1].push(v);
      } else {
        r.push(v === a[i + 1] ? [v] : v);
      }
      return r;
    }, []);

    grouped.map((val, key) => {
      Array.isArray(val)
        ? (salida += val.length + " x " + val[0] + " ")
        : (salida += "1 x " + val + " ");
    });

    //console.log(salida);
    return salida;
  };

  const handleChangeSelectRestaurante = (event) => {
    console.log(event.target.value);
    setSelectedRestaurante(event.target.value);
  };

  const handleChangeSelectTipoPago = (event) => {
    setSelectedTipoPago(event.target.value);
  };

  const borrarFiltros = () => {
    setDesde(null);
    setHasta(null);
    setSelectedRestaurante("");
    setSelectedTipoPago("TODOS");
  };

  function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : "0" + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : "0" + day;

    return day + "/" + month + "/" + year;
  }

  const handleIrAlPedido = (id) => {
    localStorage.setItem("__detalle_pedido", id);
    history.push(rutas.detalle_pedido);
  };
  return (
    <>
      <Grid
        item
        xs={12}
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Grid
          item
          xs={6}
          display="column"
          justifyContent="center"
          alignItems="center"
        >
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              inputFormat="dd/MM/yyyy"
              value={desde}
              onChange={(newValue) => {
                setDesde(newValue);
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
        </Grid>
        <Grid
          item
          xs={6}
          display="column"
          justifyContent="center"
          alignItems="center"
        >
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              inputFormat="dd/MM/yyyy"
              value={hasta}
              onChange={(newValue) => {
                setHasta(newValue);
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
        </Grid>
      </Grid>
      <Grid
        item
        xs={12}
        display="flex"
        justifyContent="center"
        alignItems="center"
        sx={{ mt: 2 }}
      >
        <Grid
          item
          xs={5}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ pr: 3 }}
        >
          <FormControl fullWidth error>
            <InputLabel id="demo-simple-select-label">Restaurante</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectedRestaurante}
              label="Restaurante"
              onChange={handleChangeSelectRestaurante}
              color="error"
            >
              {restaurantesUsados.map((option, key) => {
                return (
                  <MenuItem key={key} value={option}>
                    {option}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid
          item
          xs={5}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ pr: 3 }}
        >
          <FormControl fullWidth error>
            <InputLabel id="demo-simple-select-label">Tipo de Pago</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectedTipoPago}
              label="Tipo de Pago"
              onChange={handleChangeSelectTipoPago}
              color="error"
            >
              <MenuItem key={1} value="PAYPAL">
                PayPal
              </MenuItem>
              <MenuItem key={2} value="EFECTIVO">
                Efectivo
              </MenuItem>
              <MenuItem key={3} value="TODOS">
                Todos
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid
          item
          xs={2}
          display="column"
          justifyContent="left"
          alignItems="left"
          sx={{ pr: 3 }}
        >
          <Button variant="contained" color="error" onClick={borrarFiltros}>
            <BackspaceIcon sx={{ ml: 1 }} />
          </Button>
        </Grid>
      </Grid>
      {data
        .filter((val) => {
          let check = "";
          let from = "";
          let to = "";

          if (
            selectedRestaurante === "" &&
            selectedTipoPago === "TODOS" &&
            desde === null &&
            hasta === null
          ) {
            console.log(val.fecha);
            console.log(val);
            return val;
          } else if (
            selectedRestaurante === "" &&
            selectedTipoPago === "TODOS" &&
            desde !== null &&
            hasta !== null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            from = moment(getFormattedDate(desde), "DD/MM/YYYY");
            to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

            console.log(val.fecha);
            console.log(
              moment(val.fecha, "DD/MM/YYYY").isBetween(
                moment(getFormattedDate(desde), "DD/MM/YYYY"),
                moment(getFormattedDate(hasta), "DD/MM/YYYY"),
                undefined,
                []
              )
            );

            return val ? check.isBetween(from, to, undefined, []) : "";
          } else if (
            selectedRestaurante === "" &&
            selectedTipoPago === "TODOS" &&
            desde !== null &&
            hasta === null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            from = moment(getFormattedDate(desde), "DD/MM/YYYY");

            console.log(val ? check.isSameOrAfter(from, undefined) : "");

            return val ? check.isSameOrAfter(from, undefined) : "";
          } else if (
            selectedRestaurante === "" &&
            selectedTipoPago === "TODOS" &&
            desde === null &&
            hasta !== null
          ) {
            console.log("filtro porfecha");

            check = moment(val.fecha, "DD/MM/YYYY");
            to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

            console.log(val ? check.isSameOrBefore(to, undefined) : "");

            return val ? check.isSameOrBefore(to, undefined) : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago === "TODOS" &&
            desde === null &&
            hasta === null
          ) {
            return val ? val.restauranteNombre === selectedRestaurante : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago === "TODOS" &&
            desde !== null &&
            hasta !== null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            from = moment(getFormattedDate(desde), "DD/MM/YYYY");
            to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  check.isBetween(from, to, undefined, [])
              : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago === "TODOS" &&
            desde !== null &&
            hasta === null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            from = moment(getFormattedDate(desde), "DD/MM/YYYY");

            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  check.isSameOrAfter(from, undefined)
              : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago === "TODOS" &&
            desde === null &&
            hasta !== null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  check.isSameOrBefore(to, undefined)
              : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago !== "TODOS" &&
            desde === null &&
            hasta === null
          ) {
            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  val.metodoPago === selectedTipoPago
              : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago !== "TODOS" &&
            desde !== null &&
            hasta !== null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            from = moment(getFormattedDate(desde), "DD/MM/YYYY");
            to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  val.metodoPago === selectedTipoPago &&
                  check.isBetween(from, to, undefined, [])
              : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago !== "TODOS" &&
            desde !== null &&
            hasta === null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            from = moment(getFormattedDate(desde), "DD/MM/YYYY");

            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  val.metodoPago === selectedTipoPago &&
                  check.isSameOrAfter(from, undefined)
              : "";
          } else if (
            selectedRestaurante !== "" &&
            selectedTipoPago !== "TODOS" &&
            desde === null &&
            hasta !== null
          ) {
            check = moment(val.fecha, "DD/MM/YYYY");
            to = moment(getFormattedDate(hasta), "DD/MM/YYYY");

            return val
              ? val.restauranteNombre === selectedRestaurante &&
                  val.metodoPago === selectedTipoPago &&
                  check.isSameOrBefore(to, undefined)
              : "";
          } else if (
            selectedRestaurante === "" &&
            selectedTipoPago !== "TODOS"
          ) {
            return val ? val.metodoPago === selectedTipoPago : "";
          }
        })
        .map((value, key) => (
          <Card sx={{ display: "flex", mt: 2, mr: 3 }}>
            <CardMedia
              component="img"
              sx={{ width: 151, m: 2, border: "1px solid black" }}
              image={value.restauranteImage}
              alt={value.restauranteNombre}
            />
            <Box sx={{ display: "flex", flexDirection: "column" }}>
              <CardContent sx={{ flex: "1 0 auto" }}>
                <Typography component="div" variant="h5">
                  {value.restauranteNombre}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  <Link
                    underline="none"
                    sx={{ cursor: "pointer" }}
                    onClick={() => handleIrAlPedido(value.idPedido)}
                  >
                    Ver detalle #{value.idPedido}
                  </Link>
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  {value.fecha} -{" "}
                  {value.metodoPago === "EFECTIVO" ? "Efectivo" : "PayPal"} - ${" "}
                  {value.monto}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  {calcularCantidad(value.menus)}
                </Typography>
              </CardContent>
            </Box>
          </Card>
        ))}
    </>
  );
};

export default ListaPedidos;
