import React, { useState, useEffect, useRef } from "react";
import {
  Container,
  Grid,
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
  Slider,
} from "@mui/material";
import Input from "./../components/basicos/Input";
import NavBar from "../components/NavBar/NavBar";
import colorCodes from "../assets/colors/Colors";
import StarIcon from "@mui/icons-material/Star";
import SearchIcon from "@mui/icons-material/Search";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import CloseIcon from "@mui/icons-material/Close";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import AxiosRestaurante from "../components/axios/AxiosRestaurante";
import MapaRestaurante from "../components/Maps/MapaRestaurante";
import Carrito from "../components/Carrito/Carrito";
import CardMenu from "../components/basicos/CardMenu";
import CardPromo from "../components/basicos/CardPromo";
import { useHistory } from "react-router-dom";
import rutas from "./../assets/const/rutas";

const Restaurante = () => {
  let history = useHistory();
  const [state, setState] = useState({
    idRestaurante: "",
    imagen: "",
    nombre: "",
    calificacion: "",
    costoEnvio: "",
    direccion: {},
    horarios: [],
    ubicacion: {},
    cobertura: "",
    menu: [],
    categorias: [],
  });

  const [email, setEmail] = useState("");

  useEffect(() => {
    let emailRestoran = localStorage.getItem("__email_restoran");
    if (emailRestoran === null) {
      history.push(rutas.lista_restaurantes);
    } else {
      setEmail(emailRestoran);
    }
  }, []);

  useEffect(() => {
    AxiosRestaurante.get(`/buscar/full/${email}`).then(function (response) {
      setState({
        idRestaurante: response.data.idRestaurante,
        imagen: response.data.imagen,
        nombre: response.data.nombre,
        calificacion: response.data.calificacion,
        costoEnvio: response.data.costoEnvio,
        direccion: response.data.direccion,
        horarios: response.data.horarios,
        ubicacion: response.data.ubicacion,
        cobertura: response.data.cobertura,
        menu: response.data.menu,
        email: response.data.email,
        categorias: response.data.categorias,
      });
    });
  }, [email]);

  const [topePrecio, setTopePrecio] = useState(0);

  useEffect(() => {
    let max = 0;
    state.menu.forEach((m) => {
      if (m.precio > max) {
        max = m.precio;
      }
    });

    setTopePrecio(max);
    setFiltroPrecio(max);
  }, [state.menu]);

  const [listaCategorias, setListaCategorias] = useState([]);

  useEffect(() => {
    let listaCat = [];
    listaCat = [...new Set(state.categorias.map((c) => c.descripcion))];
    setListaCategorias(listaCat);
  }, [state.categorias]);

  const [filtroPrecio, setFiltroPrecio] = useState(0);
  const handleSliderChange = (event, value) => {
    setFiltroPrecio(value);
  };

  const [selectedCategoria, setSelectedCategoria] = useState("");
  const handleChangeSelect = (event) => {
    setSelectedCategoria(event.target.value);
  };

  const [promociones, setPromociones] = useState([]);
  const [mostrarPromos, setMostrarPromos] = useState(false);
  const [menus, setMenus] = useState([]);
  useEffect(() => {
    let promos = promociones.slice();
    let menu = menus.slice();

    state.menu.forEach((m) => {
      if (m.promocion) {
        promos.push(m);
      } else {
        menu.push(m);
      }
    });

    setPromociones(promos);
    setMostrarPromos(true);
    setMenus(menu);
  }, [state.menu]);

  const carrusel = useRef(null);

  const [searchTerm, setSearchTerm] = useState("");

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  let filteredListPromos = promociones.filter(
    (item) =>
      item.titulo.toLowerCase().includes(searchTerm) ||
      item.titulo.includes(searchTerm) ||
      item.descripcion.toLowerCase().includes(searchTerm) ||
      item.descripcion.includes(searchTerm)
  );

  let filteredListMenus = menus.filter(
    (item) =>
      item.titulo.toLowerCase().includes(searchTerm) ||
      item.titulo.includes(searchTerm) ||
      item.descripcion.toLowerCase().includes(searchTerm) ||
      item.descripcion.includes(searchTerm)
  );

  const borrarFiltros = () => {
    setSearchTerm("");
    setSelectedCategoria("");
    setFiltroPrecio(1000);
  };

  const handleClickRight = () => {
    carrusel.current.scrollLeft += carrusel.current.offsetWidth;
  };

  const handleClickLeft = () => {
    carrusel.current.scrollLeft -= carrusel.current.offsetWidth;
  };

  const [carrito, setCarrito] = useState([]);

  const agregarAlCarrito = (pedido) => {
    let newCarrito = carrito.slice();

    newCarrito.push(pedido);
    setCarrito(newCarrito);
  };

  return (
    <>
      <NavBar />
      <Grid display="flex">
        <Container maxWidth="xs" sx={{ mr: 0 }}>
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <img height="300" src={state.imagen} />
              </Grid>
              <Grid item xs={12} display="flex">
                <Grid
                  item
                  xs={6}
                  sx={{ ml: 2 }}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <h2> {state.nombre}</h2>
                </Grid>
                <Grid
                  item
                  xs={6}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  {state.calificacion == 0 ? (
                    <h2 style={{ color: "green" }}> NUEVO</h2>
                  ) : (
                    <h2 style={{ color: "green" }}>
                      {" "}
                      <StarIcon /> {state.calificacion}
                    </h2>
                  )}
                </Grid>
              </Grid>
              <Grid
                item
                xs={12}
                sx={{ ml: 4 }}
                display="flex"
                justifyContent="left"
                alignItems="center"
              >
                {state.costoEnvio > 0 ? (
                  <span> Costo de env&iacute;o: $ {state.costoEnvio} </span>
                ) : (
                  <span> Costo de env&iacute;o: GRATIS</span>
                )}
              </Grid>
              <Grid
                item
                xs={12}
                sx={{ ml: 4 }}
                display="flex"
                style={{ flexDirection: "column" }}
                justifyContent="center"
                alignItems="left"
              >
                <h3> Direcci&oacute;n: </h3>
                <span>
                  {state.direccion.calle} {state.direccion.numero}
                </span>
              </Grid>
              {state.ubicacion.latitud && state.ubicacion.longitud ? (
                state.ubicacion.latitud !== 0 &&
                state.ubicacion.longitud !== 0 ? (
                  <Grid
                    item
                    xs={12}
                    sx={{ ml: 4, mr: 4 }}
                    display="flex"
                    style={{ flexDirection: "column" }}
                    justifyContent="center"
                    alignItems="left"
                  >
                    <MapaRestaurante
                      limite={state.cobertura}
                      posicion={[
                        parseInt(state.ubicacion.latitud),
                        state.ubicacion.longitud,
                      ]}
                    />
                  </Grid>
                ) : null
              ) : null}
              <Grid
                item
                xs={12}
                sx={{ ml: 4, mb: 4 }}
                display="flex"
                style={{ flexDirection: "column" }}
                justifyContent="center"
                alignItems="left"
              >
                <h3> Horarios: </h3>
                {state.horarios.map((horario) => {
                  if (horario.horaApertura && horario.horaCierre) {
                    return (
                      <span>
                        {" "}
                        {horario.dia}, {horario.horaApertura} a{" "}
                        {horario.horaCierre} hs
                      </span>
                    );
                  }
                  return null;
                })}
              </Grid>
            </Grid>
          </Box>
        </Container>

        <Container maxWidth="md" sx={{ ml: 0, mr: 0 }}>
          <Box bgcolor={colorCodes.backBox} sx={{ mt: 6 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} display="flex">
                <Grid
                  item
                  xs={10}
                  sx={{ ml: 2 }}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Input
                    id="buscar"
                    label="Buscar menus..."
                    value={searchTerm}
                    onChange={handleSearch}
                  />
                </Grid>
                <Grid
                  item
                  xs={2}
                  display="flex"
                  justifyContent="left"
                  alignItems="center"
                >
                  <SearchIcon />
                </Grid>
              </Grid>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="left"
                alignItems="center"
                sx={{ mt: 2, ml: 6 }}
              >
                <Typography component="legend">Filtrar por:</Typography>
              </Grid>
              <Grid
                item
                xs={12}
                display="column"
                justifyContent="center"
                alignItems="center"
                sx={{ ml: 1, mr: 1, mb: 2 }}
              >
                <Grid
                  item
                  xs={12}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={1} sx={{ mr: 2 }} align="right">
                    <MonetizationOnIcon />
                  </Grid>
                  <Grid item xs={4}>
                    <Typography id="input-slider" gutterBottom>
                      Precio
                    </Typography>
                    <Slider
                      value={
                        typeof filtroPrecio === "number" ? filtroPrecio : 0
                      }
                      onChange={handleSliderChange}
                      aria-labelledby="input-slider"
                      color="error"
                      max={topePrecio}
                      min={0}
                      step={5}
                    />
                  </Grid>
                  <Grid item xs={1} sx={{ pb: 1, ml: 3 }}>
                    $ {filtroPrecio}
                  </Grid>
                  <Grid
                    item
                    xs={5}
                    sx={{ mt: 2, ml: 2, mr: 6 }}
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <FormControl fullWidth sx={{ ml: 3, mr: 3 }} error>
                      <InputLabel id="demo-simple-select-label">
                        Categoria
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={selectedCategoria}
                        label="Categoria"
                        onChange={handleChangeSelect}
                        color="error"
                      >
                        {listaCategorias.map((option, key) => {
                          return (
                            <MenuItem key={key} value={option}>
                              {option}
                            </MenuItem>
                          );
                        })}
                      </Select>
                    </FormControl>
                    <CloseIcon
                      sx={{ cursor: "pointer" }}
                      onClick={borrarFiltros}
                    />
                  </Grid>
                </Grid>
              </Grid>

              {mostrarPromos ? (
                <Grid
                  item
                  xs={12}
                  display="flex"
                  style={{ flexDirection: "column" }}
                  justifyContent="center"
                  alignItems="center"
                >
                  <h2> Promociones </h2>
                  <Grid
                    item
                    xs={12}
                    display="flex"
                    style={{ flexDirection: "row" }}
                    justifyContent="center"
                    alignItems="center"
                  >
                    <KeyboardArrowLeftIcon
                      sx={{ cursor: "pointer" }}
                      onClick={handleClickLeft}
                    />
                    <Grid
                      component="div"
                      ref={carrusel}
                      sx={{
                        transition: "all ease-in 1s",
                        pt: 1,
                        pb: 1,
                        position: "relative",
                        width: "100%",
                        display: "flex",
                        justifyContent: "space-between",
                        overflowX: "hidden",
                        scrollBehavior: "smooth",
                        flexDirection: "row",
                      }}
                    >
                      <CardPromo
                        data={filteredListPromos}
                        filtroPrecio={filtroPrecio}
                        categoria={selectedCategoria}
                        agregarAlCarrito={agregarAlCarrito}
                      />
                    </Grid>
                    <KeyboardArrowRightIcon
                      sx={{ cursor: "pointer" }}
                      onClick={handleClickRight}
                    />
                  </Grid>
                </Grid>
              ) : null}
              <Grid
                item
                xs={12}
                display="flex"
                style={{ flexDirection: "column" }}
                justifyContent="center"
                alignItems="center"
              >
                <h2> Menus </h2>
                <Box sx={{ pt: 1, pb: 1, width: "100%" }}>
                  <CardMenu
                    data={filteredListMenus}
                    filtroPrecio={filtroPrecio}
                    categoria={selectedCategoria}
                    agregarAlCarrito={agregarAlCarrito}
                  />
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>

        <Container maxWidth="xs" sx={{ ml: 0 }}>
          <Box sx={{ mt: 6 }}>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Carrito menus={carrito} emailRestaurante={state.email} />
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Grid>
    </>
  );
};

export default Restaurante;
